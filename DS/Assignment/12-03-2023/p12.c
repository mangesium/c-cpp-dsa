#include <stdio.h>
#include <stdlib.h>
int size = 0;
int **matrixtranspose(int (*arr)[size], int row, int col)
{
    int **ptr;
    if (row >= col)
    {
        ptr = malloc(sizeof(int *) * col);
        for (int i = 0; i < col; i++)
        {
            ptr[i] = malloc(sizeof(int) * row);
            int j = 0;
            while (1)
            {
                if (j > row-1)
                    break;
                ptr[i][j] = arr[j][i];
                j++;
            }
        }
    }
    else
    {
        
    }
    return ptr;
}
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    size = col;
    int arr[row][row];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    printf("Input array : \n");
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }
    int **ptr = matrixtranspose(arr, row, col);
    printf("Transpose Array : \n");
    for (int i = 0; i < col; i++)
    {
        for (int j = 0; j < row; j++)
        {
            printf("%d ", ptr[i][j]);
        }
        printf("\n");
    }
}