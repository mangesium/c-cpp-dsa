#include <stdio.h>
void reverserange(int *arr, int size, int start, int end)
{
    int k = 0;
    for (int i = start; i < end; i++)
    {
        int swap = arr[i];
        arr[i] = arr[end - k];
        arr[end - k] = swap;
        k++;
    }
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    int start = size, end = size;
    while (start > size - 1 || end > size - 1)
    {
        printf("Enter start : ");
        scanf("%d", &start);
        printf("Enter end : ");
        scanf("%d", &end);
        if (start < 0 || end < 0)
        {
            start = end = size;
        }
    }

    printf("Before reverse : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    reverserange(arr, size, start, end);
    printf("After reverse : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
}