#include <stdio.h>
void evenrange(int *arr, int n)
{
    int pfix[n];
    int k = 0;
    if (arr[0] % 2 == 0)
        k = 1;
    else
        k = 0;
    pfix[0] = k;
    for (int i = 1; i < n; i++)
    {
        if (arr[i] % 2 == 0)
            k = 1;
        else
            k = 0;
        pfix[i] = pfix[i - 1] + k;
    }
    printf("Enter number of queries : ");
    int q = 0;
    scanf("%d", &q);
    int s = 0, e = 0;
    for (int i = 0; i < q; i++)
    {
        printf("Enter Start : ");
        scanf("%d", &s);
        printf("Enter end : ");
        scanf("%d", &e);
        if (s == 0)
        {
            printf("even numbers = %d\n", pfix[e]);
        }
        else
        {
            printf("even numbers = %d\n", pfix[e] - pfix[s - 1]);
        }
    }
    for (int i = 0; i < n; i++)
    {
        printf("%d ", pfix[i]);
    }
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    evenrange(arr, size);
}