#include <stdio.h>
#include <stdlib.h>
int *leader(int *arr, int n, int *ls)
{
    int *ld = malloc(sizeof(int) * n);
    int j = 1;
    ld[0] = arr[n - 1];
    int max = arr[n - 1];
    for (int i = n - 2; i >= 0; i--)
    {
        if (arr[i] > max)
        {
            max = arr[i];
            ld[j] = max;
            j++;
        }
    }
    int *l = malloc(sizeof(int) * j);
    for (int i = 0; i < j; i++)
    {
        l[i] = ld[i];
    }
    *ls = j;
    return l;
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    int ls = 0;
    int *ptr = leader(arr, size, &ls);
    for (int i = 0; i < ls; i++)
    {
        printf("%d ", ptr[i]);
    }
    printf("\n");
}