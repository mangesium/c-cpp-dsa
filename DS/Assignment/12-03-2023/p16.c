#include <stdio.h>
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    int matrix[row][col];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &matrix[i][j]);
        }
    }
    printf("Input array : \n");
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
    int rows[row],cols[col];

    for (int i = 0; i < row; i++)
    {
        rows[i] = -1;
    }
    for (int i = 0; i < col; i++)
    {
        cols[i] = -1;
    }

    for (int i = 0; i <row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (matrix[i][j] == 0)
            {
                rows[i] = 0;
                cols[j] = 0;
            }
        }
    }
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (rows[i] == 0 || cols[j] == 0)
            {
                matrix[i][j] = 0;
            }
        }
    }
    printf("Output array : \n");
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}