#include <stdio.h>
int equlibrium(int *arr, int size)
{
    int pfix[size];
    pfix[0] = arr[0];
    for (int i = 1; i < size; i++)
    {
        pfix[i] = pfix[i - 1] + arr[i];
    }
    for (int i = 1; i < size; i++)
    {
        if (i == 0)
        {
            if (pfix[size - 1] == 0)
                return 0;
        }
        else if (i == size - 1)
        {
            if (pfix[size - 2] == 0)
                return size - 1;
        }
        else
        {
            if (pfix[i - 1] == (pfix[size - 1] - pfix[i]))
                return i;
        }
    }
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Equilibrium element present at %d index\n", equlibrium(arr, size));
}