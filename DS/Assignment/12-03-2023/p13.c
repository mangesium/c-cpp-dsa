#include <stdio.h>
int size = 0;
void rotatematrix(int (*matrix)[size], int row, int col)
{
    int i;
    int j;
    int temp;

    for (i = 0; i < row; i++)
    {
        for (j = i; j < row; j++)
        {
            temp = matrix[i][j];
            matrix[i][j] = matrix[j][i];
            matrix[j][i] = temp;
        }
    }

    for (i = 0; i < row; i++)
    {
        for (j = 0; j < row / 2; j++)
        {
            temp = matrix[i][j];
            matrix[i][j] = matrix[i][row - 1 - j];
            matrix[i][row - 1 - j] = temp;
        }
    }
}
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    size = col;
    int arr[row][col];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    printf("Input array : \n");
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }
    rotatematrix(arr, row, col);
    printf("Rotated Matrix : \n");
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }
}