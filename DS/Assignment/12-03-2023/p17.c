#include <stdio.h>
#include <stdlib.h>
int *prfixarrar(int *arr, int size)
{

    int *ptr = malloc(sizeof(int) * size);
    ptr[0] = arr[0];

    for (int i = 1; i < size; i++)
    {
        ptr[i] = ptr[i - 1] + arr[i];
    }
    return ptr;
}
void main()
{
    int row;
    printf("Enter size of array : ");
    scanf("%d", &row);

    int arr[row];
    for (int i = 0; i < row; i++)
    {
        printf("Enter element : ");
        scanf("%d", &arr[i]);
    }
    int *ptr = prfixarrar(arr, row);
    printf("prefix array : \n");
    for (int i = 0; i < row; i++)
    {
        printf("%d ", ptr[i]);
    }

    printf("\n");
}