#include <stdio.h>
#include <stdlib.h>
int size=0;
int maindigsum(int (*arr)[size], int row){
    int sum=0;
    for(int i=0;i<row;i++){
        sum+=arr[i][i];
    }
    
    return sum;
}
void main()
{
    int row;
    printf("Enter number of rows and column: ");
    scanf("%d", &row);
    size=row;
    int arr[row][row];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < row; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    int ptr = maindigsum(arr, row);
    printf("Sum of main diagonal : %d\n",ptr);
}