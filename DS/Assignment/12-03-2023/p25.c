#include <stdio.h>
int goodsubarray(int *arr, int size, int B)
{
    int count = 0;
    for (int i = 0; i < size; i++)
    {
        int sum = 0;
        int r = 0;
        for (int j = i; j < size; j++)
        {
            sum += arr[j];
            r++;
            if (r % 2 == 0 && sum < B)
            {
                count++;
            }
            else if (r % 2 == 1 && sum > B)
                count++;
        }
    }
    return count;
}

void main()
{
    int size, B = 0;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Enter Number to compare : ");
    scanf("%d", &B);
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    int ptr = goodsubarray(arr, size, B);

    printf("%d is the count of all good subarray\n", ptr);
}