#include <stdio.h>
int retmaxcount(int *arr, int size)
{
    int max = arr[0], count = 1, flag = 0;

    for (int i = 1; i < size; i++)
    {
        if (arr[i] > max)
        {
            max = arr[i];
            flag = 1;
            count = 1;
        }
        else if (arr[i] == max)
        {
            count++;
        }
    }
    if (flag == 0)
        return 0;
    return size - count;
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("%d max elements\n", retmaxcount(arr, size));
}