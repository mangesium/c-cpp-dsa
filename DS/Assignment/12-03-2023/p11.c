
#include <stdio.h>
#include <stdlib.h>
int size = 0;
int ROW = 0;
int **antidiagonalmatrix(int (*arr)[size], int orow, int col)
{
    int row = 2 * orow - 1;
    int **ptr = malloc(sizeof(int *) * row);
    int k = 0, m = 0, n = 0, l = 1;
    for (int i = 0; i < row; i++)
    {
        ptr[i] = malloc(sizeof(int) * col);
        if (i <= row / 2)
        {
            k++;
            m = 0;
            n = k - 1;
        }
        else
        {
            k--;
            m = l;
            n = orow - 1;
            l++;
        }
        int p = k;
        for (int j = 0; j < col; j++)
        {
            if (p > 0)
            {
                ptr[i][j] = arr[m][n];
                m++;
                n--;
            }
            else
            {
                ptr[i][j] = 0;
            }
            p--;
        }
    }
    ROW = row;
    return ptr;
}

void main()
{
    int row;
    printf("Enter number of rows and columns : ");
    scanf("%d", &row);
    size = row;
    int arr[row][row];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < row; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    int **ptr = antidiagonalmatrix(arr, row, row);

    printf("Antidiagonal Array : \n");
    for (int i = 0; i < ROW; i++)
    {
        for (int j = 0; j < row; j++)
        {
            printf("%d ", ptr[i][j]);
        }
        printf("\n");
    }
}