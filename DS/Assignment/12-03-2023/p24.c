#include <stdio.h>

int sumofsubarray(int *arr, int size)
{

    int sum = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = i; j < size; j++)
        {
            for (int k = i; k <= j; k++)
            {
                sum += arr[k];
            }
        }
    }
    return sum;
}

void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    int ptr = sumofsubarray(arr, size);

    printf("%d is the sum of all sub array\n", ptr);
}