#include <stdio.h>
#include<stdlib.h>
int size1 = 0, size2 = 0;
int **addmatrix(int (*arr1)[2], int row1, int col1, int (*arr2)[2], int row2, int col2)
{

    int **ptr = malloc(sizeof(int *) * row1);
    for (int i = 0; i < row1; i++)
    {
        ptr[i] = malloc(sizeof(int) * col1);
        for (int j = 0; j < col1; j++)
        {
            ptr[i][j] = arr1[i][j] + arr2[i][j];
        }
    }
    return ptr;
}
void main()
{
    int row1, col1, row2, col2;
    printf("Enter number of rows : ");
    scanf("%d", &row1);
    printf("Enter number of columns : ");
    scanf("%d", &col1);
    size1 = col1;
    int arr1[row1][col1];
    for (int i = 0; i < row1; i++)
    {
        for (int j = 0; j < col1; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr1[i][j]);
        }
    }
    printf("Input array : \n");
    for (int i = 0; i < row1; i++)
    {
        for (int j = 0; j < col1; j++)
        {
            printf("%d ", arr1[i][j]);
        }
        printf("\n");
    }
    printf("Enter number of rows : ");
    scanf("%d", &row2);
    printf("Enter number of columns : ");
    scanf("%d", &col2);
    size1 = col2;
    int arr2[row2][col2];
    for (int i = 0; i < row2; i++)
    {
        for (int j = 0; j < col2; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr2[i][j]);
        }
    }
    printf("Input array : \n");
    for (int i = 0; i < row2; i++)
    {
        for (int j = 0; j < col2; j++)
        {
            printf("%d ", arr2[i][j]);
        }
        printf("\n");
    }
    int **ptr;
    if (row1 != row2 || col1 != col2)
    {
        printf("Can't add the matrix not ideal matrix\n");
    }
    else
    {
        ptr = addmatrix(arr1, row1, col1, arr2, row2, col2);
    }
    /*int ptr[row1][col1];
    for (int i = 0; i < row1; i++)
    {
        //ptr[i] = malloc(sizeof(int) * col1);
        for (int j = 0; j < col1; j++)
        {
            ptr[i][j]=0;
            ptr[i][j] = arr1[i][j] + arr2[i][j];
        }
    }*/

    printf("Addition Matrix : \n");
    for (int i = 0; i < row1; i++)
    {
        for (int j = 0; j < col1; j++)
        {
            printf("%d ", ptr[i][j]);
        }
        printf("\n");
    }
}