#include <stdio.h>
#include <stdlib.h>
int *rowsum(int (*ptr)[4], int row, int col)
{
    int *arr = malloc(sizeof(int) * row);
    for (int i = 0; i < row; i++)
        arr[i] = 0;
    int k = 0;
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            arr[k] += ptr[i][j];
        }
        k++;
    }

    return arr;
}
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    int arr[row][col];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    int *ptr = rowsum(arr, row, col);
    printf("Array of row sum : ");
    for (int i = 0; i < row; i++)
    {
        printf("%d ", ptr[i]);
    }
    printf("\n");
}