#include <stdio.h>
void reverse(int *arr, int size)
{
    for (int i = 0; i < size/2; i++)
    {
        int swap = arr[i];
        arr[i] = arr[size - 1 - i];
        arr[size - 1 - i] = swap;
    }
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }

    printf("Before reverse : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    reverse(arr, size);
    printf("After reverse  : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
}