#include <stdio.h>
#include <stdlib.h>
int *columnsum(int (*ptr)[4], int row, int col)
{
    int *arr = malloc(sizeof(int) * col);
    for (int i = 0; i < col; i++)
        arr[i] = 0;

    int k = 0;
    for (int i = 0; i < col; i++)
    {
        for (int j = 0; j < col; j++)
        {
            if (j >= row)
            {
                break;
            }
            else
                arr[k] += ptr[j][i];
        }
        k++;
    }

    return arr;
}
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    int arr[row][col];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    int *ptr = columnsum(arr, row, col);

    printf("Array of column sum : ");
    for (int i = 0; i < col; i++)
    {
        printf("%d ", ptr[i]);
    }
    printf("\n");
}