#include <stdio.h>
#include <limits.h>
int goodpair(int *arr, int size, int num)
{
    int count = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = i + 1; j < size; j++)
        {
            if (arr[i] + arr[j] == num)
            {
                count++;
                arr[j] = INT_MAX;
                break;
            }
        }
    }
    return count;
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    int num;
    printf("Enter number to fing pair : ");
    scanf("%d", &num);
    printf("%d pairs available\n", goodpair(arr, size, num));
}