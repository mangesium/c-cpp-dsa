#include <stdio.h>
void rotate(int *arr, int size, int r)
{
    int num = r;
    while (num > size - 1)
    {
        num = r / size;
    }
    for (int i = 0; i < r; i++)
    {
        int tmp = arr[size - 1];
        for (int j = size - 2; j >= 0; j--)
        {
            arr[j + 1] = arr[j];
        }
        arr[0] = tmp;
    }
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    int r;
    printf("Enter Rotations of array : ");
    scanf("%d", &r);
    printf("Before rotation : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    rotate(arr, size, r);
    printf("After rotation  : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
}