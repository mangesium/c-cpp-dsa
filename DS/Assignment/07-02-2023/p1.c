#include <stdio.h>
#include <limits.h>
int retmaxcount(int *arr, int size)
{
    int min = INT_MAX, count = 0, flag = 0, i = 0;
    for (i = 0; i < size - 1; i++)
    {
        if (arr[i] < min)
        {
            min = arr[i];
        }
        if (arr[i] == arr[i + 1])
        {
            flag = 1;
        }
        else
        {
            flag = 0;
        }
    }

    if (flag == 1)
        return 0;

    if (arr[i] < min)
        min = arr[i];

    for (int i = 0; i < size; i++)
    {
        for (int j = i + 1; j < size; j++)
        {

            if (arr[i] == arr[j] && arr[i] > min)
            {
                count++;
            }
        }
        if (flag == 1)
            return 0;
    }
    return size - 1 - count;
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("%d max elements\n", retmaxcount(arr, size));
}