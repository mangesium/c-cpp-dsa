#include <stdio.h>
#include <stdlib.h>
int size = 0;

int minordigsum(int (*arr)[size], int row)
{
    static int i = 0, sum = 0;

    if (i == row)
        return sum;
    sum += arr[i][row-1-i];
    i++;
    return minordigsum(arr, row);
}
void main()
{
    int row;
    printf("Enter number of rows and column: ");
    scanf("%d", &row);
    size = row;
    int arr[row][row];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < row; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    int ptr = minordigsum(arr, row);
    printf("Sum of main diagonal : %d\n", ptr);
}