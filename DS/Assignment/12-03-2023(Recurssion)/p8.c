#include <stdio.h>
#include <stdlib.h>

int size = 0;
void rowsum(int (*ptr)[size], int row, int col, int *arr)
{

    static int k = 0, i = 0;
    if (i == row)
        return;
    for (int j = 0; j < col; j++)
    {
        arr[k] += ptr[i][j];
    }
    k++;
    i++;
    return rowsum(ptr, row, col, arr);
}
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    size = col;
    int arr[row][col];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    int *arr1 = malloc(sizeof(int) * row);
    for (int i = 0; i < col; i++)
        arr1[i] = 0;

    rowsum(arr, row, col, arr1);

    printf("Array of column sum : ");
    for (int i = 0; i < col; i++)
    {
        printf("%d ", arr1[i]);
    }
    printf("\n");
}