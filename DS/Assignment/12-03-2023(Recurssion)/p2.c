#include <stdio.h>
#include <limits.h>
int goodpair(int *arr, int size, int num)
{
    static int i = 0, count = 0;
    if (i == size)
        return count;
    for (int j = i + 1; j < size; j++)
    {
        if (arr[i] + arr[j] == num)
        {
            count++;
            arr[j] = INT_MAX;
            break;
        }
    }
    i++;

    return goodpair(arr, size, num);
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    int num;
    printf("Enter number to find pair : ");
    scanf("%d", &num);
    printf("%d pairs available\n", goodpair(arr, size, num));
}