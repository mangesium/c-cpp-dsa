#include <stdio.h>
#include <stdlib.h>

int *prfixarrar(int *arr, int size, int *ptr)
{
    ptr[0] = arr[0];
    static int i = 1;
    if (i == size)
        return ptr;

    ptr[i] = ptr[i - 1] + arr[i];
    i++;
    return prfixarrar(arr, size,ptr);
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);

    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element : ");
        scanf("%d", &arr[i]);
    }
    int *ptr1 = malloc(sizeof(int) * size);
    int *ptr = prfixarrar(arr, size, ptr1);
    printf("prefix array : \n");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", ptr[i]);
    }

    printf("\n");
}