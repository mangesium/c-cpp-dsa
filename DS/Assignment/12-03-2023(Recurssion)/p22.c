#include <stdio.h>
#include <string.h>
int subsequence(char *cptr, char *c)
{
    static int i = 0, count = 0, k = 0;
    char ch = 'a';
    if (i == strlen(cptr))
        return count;

    if (cptr[i] == c[0])
    {
        k++;
    }
    if (cptr[i] == c[1])
    {
        count += k;
    }
    i++;
    return subsequence(cptr, c);
}
void main()
{
    char arr[20] = "AGBCGAGG";
    for (int i = 0; i < strlen(arr); i++)
    {
        printf("%c", arr[i]);
    }
    printf("\n");
    int p = subsequence(arr, "AG");
    printf("%d ", p);
    printf("\n");
}