#include <stdio.h>

int retmaxcount(int *arr, int size, int max)
{
    static int i = 1, count = 1,flag=0;

    if (i == size - 1)
    {
        if(flag==0)
            return 0;
        return size - count;
    }
    if (arr[i] > max)
    {
        max = arr[i];
        count = 1;
        flag=1;
    }
    else if (arr[i] == max)
    {
        count++;
    }
    i++;
    return retmaxcount(arr, size, max);
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    int max = arr[0];
    printf("%d max elements\n", retmaxcount(arr, size, max));
}