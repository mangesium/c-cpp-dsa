#include <stdio.h>
#include <stdlib.h>
int size = 0;
void columnsum(int (*ptr)[size], int row, int col, int *arr)
{

    static int k = 0, i = 0;
    if (i == col)
        return;
    for (int j = 0; j < col; j++)
    {
        if (j >= row)
        {
            break;
        }
        else
            arr[k] += ptr[j][i];
    }
    k++;
    i++;
    return columnsum(ptr, row, col, arr);
}
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    size = col;
    int arr[row][col];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    int *arr1 = malloc(sizeof(int) * col);
    for (int i = 0; i < col; i++)
        arr1[i] = 0;

    columnsum(arr, row, col, arr1);

    printf("Array of column sum : ");
    for (int i = 0; i < col; i++)
    {
        printf("%d ", arr1[i]);
    }
    printf("\n");
}