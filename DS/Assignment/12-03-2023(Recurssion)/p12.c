#include <stdio.h>
#include <stdlib.h>
int size = 0;
void matrixtranspose(int (*arr)[size], int row, int col, int **ptr)
{
    static int i = 0;
    if (i == col)
        return;
    ptr[i] = malloc(sizeof(int) * row);
    int j = 0;
    while (1)
    {
        if (j > row - 1)
            break;
        ptr[i][j] = arr[j][i];
        j++;
    }
    i++;

    return matrixtranspose(arr, row, col, ptr);
}
void main()
{
    int row, col;
    printf("Enter number of rows : ");
    scanf("%d", &row);
    printf("Enter number of columns : ");
    scanf("%d", &col);
    size = col;
    int arr[row][row];
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("Enter element : ", i);
            scanf("%d", &arr[i][j]);
        }
    }
    printf("Input array : \n");
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }
    int **ptr = malloc(sizeof(int *) * col);
    matrixtranspose(arr, row, col, ptr);
    printf("Transpose Array : \n");
    for (int i = 0; i < col; i++)
    {
        for (int j = 0; j < row; j++)
        {
            printf("%d ", ptr[i][j]);
        }
        printf("\n");
    }
}