#include <stdio.h>

void reverse(int *arr, int size)
{
    static int i = 0;
    if (i == size / 2)
    {
        return;
    }
    int swap = arr[i];
    arr[i] = arr[size - 1 - i];
    arr[size - 1 - i] = swap;
    i++;

    return reverse(arr, size);
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter at %d index : ", i);
        scanf("%d", &arr[i]);
    }

    printf("Before reverse : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    reverse(arr, size);
    printf("After reverse  : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
}