#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
};

struct queue
{
    struct tree *data;
    struct queue *next;
};

struct queue *front = NULL;
struct queue *rear = NULL;
bool isQEmpty()
{
    if (front == NULL)
        return true;
    return false;
}
void enqueue(struct tree *root)
{

    struct queue *temp = malloc(sizeof(struct queue));
    temp->data = root;
    if (isQEmpty())
    {
        front = rear = temp;
    }
    else
    {
        rear->next = temp;
        rear = temp;
    }
}
struct tree *dequeue()
{
    if (isQEmpty())
    {
        return NULL;
    }
    else
    {
        struct queue *temp = front;
        struct tree *item = temp->data;
        if (front == rear)
        {
            front = rear = NULL;
        }
        else
        {
            front = front->next;
        }
        free(temp);
        return item;
    }
}
void levelOrder(struct tree *root)
{
    enqueue(root);
    while (!isQEmpty())
    {
        root = dequeue();
        printf("%d ", root->data);
        if (root->left)
            enqueue(root->left);
        if (root->right)
            enqueue(root->right);
    }
}
void levelOrderRtL(struct tree *root)
{
    enqueue(root);
    while (!isQEmpty())
    {
        root = dequeue();
        printf("%d ", root->data);
        if (root->right)
            enqueue(root->right);
        if (root->left)
            enqueue(root->left);
    }
}
void levelOrderNull(struct tree *root)
{
    enqueue(root);
    enqueue(NULL);
    while (front != rear)
    {
        root = dequeue();
        if (root == NULL)
        {
            printf("\n");
            enqueue(NULL);
        }
        else
        {
            printf("%d ", root->data);
            if (root->left)
                enqueue(root->left);
            if (root->right)
                enqueue(root->right);
        }
    }
}
int size()
{
    if (isQEmpty())
        return 0;
    int cnt = 0;
    struct queue *temp = front;
    while (temp != rear)
    {
        cnt++;
        temp = temp->next;
    }
    return cnt + 1;
}
void levelOrderOptimised(struct tree *root)
{
    enqueue(root);
    while (!isQEmpty())
    {
        int sz = size();
        for (int i = 0; i < sz; i++)
        {
            root = dequeue();
            printf("%d ", root->data);
            if (root->left)
                enqueue(root->left);
            if (root->right)
                enqueue(root->right);
        }
        printf("\n");
    }
}
void levelOrderLeft(struct tree *root)
{
    enqueue(root);
    while (!isQEmpty())
    {
        int sz = size();
        for (int i = 0; i < sz; i++)
        {
            root = dequeue();
            if (i == 0)
                printf("%d ", root->data);
            if (root->left)
                enqueue(root->left);
            if (root->right)
                enqueue(root->right);
        }
        printf("\n");
    }
}
void levelOrderRight(struct tree *root)
{
    enqueue(root);
    while (!isQEmpty())
    {
        int sz = size();
        for (int i = 0; i < sz; i++)
        {
            root = dequeue();
            if (i == sz - 1)
                printf("%d ", root->data);
            if (root->left)
                enqueue(root->left);
            if (root->right)
                enqueue(root->right);
        }
        printf("\n");
    }
}
struct stack
{
    struct tree *data;
    struct stack *next;
};
struct stack *top = NULL;
bool isEmpty()
{
    if (top == NULL)
        return true;
    return false;
}
void push(struct tree *root)
{
    struct stack *temp = malloc(sizeof(struct stack));
    temp->data = root;
    temp->next = top;
    top = temp;
}
struct tree *pop()
{
    if (isEmpty())
    {
        printf("Empty stack\n");
    }
    else
    {
        struct stack *temp = top;
        struct tree *item = top->data;
        top = top->next;
        free(temp);
        return item;
    }
}
void iterativeInOrder(struct tree *root)
{
    while (root != NULL || !isEmpty())
    {
        if (root)
        {
            push(root);
            root = root->left;
        }
        else
        {
            root = pop();
            printf("%d ", root->data);
            root = root->right;
        }
    }
}
void iterativePreOrder(struct tree *root)
{
    while (root != NULL || !isEmpty())
    {
        if (root)
        {
            printf("%d ", root->data);
            if (root->right != NULL)
                push(root->right);
            root = root->left;
        }
        else
        {
            root = pop();
        }
    }
}
void iterativePostOrder(struct tree *root)
{
    while (root != NULL || !isEmpty())
    {
        if (root)
        {
            push(root);
            root = root->left;
        }
        else
        {
            struct tree *temp = top->data->right;
            if (temp == NULL)
            {
                temp = top->data;
                pop();
                printf("%d ", temp->data);
                while (!isEmpty() && temp == top->data->right)
                {
                    temp = top->data;
                    pop();
                    printf("%d ", temp->data);
                }
            }
            else
            {
                root = temp;
            }
        }
    }
}
struct tree *buildTreePreIn(int *in, int *pre, int instart, int inend, int pstart, int pend)
{
    if (instart > inend)
        return NULL;

    struct tree *root = malloc(sizeof(struct tree));
    root->data = pre[pstart];
    int i = instart;
    for (; i <= inend; i++)
    {
        if (in[i] == pre[pstart])
            break;
    }
    root->left = buildTreePreIn(in, pre, instart, i - 1, pstart + 1, i - instart + pstart);
    root->right = buildTreePreIn(in, pre, i + 1, inend, i - instart + pstart + 1, pend);

    return root;
}
struct tree *buildTreePostIn(int *in, int *post, int instart, int inend, int pstart, int pend)
{
    if (instart > inend)
        return NULL;

    struct tree *root = malloc(sizeof(struct tree));
    root->data = post[pend];
    int i = instart;
    for (; i <= inend; i++)
    {
        if (in[i] == post[pend])
            break;
    }
    root->left = buildTreePostIn(in, post, instart, i - 1, pstart, i - instart + pstart - 1);
    root->right = buildTreePostIn(in, post, i + 1, inend, i - instart + pstart, pend - 1);
    return root;
}
struct tree *buildTreePrePost(int *pre, int *post, int prestart, int preend, int poststart, int postend)
{
    if (prestart > preend)
        return NULL;
    struct tree *root = malloc(sizeof(struct tree));
    root->data = pre[prestart];
    int i = poststart;
    for (; i <= postend; i++)
    {
        if (pre[prestart] == post[i])
            break;
    }
    printf("In Pre Post\n");
    root->left = buildTreePrePost(pre, post, prestart + 1, i - poststart + prestart, poststart, i);
    root->right = buildTreePrePost(pre, post, i - poststart + prestart + 1, preend, i + 1, postend - 1);
    return root;
}
void preOrder(struct tree *root)
{
    if (root != NULL)
    {
        printf("%d ", root->data);
        preOrder(root->left);
        preOrder(root->right);
    }
}
void inOrder(struct tree *root)
{
    if (root != NULL)
    {
        inOrder(root->left);
        printf("%d ", root->data);
        inOrder(root->right);
    }
}
void postOrder(struct tree *root)
{
    if (root != NULL)
    {
        postOrder(root->left);
        postOrder(root->right);
        printf("%d ", root->data);
    }
}
void printTree(struct tree *root)
{
    int k = 1;
    do
    {
        printf("1.PreOrder\n");
        printf("2.InOrder\n");
        printf("3.PostOrder\n");
        printf("4.Exit\n");
        printf("5.Iterative InOrder\n");
        printf("6.Iterative PreOrder\n");
        printf("7.Iterative PostOrder\n");
        printf("8.Level Order\n");
        printf("9.Level Order Null Approch\n");
        printf("10.Level Order Optimised Approch\n");
        printf("11.Level Order Left View\n");
        printf("12.Level Order Right View\n");
        printf("13.Level Order Right to Left\n");

        int ch;
        printf("Enter choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
        {
            printf("PreOrder : ");
            preOrder(root);
            printf("\n");
            break;
        }
        case 2:
        {
            printf("InOrder : ");
            inOrder(root);
            printf("\n");
            break;
        }
        case 3:
        {
            printf("PostOrder : ");
            postOrder(root);
            printf("\n");
            break;
        }
        case 4:
            k = 2;
            break;
        case 5:
        {
            printf("Iterative InOrder : ");
            iterativeInOrder(root);
            printf("\n");
            break;
        }
        case 6:
        {
            printf("Iterative PreOrder : ");
            iterativePreOrder(root);
            printf("\n");
            break;
        }
        case 7:
        {
            printf("Iterative PostOrder : ");
            iterativePostOrder(root);
            printf("\n");
            break;
        }
        case 8:
        {
            printf("Level Order : ");
            levelOrder(root);
            printf("\n");
            break;
        }
        case 9:
        {
            printf("Level Order Null Approach : \n");
            levelOrderNull(root);
            printf("\n");
            break;
        }
        case 10:
        {
            printf("Level Order Optimised Approach : \n");
            levelOrderOptimised(root);
            break;
        }
        case 11:
        {
            printf("Level Order Left View : \n");
            levelOrderLeft(root);
            break;
        }
        case 12:
        {
            printf("Level Order Right View : \n");
            levelOrderRight(root);
            break;
        }
        case 13:
        {
            printf("Level Order RighttoLeft: ");
            levelOrderRtL(root);
            printf("\n");
            break;
        }
        default:
            printf("Enter valid choice\n");
            break;
        }

    } while (k == 1);
    printf("\n");
}

int countTree(struct tree *root)
{
    if (root)
    {
        return 1 + countTree(root->left) + countTree(root->right);
    }
    else
        return 0;
}
int maxof(int l, int r)
{
    if (l > r)
        return l;
    return r;
}

int heightOfTree(struct tree *root)
{
    if (root)
    {
        int left = heightOfTree(root->left);
        int right = heightOfTree(root->right);
        return maxof(left, right) + 1;
    }
    else
        return -1;
}
int maxOfTree(struct tree *root)
{
    if (root)
    {
        int left = maxOfTree(root->left);
        int right = maxOfTree(root->right);
        if (root->data > maxof(left, right))
            return root->data;
        else
            return maxof(left, right);
    }
    else
        return INT_MIN;
}
int sumofBtree(struct tree *root)
{
    if (root != NULL)
    {
        return root->data + sumofBtree(root->left) + sumofBtree(root->right);
    }
    return 0;
}
void main()
{
    int pre[] = {1, 2, 4, 5, 3, 6, 7};
    int in[] = {4, 2, 5, 1, 6, 7, 3};
    int post[] = {4, 5, 2, 7, 6, 3, 1};

    // int pre[] = {1, 2, 3, 5, 6, 4, 7, 8};
    // int in[] = {5, 3, 6, 2, 7, 4, 8, 1};
    // int post[] = {5, 6, 3, 7, 8, 4, 2, 1};
    int size = sizeof(post) / sizeof(post[0]);
    struct tree *root = buildTreePreIn(in, pre, 0, size - 1, 0, size - 1);
    // struct tree *root = buildTreePostIn(in, post, 0, size - 1, 0, size - 1);
    // struct tree *root = buildTreePrePost(pre, post, 0, size - 1, 0, size - 1);
    printf("Size of binary tree = %d\n", countTree(root));
    printf("Height of tree = %d\n", heightOfTree(root));
    printf("Maximum of tree = %d\n", maxOfTree(root));
    printf("Sum of nodes of tree = %d\n", sumofBtree(root));

    printTree(root);
}
