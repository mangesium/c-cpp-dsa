#include <stdio.h>
void swap(int *x, int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}
int part(int *arr, int start, int end)
{

    int pivot = arr[start];
    int i = start - 1, j = end + 1;

    while (1)
    {
        do
        {
            i++;
        } while (arr[i] < pivot);
        do
        {
            j--;
        } while (arr[j] > pivot);
        if (i >= j)
            return j;
            
        swap(&arr[i], &arr[j]);
    }
    return j;
}

void quick(int *arr, int start, int end)
{
    if (start < end)
    {
        int pivot = part(arr, start, end);
        quick(arr, start, pivot);
        quick(arr, pivot + 1, end);
    }
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Before sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    quick(arr, 0, size - 1);
    printf("After sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}