#include <stdio.h>

void sort(int *arr, int start, int mid, int end)
{
    int A[end - start + 1];
    int i = start, j = mid + 1, k = 0;
    while (i <= mid && j <= end)
    {
        if (arr[i] > arr[j])
        {
            A[k] = arr[j];
            j++;
        }
        else
        {
            A[k] = arr[i];
            i++;
        }
        k++;
    }
    while (i <= mid)
    {
        A[k] = arr[i];
        i++;
        k++;
    }
    while (j <= end)
    {
        A[k] = arr[j];
        j++;
        k++;
    }
    for (i = start; i <= end; i++)
    {
        arr[i] = A[i - start];
    }
}

void merge(int *arr, int start, int end)
{
    if (start < end)
    {
        int mid = start + (end - start) / 2;
        merge(arr, start, mid);
        merge(arr, mid + 1, end);
        sort(arr, start, mid, end);
    }
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Before sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    merge(arr, 0, size - 1);
    printf("After sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}