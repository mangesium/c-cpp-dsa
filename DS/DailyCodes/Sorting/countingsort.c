#include <stdio.h>
void counting(int *arr,int size){

    int max=arr[0];
    for(int i=0;i<size;i++){
        if(arr[i]>max)
            max=arr[i];
    }
    int countarr[max+1];
    for(int i=0;i<=max;i++){
        countarr[i]=0;
    }
    for(int i=0;i<size;i++)
        countarr[arr[i]]++;
    
    int output[size];
    for(int i=1;i<=max;i++)
        countarr[i]=countarr[i-1]+countarr[i];

    for(int i=size-1;i>=0;i--){
        output[countarr[arr[i]]-1]=arr[i]; 
        countarr[arr[i]]--;
    }

    for(int i=0;i<size;i++)
        arr[i]=output[i];   
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Before sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    counting(arr, size);
    printf("After sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}