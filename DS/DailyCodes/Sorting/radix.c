#include <stdio.h>

void counting(int *arr, int size, int pos)
{

    int countArr[10] = {0};

    for (int i = 0; i < size; i++)
    {
        countArr[(arr[i] / pos) % 10]++;
    }
    for (int i = 1; i < 10; i++)
        countArr[i] += countArr[i - 1];

    int output[size];
    for (int i = size - 1; i >= 0; i--)
    {
        output[countArr[(arr[i] / pos) % 10] - 1] = arr[i];
        countArr[(arr[i] / pos) % 10]--;
    }

    for (int i = 0; i < size; i++)
        arr[i] = output[i];
}

void radix(int *arr, int size)
{
    int max = arr[0];
    for (int i = 1; i < size; i++)
    {
        if (arr[i] > max)
            max = arr[i];
    }
    for (int pos = 1; max / pos > 0; pos *= 10)
    {
        counting(arr, size, pos);
    }
}

void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Before sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    radix(arr, size);
    printf("After sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}