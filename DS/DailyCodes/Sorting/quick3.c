#include <stdio.h>

int part(int *arr, int start, int end)
{
    int A[end - start + 1];
    int idx = 0, pivot = arr[end], i = start;
    for (i = start; i < end; i++)
    {
        if (arr[i] < pivot)
        {
            A[idx] = arr[i];
            idx++;
        }
    }
    int k = idx + start;
    A[idx] = pivot;
    idx++;
    for (i = start; i < end; i++)
    {
        if (arr[i] >= pivot)
        {
            A[idx] = arr[i];
            idx++;
        }
    }
    // for (i = start; i <= end; i++)
    // {
    //     arr[i] = A[i - start];
    // }
    for(int i=0;i<end-start+1;i++)
        arr[start+i]=A[i];
    return k;
}

void quick(int *arr, int start, int end)
{
    if (start < end)
    {
        int pivot = part(arr, start, end);
        quick(arr, start, pivot - 1);
        quick(arr, pivot + 1, end);
    }
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Before sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    quick(arr, 0, size - 1);
    printf("After sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}