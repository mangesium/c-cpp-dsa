/*
Q1. Given an array arr[ ] consisting of N positive integers, the task is to sort
the array such that –
- All even numbers must come before all odd numbers.
- All even numbers divisible by 5 must come first then even numbers
not divisible by 5.
- If two even numbers are divisible by 5 then the number having a greater
value will come first
- If two even numbers were not divisible by 5 then the number having a
greater index in the array will come first.
- All odd numbers must come in relative order as they are present in the
array.
Examples:
Input
:
arr[] = {5, 10, 30, 7}
Output
:
30 10 5 7

*/
#include <stdio.h>
void sort(int* arr,int size)
{
    int e=0,o=0;
    for(int i=0;i<size;i++){
        if(arr[i]%2==0){
            e++;
        }
    }
    for(int i=0;i<size;i++){
        if(arr[i]%2==1){
            
        }
    }

}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Before sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    sort(arr, size);
    printf("After sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}