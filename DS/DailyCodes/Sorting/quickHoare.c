#include <stdio.h>

void swap(int *x, int *y)
{
    int temp = *x;
    *x = *y;
    *y = temp;
}

int part(int *arr, int start, int end)
{
    int pivot = arr[start];
    start--;
    end++;
    while (1)
    {
        do
        {
            start++;
        } while (arr[start] < pivot);

        do
        {
            end--;
        } while (arr[end] > pivot);
        
        if (start >= end)
            return end;

        swap(&arr[start], &arr[end]);
    }
}
void quick(int *arr, int start, int end)
{
    if (start < end)
    {
        int pivot = part(arr, start, end);
        quick(arr, start, pivot);
        quick(arr, pivot + 1, end);
    }
}

void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    printf("Enter elements in array\n");
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element at %d idx : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Array before Sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    quick(arr, 0, size - 1);
    printf("Array after Sorting : ");
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}