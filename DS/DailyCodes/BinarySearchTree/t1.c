#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
struct BSTNode
{
    int data;
    struct BSTNode *left;
    struct BSTNode *right;
};
struct BSTNode *constructBST(struct BSTNode *root, int ele)
{
    if (root == NULL)
    {
        struct BSTNode *newNode = malloc(sizeof(struct BSTNode));
        newNode->data = ele;
        newNode->left = NULL;
        newNode->right = NULL;
        return newNode;
    }
    if (root->data > ele)
    {
        root->left = constructBST(root->left, ele);
    }
    if (root->data < ele)
    {

        root->right = constructBST(root->right, ele);
    }

    return root;
}
void printInOrder(struct BSTNode *root)
{
    if (root)
    {
        printInOrder(root->left);
        printf("%d ", root->data);
        printInOrder(root->right);
    }
}
void printPreOrder(struct BSTNode *root)
{
    if (root)
    {
        printf("%d ", root->data);
        printPreOrder(root->left);
        printPreOrder(root->right);
    }
}
void printPostOrder(struct BSTNode *root)
{
    if (root)
    {
        printPostOrder(root->left);
        printPostOrder(root->right);
        printf("%d ", root->data);
    }
}
bool searchElement(struct BSTNode *root, int ele)
{
    if (root)
    {
        if (root->data == ele)
            return true;
        if (root->data > ele)
            return searchElement(root->left, ele);
        if (root->data < ele)
            return searchElement(root->right, ele);
    }
    return false;
}
bool searchElementIterating(struct BSTNode *root, int ele)
{
    struct BSTNode *temp = root;
    while (temp!=NULL)
    {
        if (temp->data == ele)
            return true;
        if (temp->data > ele)
            temp = temp->left;
        else if (temp->data < ele)
            temp = temp->right;
    }
    return false;
}
void main()
{
    struct BSTNode *root = NULL;
    int arr[] = {20, 10, 15, 5, 25, 22, 30};
    for (int i = 0; i < 7; i++)
    {
        // int temp = 0;
        // printf("Enter data : ");
        // scanf("%d", &temp);
        root = constructBST(root, arr[i]);
    }
    printf("PreOrder = ");
    printPreOrder(root);
    printf("\n");
    printf("INOrder = ");
    printInOrder(root);
    printf("\n");
    printf("PostOrder = ");
    printPostOrder(root);
    printf("\n");

    int val;
    printf("Enter element to search : ");
    scanf("%d", &val);
    if (searchElement(root, val))
    {
        printf("%d element is present in following tree\n", val);
    }
    else
        printf("%d element is NOT present in following tree\n", val);
    if (searchElementIterating(root, val))
    {
        printf("%d element is present in following tree Using iterating approach\n", val);
    }
    else
        printf("%d element is NOT present in following tree Using iterating approach\n", val);
}
