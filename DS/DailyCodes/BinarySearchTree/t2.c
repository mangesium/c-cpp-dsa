#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct BSTNode
{
    int data;
    struct BSTNode *left;
    struct BSTNode *right;
};

struct BSTNode *constructBST(struct BSTNode *root, int data)
{
    if (root == NULL)
    {
        struct BSTNode *newNode = malloc(sizeof(struct BSTNode));
        newNode->data = data;
        newNode->left = NULL;
        newNode->right = NULL;
        root = newNode;
        return root;
    }
    if (root->data > data)
    {
        root->left = constructBST(root->left, data);
    }
    if (root->data < data)
    {
        root->right = constructBST(root->right, data);
    }
    return root;
}
void printInOrder(struct BSTNode *root)
{
    if (root)
    {
        printInOrder(root->left);
        printf("%d ", root->data);
        printInOrder(root->right);
    }
}
void printPreOrder(struct BSTNode *root)
{
    if (root)
    {
        printf("%d ", root->data);
        printPreOrder(root->left);
        printPreOrder(root->right);
    }
}
void printPostOrder(struct BSTNode *root)
{
    if (root)
    {
        printPostOrder(root->left);
        printPostOrder(root->right);
        printf("%d ", root->data);
    }
}
bool searchElement(struct BSTNode *root, int ele)
{
    if (root)
    {
        if (root->data == ele)
            return true;
        if (root->data > ele)
            return searchElement(root->left, ele);
        if (root->data < ele)
            return searchElement(root->right, ele);
    }
    return false;
}
bool searchElementIterating(struct BSTNode *root, int ele)
{
    struct BSTNode *temp = root;
    while (temp)
    {
        if (temp->data > ele)
            temp = temp->left;
        else if (temp->data < ele)
            temp = temp->right;
        else
            return true;
    }
    return false;
}

int minTree(struct BSTNode *root)
{
    if (root)
    {
        if (root->left == NULL)
            return root->data;
        return minTree(root->left);
    }
    return -1;
}
int minTreeItr(struct BSTNode *root)
{
    if (root == NULL)
        return -1;
    struct BSTNode *temp = root;
    while (temp->left)
        temp = temp->left;
    return temp->data;
}
int maxTree(struct BSTNode *root)
{
    if (root)
    {
        if (root->right == NULL)
            return root->data;
        return maxTree(root->right);
    }
    return -1;
}
int maxTreeItr(struct BSTNode *root)
{
    if (root == NULL)
        return -1;
    struct BSTNode *temp = root;
    while (temp->right)
        temp = temp->right;
    return temp->data;
}

struct BSTNode *deleteNode(struct BSTNode *root, int k)
{
    if (root == NULL)
        return root;
    if (root->data == k)
    {
        if (root->left == NULL && root->right == NULL)
            return NULL;
        if (root->left != NULL && root->right == NULL)
        {
            struct BSTNode *temp = root->left;
            free(root);
            return temp;
        }
        if (root->left == NULL && root->right != NULL)
        {
            struct BSTNode *temp = root->right;
            free(root);
            return temp;
        }
        if (root->left != NULL && root->right != NULL)
        {
            int temp = maxTree(root->left);
            root->data = temp;
            k = root->data;
            root->left = deleteNode(root->left, k);
        }
    }
    if (root->data > k)
        root->left = deleteNode(root->left, k);
    if (root->data < k)
        root->right = deleteNode(root->right, k);
    return root;
}

bool validateBST(struct BSTNode *root)
{
    if (root)
    {
        if ((root->left != NULL && root->left->data >= root->data) || (root->right != NULL && root->right->data <= root->data))
        {
            return false;
        }
        else
        {
            return validateBST(root->left) && validateBST(root->right);
        }
    }
    return true;
}
int BSTSuccessor(struct BSTNode *root, int data)
{
    int ans = 0;
    while (root)
    {
        if (data >= root->data)
        {
            root = root->right;
        }
        else
        {
            ans = root->data;
            root = root->left;
        }
    }
    return ans;
}
int BSTPredecessor(struct BSTNode *root, int data)
{
    int ans = 0;
    while (root)
    {
        if (data <= root->data)
        {
            root = root->left;
        }
        else
        {
            ans = root->data;
            root = root->right;
        }
    }
    return ans;
}
void kthLargestElement(struct BSTNode *root, int k)
{
    static int cnt = 0;
    if (root)
    {
        kthLargestElement(root->right, k);
        cnt++;
        if (cnt == k)
            printf("%d is the %dth Largest element\n", root->data, k);
        kthLargestElement(root->left, k);
    }
}
void kthSmallestElement(struct BSTNode *root, int k)
{
    static int cnt = 0;
    if (root)
    {
        kthSmallestElement(root->left, k);
        cnt++;
        if (cnt == k)
            printf("%d is the %dth smallest element\n", root->data, k);
        kthSmallestElement(root->right, k);
    }
}
void main()
{
    struct BSTNode *root = NULL;
    int arr[] = {20, 10, 10, 15, 5, 25, 22, 30, 3, 7};
    // int arr[] = {5, 3, 7};
    int size = sizeof(arr) / sizeof(int);
    for (int i = 0; i < size; i++)
    {
        root = constructBST(root, arr[i]);
    }
    // while (root)
    // {
    printf("INOrder = ");
    printInOrder(root);
    printf("\n");
    printf("root of tree = %d\n", root->data);

    // int num;
    // printf("Enter number to delete : ");
    // scanf("%d", &num);
    // // num = 5;
    // root = deleteNode(root, num);

    // printf("INOrder = ");
    // printInOrder(root);
    // printf("\n");
    if (validateBST(root))
        printf("following tree is valid BST\n");
    else
        printf("following tree is Invalid BST\n");

    int num;
    printf("Enter number find Successor : ");
    scanf("%d", &num);
    printf("%d is the Successor of %d\n", BSTSuccessor(root, num), num);
    printf("%d is the Predecessor of %d\n", BSTPredecessor(root, num), num);
    int k;
    printf("Enter number find smallest and largest element : ");
    scanf("%d", &num);
    kthLargestElement(root, num);
    kthSmallestElement(root, num);
    // printf("%d is the %dth smallest element\n",kthSmallestElement(root,num),num);
    // printf("%d is the %dth Largest element\n",kthLargestElement(root,num),num);

    // }
}