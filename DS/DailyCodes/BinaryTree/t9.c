#include <stdio.h>
#include <stdlib.h>
struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
};
struct tree *createnode(int level)
{
    level += 1;
    struct tree *root = malloc(sizeof(struct tree));
    printf("Input to left of root node %d ?(y/Y) : ", root->data);
    char ch;
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
        root->left = createnode(level);
    else
        root->left = NULL;
    getchar();
    printf("Input to right of root node %d ?(y/Y) : ", root->data);
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
        root->right = createnode(level);
    else
        root->right = NULL;
}
void printTree(struct tree *root)
{
    if (root != NULL)
    {
        printf("%d ", root->data);
        printTree(root->left);
        printTree(root->right);
    }
}
void main()
{

   struct tree *root = malloc(sizeof(struct tree));
    printf("Enter Data : ");
    scanf("%d", &root->data);
    printf("Tree Rooted with %d\n", root->data);
    getchar();
    char ch;
    printf("Input to left of root node %d ?(y/Y) : ", root->data);
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
        root->left = createnode(0);
    else
        root->left = NULL;
    getchar();
    printf("Input to right of root node %d ?(y/Y) : ", root->data);
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
        root->right = createnode(0);
    else
        root->right = NULL;
    printTree(root);
}