#include <stdio.h>
#include <stdlib.h>
struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
};
void preOrder(struct tree *root)
{
    if (root != NULL)
    {
        printf("%d ", root->data);
        preOrder(root->left);
        preOrder(root->right);
    }
}
void inOrder(struct tree *root)
{
    if (root != NULL)
    {
        inOrder(root->left);
        printf("%d ", root->data);
        inOrder(root->right);
    }
}
void postOrder(struct tree *root)
{

    if (root != NULL)
    {
        postOrder(root->left);
        postOrder(root->right);
        printf("%d ", root->data);
    }
}
void printTree(struct tree *root)
{
    int k = 1;
    do
    {
        printf("1.PreOrder\n");
        printf("2.InOrder\n");
        printf("3.PostOrder\n");
        printf("4.Exit\n");
        int ch;
        printf("Enter choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
        {
            printf("PreOrder : ");
            preOrder(root);
            printf("\n");
            break;
        }
        case 2:
        {
            printf("InOrder : ");
            inOrder(root);
            printf("\n");
            break;
        }
        case 3:
        {
            printf("PostOrder : ");
            postOrder(root);
            printf("\n");
            break;
        }
        case 4:
        {
            k = 0;
        }
        break;
        default:
            printf("Enter valid choice\n");
            break;
        }

    } while (k == 1);
}

struct tree *buildtreePreIn(int in[], int pre[], int instart, int inend, int prestart, int preend)
{
    if (instart > inend)
        return NULL;

    struct tree *root = (struct tree *)malloc(sizeof(struct tree));
    root->data = pre[prestart];
    int i = instart;
    for (; i <= inend; i++)
    {
        if (in[i] == pre[prestart])
            break;
    }
    root->left = buildtreePreIn(in, pre, instart, i - 1, prestart + 1, i - instart + prestart);
    root->right = buildtreePreIn(in, pre, i + 1, inend, i - instart + prestart + 1, preend);
    return root;
}
struct tree *buildtreePostIn(int in[], int post[], int instart, int inend, int poststart, int postend)
{
    if (instart > inend)
        return NULL;
    struct tree *root = (struct tree *)malloc(sizeof(struct tree));
    root->data = post[postend];
    int i = instart;
    
    for (; i <= inend; i++)
    {
        if (in[i] == post[postend])
            break;
    }
    root->left = buildtreePostIn(in, post, instart, i - 1, poststart, i - instart + poststart-1);
    root->right = buildtreePostIn(in, post, i + 1, inend, i - instart + poststart , postend-1);

    return root;
}
void main()
{
    // int in[] = {6, 4, 7, 2, 5, 8, 1, 9, 3, 11, 10, 12};
    // int pre[] = {1, 2, 4, 6, 7, 5, 8, 3, 9, 10, 11, 12};
    int pre[] = {1, 2, 4, 3, 5, 6, 7, 8, 9, 10};
    int in[] = {4, 2, 1, 3, 6, 8, 7, 10, 9, 5};
    int post[] = {4, 2, 8, 10, 9, 7, 6, 5, 3, 1};

    int size = sizeof(in) / sizeof(in[0]);
    // struct tree *root = buildtreePreIn(in, pre, 0, size - 1, 0, size - 1);
    struct tree *root = buildtreePostIn(in, post, 0, size - 1, 0, size - 1);
    printTree(root);
}