#include <stdio.h>
#include <stdlib.h>
#include <conio.h>  
struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
};

void preOrder(struct tree *root){
    if(root!=NULL){
        printf("%d ",root->data);
        preOrder(root->left);
        preOrder(root->right);
    }
}
void inOrder(struct tree *root){
    if(root!=NULL){
        inOrder(root->left);
        printf("%d ",root->data);
        inOrder(root->right);
    }
}

void postOrder(struct tree *root){
    
    if(root!=NULL){
        postOrder(root->left);
        postOrder(root->right);
        printf("%d ",root->data);
    }
}

void printTree(struct tree *root)
{
    int k = 1;
    do
    {
        printf("1.PreOrder\n");
        printf("2.InOrder\n");
        printf("3.PostOrder\n");
        printf("4.Exit\n");
        int ch;
        printf("Enter choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
        {
            printf("PreOrder : ");
            preOrder(root);
            printf("\n");
            break;
        }
        case 2:
        {
            printf("InOrder : ");
            inOrder(root);
            printf("\n");
            break;
        }
        case 3:
        {
            printf("PostOrder : ");
            postOrder(root);
            printf("\n");
            break;
        }
        case 4:
            k = 2;
            break;
        default:
            printf("Enter valid choice\n");
            break;
        }
        
    } while (k == 1);
}

struct tree *createnode(int level)
{

    level += 1;
    struct tree *newnode = malloc(sizeof(struct tree));
    printf("Enter data : ");
    scanf("%d", &(*newnode).data);
    char ch;
    printf("Continue to put data in left of %d node of level-%d ? \n",newnode->data,level);
    getchar();
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
    {
        newnode->left = createnode(level);
    }
    else
    {
        newnode->left = NULL;
    }
    printf("Continue to put data in right of %d node of level-%d ? \n",newnode->data,level);
    getchar();
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
    {
       newnode->right = createnode(level);
    }
    else
    {
        newnode->right = NULL;
    }
}
int sumofBtree(struct tree* root){
    if(root!=NULL){
        return root->data+sumofBtree(root->left)+sumofBtree(root->right);
    }
    return 0;
}
int max(int left,int right){
    if(left>right)
        return left;
    return right;
}
int heightofBtree(struct tree* root){

    if(root==NULL)
        return -1;
    int left = heightofBtree(root->left);
    int right = heightofBtree(root->right);

    return max(left ,right) + 1;
}
int sizeofBT(struct tree* root){
    if(root!=NULL){
        return 1+sizeofBT(root->left)+sizeofBT(root->right);
    }
    return 0;
}
int checkIdeal(struct tree* rootleft,struct tree* rootright){
    if(rootleft!=NULL&&rootright!=NULL)
        return 1;
    return 0;
}
int idealnode(struct tree* root){
    if(root==NULL)
        return 0;

    return checkIdeal(root->left,root->right)+idealnode(root->left)+idealnode(root->right);
}
struct tree* random(struct tree* rootleft,struct tree* rootright){
    if(rootleft!=NULL&&rootright!=NULL){
        if(rand()%2==0)
            return rootleft;
        else
            return rootright;
    }else if(rootleft!=NULL)
        return rootleft;
    else if(rootright!=NULL)
        return rootright;
    else 
        return NULL;
}
int addRandom(struct tree* root){
    if(root==NULL)
        return 0;
    printf("root->data = %d in addRandom\n",root->data);
    return root->data+addRandom(random(root->left,root->right));
}
void main()
{

    struct tree *root = (struct tree *)malloc(sizeof(struct tree));
    printf("Enter data int root : ");
    scanf("%d", &(*root).data);
    printf("            Tree rooted with %d element\n", root->data);
    char ch;
    printf("Continue to put data in left of tree ? \n");
    getchar();
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
    {
        root->left = createnode(0);
    }
    else
    {
        root->left = NULL;
    }
    printf("Continue to put data in right of tree ? \n");
    getchar();
    scanf("%c", &ch);
    if (ch == 'y' || ch == 'Y')
    {
        root->right = createnode(0);
    }
    else
    {
        root->right = NULL;
    }

    printTree(root);
    printf("Nodes present in tree = %d\n",sizeofBT(root));
    printf("Sum of Nodes present in tree = %d\n",sumofBtree(root));
    printf("max tree height = %d\n",heightofBtree(root));
    printf("Ideal Nodes in tree = %d\n",idealnode(root));
    printf("Random element addition = %d\n",addRandom(root));
    
}