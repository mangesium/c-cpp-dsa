#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
};
void preOrder(struct tree *root)
{
    if (root != NULL)
    {
        printf("%d ", root->data);
        preOrder(root->left);
        preOrder(root->right);
    }
}
void inOrder(struct tree *root)
{
    if (root != NULL)
    {
        inOrder(root->left);
        printf("%d ", root->data);
        inOrder(root->right);
    }
}
void postOrder(struct tree *root)
{

    if (root != NULL)
    {
        postOrder(root->left);
        postOrder(root->right);
        printf("%d ", root->data);
    }
}
void printTree(struct tree *root)
{
    int k = 1;
    do
    {
        printf("1.PreOrder\n");
        printf("2.InOrder\n");
        printf("3.PostOrder\n");
        printf("4.Exit\n");
        int ch;
        printf("Enter choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
        {
            printf("PreOrder : ");
            preOrder(root);
            printf("\n");
            break;
        }
        case 2:
        {
            printf("InOrder : ");
            inOrder(root);
            printf("\n");
            break;
        }
        case 3:
        {
            printf("PostOrder : ");
            postOrder(root);
            printf("\n");
            break;
        }
        case 4:
        {
            k = 0;
        }
        break;
        default:
            printf("Enter valid choice\n");
            break;
        }

    } while (k == 1);
}

struct tree *buildtree(int *in, int *pre, int start, int end, int idx)
{
    if (start > end)
        return NULL;
    struct tree *root = malloc(sizeof(struct tree));
    root->data = pre[idx];
    int i = 0, count = 0;
    for (i = start; i <= end; i++)
    {
        if (in[i] == pre[idx])
            break;
        count++;
    }
    root->left = buildtree(in, pre, start, i - 1, idx + 1);
    root->right = buildtree(in, pre, i + 1, end, idx + count + 1);

    return root;
}
void main()
{
    // int in[] = {2, 5, 4, 1, 6, 3, 8, 7};
    // int pre[] = {1, 2, 4, 5, 3, 6, 7, 8};
    int in[] = {6, 4, 7, 2, 5, 8, 1, 9, 3, 11, 10, 12};
    int pre[] = {1, 2, 4, 6, 7, 5, 8, 3, 9, 10, 11, 12};
    int size = sizeof(in) / sizeof(in[0]);
    struct tree *root = buildtree(in, pre, 0, size - 1, 0);
    printTree(root);
}