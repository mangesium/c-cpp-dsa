#include <stdio.h>
#include <stdlib.h>
struct node
{
    int data;
    struct node *left;
    struct node *right;
};
struct node *createnode()
{
    struct node *root = malloc(sizeof(struct node));
    printf("Enter data : ");
    scanf("%d", &root->data);
    root->left = NULL;
    root->right = NULL;
    char flag = 'f';
    printf("You wants to put data in left of %d section enter 'y'/'Y' else 'n'/'N' : ",root->data);
    getchar();
    scanf("%c", &flag);
    if (flag == 'y' || flag == 'Y')
    {
        root->left=createnode();
    }
    printf("You wants to put data in Right of %d section enter 'y'/'Y' else 'n'/'N' : ",root->data);
    getchar();
    scanf("%c", &flag);
    if (flag == 'y' || flag == 'Y')
    {
        root->right=createnode();
    }
    return root;
}
void preorder(struct node *root)
{
    if (root != NULL)
    {
        printf("%d ", root->data);
        preorder(root->left);
        preorder(root->right);
    }
}
void inorder(struct node *root)
{
    if (root != NULL)
    {
        
        inorder(root->left);
        printf("%d ", root->data);
        inorder(root->right);
    }
}
void postorder(struct node *root)
{
    if (root != NULL)
    {
        
        postorder(root->left);
        postorder(root->right);
        printf("%d ", root->data);
    }
}

int sumOfTree(struct node* root){
    if(root!=NULL){
        return root->data+sumOfTree(root->left)+sumOfTree(root->right);
    }
}
// char majorSide(struct node* root){
//     if(root!=NULL){
//         int leftsum= majorSide(root->left)+majorSide(root->right)+root->data;
//         int rightsum = majorSide(root->right)+majorSide(root->left)+root->data;
//         if(leftsum>rightsum)
//             return 'l';
//         if(leftsum==rightsum)
//             return 'E';
//         return 'r';
//     }
// }
void main()
{

    struct node *root = createnode();
    printf(" Preorder = ");
    preorder(root);
    printf("\n");
    printf(" Inorder = ");
    inorder(root);
    printf("\n");
    printf(" Postorder = ");
    postorder(root);
    printf("\n");

    printf("sum of Tree = %d\n",sumOfTree(root));
    if(sumOfTree(root->left)>sumOfTree(root->right))
        printf("Left side is major = %d\n",sumOfTree(root->left));
    else
        printf("Right side is major= %d\n",sumOfTree(root->right));

    // printf("Major Side of tree = %c\n",majorSide(root));
}