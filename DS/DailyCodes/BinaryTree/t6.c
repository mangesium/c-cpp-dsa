#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
};
void preOrder(struct tree *root)
{
    if (root != NULL)
    {
        printf("%d ", root->data);
        preOrder(root->left);
        preOrder(root->right);
    }
}
void inOrder(struct tree *root)
{
    if (root != NULL)
    {
        inOrder(root->left);
        printf("%d ", root->data);
        inOrder(root->right);
    }
}
void postOrder(struct tree *root)
{

    if (root != NULL)
    {
        postOrder(root->left);
        postOrder(root->right);
        printf("%d ", root->data);
    }
}
void printTree(struct tree *root)
{
    int k = 1;
    do
    {
        printf("1.PreOrder\n");
        printf("2.InOrder\n");
        printf("3.PostOrder\n");
        printf("4.Exit\n");
        int ch;
        printf("Enter choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
        {
            printf("PreOrder : ");
            preOrder(root);
            printf("\n");
            break;
        }
        case 2:
        {
            printf("InOrder : ");
            inOrder(root);
            printf("\n");
            break;
        }
        case 3:
        {
            printf("PostOrder : ");
            postOrder(root);
            printf("\n");
            break;
        }
        case 4:
        {
            k = 0;
        }
        break;
        default:
            printf("Enter valid choice\n");
            break;
        }

    } while (k == 1);
}

struct tree *buildtree(int *in, int *post, int start, int end, int idx)
{

    if (start > end)
        return NULL;
    struct tree *root = malloc(sizeof(struct tree));
    root->data = post[idx];
    int i = start, count = 0;

    for (i = start; i <= end; i++)
    {
        if (in[i] == post[idx])
        {
            break;
        }
        count++;
    }
    if (i > end)
    {
        root->data = in[start];
        root->left = NULL;
        root->right = NULL;
        return root;
    }

    // printf("Left start = %d , end = %d ,idx = %d \n", start, i - 1, idx - count - 1);
    // root->left = buildtree(in, post, start, i - 1, idx - count - 1);

    // printf("Left start = %d , end = %d ,idx = %d \n", start, i - 1, end-i);
    // root->left = buildtree(in, post, start, i - 1, end-i);

    printf("Left start = %d , end = %d ,idx = %d \n", start, i - 1, i - 1);
    root->left = buildtree(in, post, start, i - 1, i - 1);

    printf("Right start = %d , end = %d ,idx = %d \n", i + 1, end, idx - 1);
    root->right = buildtree(in, post, i + 1, end, idx - 1);

    return root;
}
void main()
{
    // int in[] = {4, 2, 5, 1, 6, 3, 7};
    // int post[] = {4, 5, 2, 6, 7, 3, 1};
    // int in[] = {4, 2, 5, 1, 7, 3, 6};
    // int post[] = {4, 5, 2, 7, 6, 3, 1};
    // int in[] = {6, 4, 7, 2, 5, 8, 1, 9, 3, 11, 10, 12};
    // int post[] = {6, 7, 4, 8, 5, 2, 9, 11, 12, 10, 3, 1};
    // int in[] = {3,2,4,1,5,7,6};
    // int post[] = {3,4,2,7,6,5,1};
    int in[] = {4, 2, 1, 3, 6, 8, 7, 10, 9, 5};
    int post[] = {4, 2, 8, 10, 9, 7, 6, 5, 3, 1};
    int size = sizeof(in) / sizeof(in[0]);
    printf("size = %d\n", size);
    struct tree *root = buildtree(in, post, 0, size - 1, size - 1);
    printTree(root);
}