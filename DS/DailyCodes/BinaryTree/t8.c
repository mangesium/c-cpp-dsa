#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
struct tree
{
    int data;
    struct tree *left;
    struct tree *right;
};

struct queue
{
    struct tree *data;
    struct queue *next;
};
struct queue *front = NULL;
struct queue *rear = NULL;

bool isEmptyQ()
{
    if (front == NULL)
        return true;
    return false;
}
void enqueue(struct tree *root)
{

    struct queue *newnode = malloc(sizeof(struct queue));
    newnode->data = root;
    newnode->next = NULL;
    if (isEmptyQ())
    {
        front = rear = newnode;
    }
    else
    {
        rear->next = newnode;
        rear = newnode;
    }
}
struct tree *dequeue()
{
    if (isEmptyQ())
    {
        printf("Queue is Empty\n");
    }
    else
    {
        struct queue *temp = front;
        struct tree *item = temp->data;
        if (front == rear)
        {
            front = rear = NULL;
        }
        else
        {
            front = front->next;
        }
        free(temp);
        return item;
    }
}

void levelOrder1(struct tree *root)
{

    struct tree *temp = root;
    enqueue(temp);
    while (!isEmptyQ())
    {
        temp = dequeue();
        printf("%d ", temp->data);
        if (temp->left)
            enqueue(temp->left);
        if (temp->right)
            enqueue(temp->right);
    }
}
int sizeofQueue()
{
    if (isEmptyQ())
        return 0;
    struct queue *temp = front;
    int count = 0;
    while (temp != rear)
    {
        count++;
        temp = temp->next;
    }
    return count + 1;
}
void levelOrdernull(struct tree *root)
{

    struct tree *temp = root;
    enqueue(temp);
    enqueue(NULL);
    while (sizeofQueue() > 1)
    {
        temp = dequeue();
        if (temp == NULL)
        {
            printf("\n");
            enqueue(NULL);
        }
        else
        {
            printf("%d ", temp->data);
            if (temp->left)
                enqueue(temp->left);
            if (temp->right)
                enqueue(temp->right);
        }
    }
}
void levelOrderWitoutNull(struct tree *root)
{
    struct tree *temp = root;
    enqueue(temp);
    while (!isEmptyQ())
    {
        int size = sizeofQueue();
        for (int i = 0; i < size; i++)
        {
            temp = dequeue();
            printf("%d ", temp->data);
            if (temp->left)
                enqueue(temp->left);
            if (temp->right)
                enqueue(temp->right);
        }
        printf("\n");
    }
}
void levelOrderLeftView(struct tree *root)
{
    struct tree *temp = root;
    enqueue(temp);
    while (!isEmptyQ())
    {
        int size = sizeofQueue();
        for (int i = 0; i < size; i++)
        {
            temp = dequeue();
            if (i == 0)
                printf("%d ", temp->data);
            if (temp->left)
                enqueue(temp->left);
            if (temp->right)
                enqueue(temp->right);
        }
        printf("\n");
    }
}
void levelOrderRightView(struct tree *root)
{
    struct tree *temp = root;
    enqueue(temp);
    while (!isEmptyQ())
    {
        int size = sizeofQueue();
        for (int i = 0; i < size; i++)
        {
            temp = dequeue();
            if (i == size - 1)
                printf("%d ", temp->data);
            if (temp->left)
                enqueue(temp->left);
            if (temp->right)
                enqueue(temp->right);
        }
        printf("\n");
    }
}

struct stack
{
    struct tree *data;
    struct stack *next;
};
struct stack *top = NULL;

bool isEmpty()
{
    if (top == NULL)
        return true;
    return false;
}
void push(struct tree *root)
{

    struct stack *newnode = malloc(sizeof(struct stack));
    newnode->data = root;
    newnode->next = top;
    top = newnode;
}
struct tree *pop()
{

    if (isEmpty())
    {
        printf("Stack isEmpty\n");
        return NULL;
    }
    struct stack *temp = top;
    struct tree *item = temp->data;
    top = top->next;
    free(temp);
    return item;
}
void iterativeInOrder(struct tree *root)
{

    struct tree *temp = root;
    while (!isEmpty() || temp != NULL)
    {
        if (temp)
        {
            push(temp);
            temp = temp->left;
        }
        else
        {
            temp = pop();
            printf("%d ", temp->data);
            temp = temp->right;
        }
    }
}

void preOrder(struct tree *root)
{
    if (root != NULL)
    {
        printf("%d ", root->data);
        preOrder(root->left);
        preOrder(root->right);
    }
}
void inOrder(struct tree *root)
{
    if (root != NULL)
    {
        inOrder(root->left);
        printf("%d ", root->data);
        inOrder(root->right);
    }
}
void postOrder(struct tree *root)
{

    if (root != NULL)
    {
        postOrder(root->left);
        postOrder(root->right);
        printf("%d ", root->data);
    }
}
void printTree(struct tree *root)
{
    int k = 1;
    do
    {
        printf("1.PreOrder\n");
        printf("2.InOrder\n");
        printf("3.PostOrder\n");
        printf("4.IterativeInOrder\n");
        printf("5.LevelOrder\n");
        printf("6.Level Order Using NULL approch\n");
        printf("7.Level Order Without Using NULL approch\n");
        printf("8.Level Order Left View\n");
        printf("9.Level Order Right View\n");
        printf("10.Exit\n");
        int ch;
        printf("Enter choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
        {
            printf("PreOrder : ");
            preOrder(root);
            printf("\n");
            break;
        }
        case 2:
        {
            printf("InOrder : ");
            inOrder(root);
            printf("\n");
            break;
        }
        case 3:
        {
            printf("PostOrder : ");
            postOrder(root);
            printf("\n");
            break;
        }
        case 4:
        {
            printf("Iterative InOrder : ");
            iterativeInOrder(root);
            printf("\n");
        }
        break;
        case 5:
        {
            printf("Level Order : ");
            levelOrder1(root);
            printf("\n");
        }
        break;
        case 6:
        {
            printf("Level Order using NULL approch : \n");
            levelOrdernull(root);
            while (isEmptyQ())
            {
                dequeue();
            }
            printf("\n");
        }
        break;
        case 7:
        {
            dequeue();
            printf("Level Order Without using NULL approch : \n");
            levelOrderWitoutNull(root);
        }
        break;
        case 8:
        {
            printf("Level Order Left View : \n");
            levelOrderLeftView(root);
            printf("\n");
        }
        break;
        case 9:
        {
            printf("Level Order Right View : \n");
            levelOrderRightView(root);
            printf("\n");
        }
        break;
        case 10:
        {
            k = 0;
        }
        break;
        default:
            printf("Enter valid choice\n");
            break;
        }
    } while (k == 1);
}

struct tree *buildTreePreIn(int in[], int pre[], int instart, int inend, int prestart, int preend)
{
    if (instart > inend)
        return NULL;
    struct tree *root = malloc(sizeof(struct tree));
    root->data = pre[prestart];
    int i = instart;
    for (; i <= inend; i++)
    {
        if (in[i] == pre[prestart])
            break;
    }

    root->left = buildTreePreIn(in, pre, instart, i - 1, prestart + 1, i - instart + prestart);
    root->right = buildTreePreIn(in, pre, i + 1, inend, i - instart + prestart + 1, preend);
    return root;
}
struct tree *buildTreePostIn(int in[], int post[], int instart, int inend, int poststart, int postend)
{
    if (instart > inend)
        return NULL;
    struct tree *root = malloc(sizeof(struct tree));
    root->data = post[postend];
    int i = instart;
    for (; i <= inend; i++)
    {
        if (in[i] == root->data)
            break;
    }
    root->left = buildTreePostIn(in, post, instart, i - 1, poststart, i - instart + poststart - 1);
    root->right = buildTreePostIn(in, post, i + 1, inend, i - instart + poststart, postend - 1);
    return root;
}
void main()
{
    // int pre[] = {1, 2, 4, 3, 5, 6, 7, 8, 9, 10};
    // int in[] = {4, 2, 1, 3, 6, 8, 7, 10, 9, 5};
    // int post[] = {4, 2, 8, 10, 9, 7, 6, 5, 3, 1};
    int pre[] = {1, 2, 4, 3, 5, 7, 6};
    int in[] = {4, 2, 1, 7, 5, 3, 6};
    int post[] = {4, 2, 7, 5, 6, 3, 1};

    int size = sizeof(in) / sizeof(in[0]);
    struct tree *root1 = buildTreePreIn(in, pre, 0, size - 1, 0, size - 1);
    printf("Building Tree Using Pre an In Order\n");
    printTree(root1);
    struct tree *root2 = buildTreePostIn(in, post, 0, size - 1, 0, size - 1);
    printf("Building Tree Using Post an In Order\n");
    printTree(root2);
}