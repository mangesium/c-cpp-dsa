#include <stdio.h>
int top = -1, size = 0, flag = 0;

int push(int *arr, int n)
{
    if (top == size - 1)
    {
        flag = 0;
        return -1;
    }
    else
    {
        arr[++top] = n;
        flag = 1;
        return arr[top];
    }
}
int pop(int *arr)
{
    if (top == -1)
    {
        flag = 0;
        return -1;
    }
    else
    {
        int val = arr[top];
        top--;
        flag = 1;
        return val;
    }
}
void printstack(int *arr)
{
    if (top == -1)
    {
        printf("Stack Underflow\n");
        return;
    }
    for (int i = top; i >= 0; i--)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
void main()
{
    printf("Enter Size of stack : ");
    scanf("%d", &size);
    int arr[size];
    char ch;
    do
    {
        printf("1.PUSH\n");
        printf("2.POP\n");
        printf("3.PRINT STACK\n");
        printf("Enter Choice : ");
        int num;
        scanf("%d", &num);
        switch (num)
        {
        case 1:
        {

            int n;
            printf("Enter number to push : ");
            scanf("%d", &n);
            n = push(arr, n);
            if (flag != 0)
                printf("%d is pushed in stack\n", n);
            else
                printf("Stack Overflow\n");
        }
        break;
        case 2:
        {
            int val = pop(arr);
            if (flag != 0)
                printf("%d is popped\n", val);
            else
                printf("Stack Underflow\n");
        }
        break;
        case 3:
        {
            printstack(arr);
        }
        default:
            break;
        }
        getchar();
        printf("Enter y/Y to continue : ");
        scanf("%c", &ch);
    } while (ch != 'n');
}
