#include <stdio.h>
#include <stdlib.h>
typedef struct demo
{
    int data;
    struct demo *next;
} d;
d *top = NULL;
int flag = 0;
d *creatnode(int n)
{
    d *newnode = malloc(sizeof(d));
    newnode->data = n;
    newnode->next = NULL;
    return newnode;
}
int push(int n)
{
    d *tmp = creatnode(n);
    if (tmp == NULL)
    {
        flag = 0;
        return -1;
    }
    else if (top == NULL)
    {
        top = tmp;
    }
    else
    {
        d *temp = top;
        while (temp->next != NULL)
            temp = temp->next;
        temp->next = tmp;
    }
    flag = 1;
    return n;
}
int pop()
{
    int val;
    if (top == NULL)
    {
        flag = 0;
        return -1;
    }
    else if (top->next == NULL)
    {
        val = top->data;
        free(top);
        top = NULL;
    }
    else
    {

        d *temp = top;
        while (temp->next->next != NULL)
            temp = temp->next;
        val = temp->next->data;
        free(temp->next);
        temp->next = NULL;
    }
    flag = 1;
    return val;
}
int printstack()
{
    d *temp = top;
    if (temp == NULL)
    {
        printf("Stack is Empty\n");
        return -1;
    }
    else
    {
        while (temp != NULL)
        {
            if (temp->next != NULL)
                printf("%d<-", temp->data);
            else
                printf("%d\n", temp->data);
            temp = temp->next;
        }
    }
}
void main()
{

    char ch;
    do
    {
        printf("1.PUSH\n");
        printf("2.POP\n");
        printf("3.PRINT STACK\n");
        printf("Enter Choice : ");
        int num;
        scanf("%d", &num);
        switch (num)
        {
        case 1:
        {
            for (int i = 0; i < 5; i++)
            {
                int n;
                printf("Enter number to push : ");
                scanf("%d", &n);
                n = push(n);
                if (flag != 0)
                    printf("%d is pushed in stack\n", n);
                else
                    printf("Stack Overflow\n");
            }
        }
        break;
        case 2:
        {
            int val = pop();
            if (flag != 0)
                printf("%d is popped\n", val);
            else
                printf("Stack Underflow\n");
        }
        break;
        case 3:
        {
            printstack();
        }
        default:
            break;
        }
        getchar();
        printf("Enter y/Y to continue : ");
        scanf("%c", &ch);
    } while (ch != 'n');
}