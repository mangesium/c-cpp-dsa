#include <stdio.h>
/**********************************************/
// int power(int a,int n){
//     int ans=1;
//     while(n){
//         ans*=a;
//         n--;
//     }
//     return ans;
// }
/***********************************************/
// int power(int a,int n){
//     static int i=0;
//     printf("stack = %d\n",++i);
//     if(n==0)
//         return 1;
//     return a*power(a,--n);
// }
/***********************************************/
// int power(int a,int n){
//     static int i=0;
//     if(n==0)
//         return 1;
//     printf("stack = %d\n",++i);
//     if(n%2==0)
//         return power(a,n/2)*power(a,n/2);
//     else
//         return  power(a,n/2)*power(a,n/2)*a;
// }
/***************************************************/

int power(int a, int n)
{
    static int i = 0;
    if (n == 0)
        return 1;
    printf("stack = %d\n", ++i);
    int pow = power(a, n / 2);
    if (n % 2 == 0)
        return pow * pow;
    else
        return pow * pow * a;
}

// int power(int a, int n)
// {
//     static int i = 0, ans = 1;
//     if (n <0)
//         return ans;
//     i+=1;
//     printf("stack = %d\n", i);
//     int last_bit = (n & 1);
//     printf("last_bit = %d\n",last_bit);
//     // Check if current LSB
//     // is set
//     if (last_bit)
//     {
//         ans = ans * a;
//     }

//     a = a * a;

//     // Right shift
//     n = n >> 1;
// }

void main()
{
    int a = 0, n = 0;
    printf("Enter number : ");
    scanf("%d", &a);
    printf("Enter number : ");
    scanf("%d", &n);
    printf("%d to the power %d = %d\n", a, n, power(a, n));
}