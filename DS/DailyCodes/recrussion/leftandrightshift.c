#include <Stdio.h>
void main()
{
    printf("Left shift = %ld\n", 3 << 1);  // 3*2^1=6
    printf("Left shift = %ld\n", 3 << 2);  // 3*2^2=3*4=12
    printf("Right shift = %ld\n", 8 >> 1); // 8/2^1=4
    printf("Right shift = %ld\n", 8 >> 2); // 8/2^2=8/4=2
}