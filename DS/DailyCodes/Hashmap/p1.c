#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HASH_SIZE 100

// Define the structure for the hash map's nodes
typedef struct Node {
    char *key;
    char *value;
    struct Node *next;
} Node;

// Define the structure for the hash map
typedef struct HashMap {
    Node *buckets[HASH_SIZE];
} HashMap;

// Hash function
unsigned int hash(const char *key) {
    unsigned int hash = 0;
    while (*key) {
        hash = (hash * 31) + *key;
        key++;
    }
    return hash % HASH_SIZE;
}

// Initialize a new hash map
HashMap *createHashMap() {
    HashMap *map = (HashMap *)malloc(sizeof(HashMap));
    for (int i = 0; i < HASH_SIZE; i++) {
        map->buckets[i] = NULL;
    }
    return map;
}

// Insert a key-value pair into the hash map
void insert(HashMap *map, const char *key, const char *value) {
    unsigned int index = hash(key);
     
    // Check if the key already exists in the bucket
    Node *current = map->buckets[index];
    while (current) {
        if (strcmp(current->key, key) == 0) {
            // Update the value and return
            free(current->value);
            current->value = strdup(value);
            return;
        }
        current = current->next;
    }
    
    Node *newNode = (Node *)malloc(sizeof(Node));
    newNode->key = strdup(key);
    newNode->value = strdup(value);
    newNode->next = NULL;
    
    if (map->buckets[index] == NULL) {
        map->buckets[index] = newNode;
    } else {
        Node *current = map->buckets[index];
        while (current->next) {
            current = current->next;
        }
        current->next = newNode;
    }
}

// Retrieve the value associated with a key
const char *get(HashMap *map, const char *key) {
    unsigned int index = hash(key);
    
    Node *current = map->buckets[index];
    while (current) {
        if (strcmp(current->key, key) == 0) {
            return current->value;
        }
        current = current->next;
    }
    
    return NULL;
}

// Clean up and free the hash map's memory
void freeHashMap(HashMap *map) {
    for (int i = 0; i < HASH_SIZE; i++) {
        Node *current = map->buckets[i];
        while (current) {
            Node *temp = current;
            current = current->next;
            free(temp->key);
            free(temp->value);
            free(temp);
        }
    }
    free(map);
}

int main() {
    HashMap *map = createHashMap();

    insert(map, "name", "John");
    insert(map, "name", "mangesh");
    insert(map, "age", "25");
    insert(map, "city", "New York");

    printf("Name: %s\n", get(map, "name"));
    printf("Age: %s\n", get(map, "age"));
    printf("City: %s\n", get(map, "city"));

    freeHashMap(map);

    return 0;
}
