#include <stdio.h>
#include <stdlib.h>
typedef struct Demo
{
    int data;
    struct Demo *next;
} d;
d *front = NULL;
d *rear = NULL;
int flag = 0, size = 0;
d *creatnode()
{
    d *newnode = malloc(sizeof(d));
    printf("Enter Data : ");
    scanf("%d", &newnode->data);
    newnode->next = NULL;
    return newnode;
}
int nodecount()
{
    int count = 0;
    if (front == NULL)
        return 0;
    d *temp = front;
    while (temp->next != front)
    {
        count++;
        temp = temp->next;
    }
    return count + 1;
}
void printqueue()
{
    if (front == NULL)
    {
        printf("Empty Queue\n");
        return;
    }
    d *temp = front;
    while (temp->next != front)
    {
        printf("| %d |->", temp->data);
        temp = temp->next;
    }
    printf("| %d |\n", temp->data);
}
int enqueue()
{
    if (size == nodecount())
    {
        flag = 0;
        return -1;
    }
    d *temp = creatnode();
    if (front == NULL)
    {
        front = temp;
        rear = temp;
        rear->next = front;
    }
    else
    {
        rear->next = temp;
        rear = temp;
        rear->next = front;
    }
    flag = 1;
    return rear->data;
}
int dequeue()
{
    int val=front->data;
    if (front == NULL)
    {
        flag = 0;
        return -1;
    }
    else if (front->next == front)
    {
        free(front);
        front = rear = NULL;
    }
    else
    {
        front = front->next;
        free(rear->next);
        rear->next = front;
    }
    flag = 1;
    return val;
}
int frontt()
{
    if (front == NULL)
    {
        flag = 0;
        return -1;
    }
    else
    {
        flag = 1;
        return front->data;
    }
}
void main()
{
    printf("Enter Size of Queue : ");
    scanf("%d", &size);
    char ch;
    do
    {
        printf("1.Enqueue\n");
        printf("2.Dequeue\n");
        printf("3.Front\n");
        printf("4.Printqueue\n");
        int num = 0;
        int val;
        getchar();
        printf("Enter Choice : ");
        scanf("%d", &num);
        switch (num)
        {

        case 1:
        {
            val = enqueue();
            if (flag != 0)
                printf("%d is enqued\n", val);
            else
                printf("Queue is Full\n");
        }
        break;
        case 2:
        {
            val = dequeue();
            if (flag != 0)
                printf("%d is dequed\n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 3:
        {
            val = frontt();
            if (flag != 0)
                printf("%d is at front \n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 4:
        {
            printqueue();
        }
        break;
        default:
            break;
        }
        getchar();
        printf("Enter n/N to Exit : ");
        scanf("%c", &ch);
    } while (ch != 'n');
}