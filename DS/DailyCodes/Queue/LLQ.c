#include <stdio.h>
#include <stdlib.h>
typedef struct Demo
{
    int data;
    struct Demo *next;
} d;
d *front = NULL;
d *rear = NULL;
int flag = 0, size = 0,count=0;
d *creatnode()
{
    d *newnode = malloc(sizeof(d));
    printf("Enter Data : ");
    scanf("%d", &newnode->data);
    newnode->next = NULL;
    return newnode;
}
int countt()
{
    if (front == NULL)
    {
        flag = 0;
        return 0;
    }
    flag = 1;
    int count = 0;
    d *temp = front;
    while (temp != NULL)
    {
        count++;
        temp = temp->next;
    }
    return count;
}
int enqueue()
{

    if (count == size)
    {
        flag = 0;
        return -1;
    }
    d *newnode = creatnode();
    int val = newnode->data;
    flag = 1;
    if (front == NULL)
    {
        front = newnode;
        rear = newnode;
    }
    else
    {
        rear->next = newnode;
        rear = newnode;
    }
    count++;
    return val;
}
int dequeue()
{
    if (front == NULL)
    {
        flag = 0;
        return -1;
    }
    int val = front->data;
    flag = 1;
    if (front == rear)
    {
        count=0;
        free(front);
        front = NULL;
        rear = NULL;
    }
    else
    {
        d *temp = front;
        front = front->next;
        free(temp);
    }
    return val;
}
int frontt()
{
    if (front == NULL)
    {
        flag = 0;
        return -1;
    }
    flag = 1;
    return front->data;
}
void printqueue()
{
    if (front == NULL)
    {
        printf("Empty Queue\n");
    }
    else
    {
        d *temp = front;
        while (temp != NULL)
        {
            if (temp->next != NULL)
                printf("|%d|->", temp->data);
            else
                printf("|%d|", temp->data);
            temp = temp->next;
        }
        printf("\n");
    }
}
void main()
{
    printf("Enter Size of Queue : ");
    scanf("%d", &size);
    char ch;
    do
    {
        printf("1.Enqueue\n");
        printf("2.Dequeue\n");
        printf("3.Front\n");
        printf("4.Printqueue\n");
        int num = 0;
        int val;
        getchar();
        printf("Enter Choice : ");
        scanf("%d", &num);
        switch (num)
        {

        case 1:
        {
            val = enqueue();
            if (flag != 0)
                printf("%d is enqued\n", val);
            else
                printf("Queue is Full\n");
        }
        break;
        case 2:
        {
            val = dequeue();
            if (flag != 0)
                printf("%d is dequed\n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 3:
        {
            val = frontt();
            if (flag != 0)
                printf("%d is at front \n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 4:
        {
            printqueue();
        }
        break;
        default:
            break;
        }
        getchar();
        printf("Enter n/N to Exit : ");
        scanf("%c", &ch);
    } while (ch != 'n');
}