#include <stdio.h>
int front = -1;
int rear = -1, size = 0, flag = 0;
int isfull()
{
    if (rear == size - 1)
        return 1;
    return 0;
}
int isempty()
{
    if (front == -1)
        return 1;
    return 0;
}
int dequeue(int *arr)
{
    if (isempty())
    {
        flag = 0;
        return -1;
    }
    int val = 0;
    val = arr[front];
    if (front == rear)
    {
        front = -1;
        rear = -1;
    }
    else
    {
        front++;
    }
    flag = 1;
    return val;
}
int enqueue(int *arr, int *pri)
{
    if (isfull())
    {
        flag = 0;
        return -1;
    }
    int data, prio;
    printf("Enter Data : ");
    scanf("%d", &data);
    prio = -1;
    while (prio < 0 || prio > 5)
    {
        printf("Enter priority between 0 to 5 : ");
        scanf("%d", &prio);
    }
    rear++;
    if (front == -1)
    {
        front++;
        arr[front] = data;
        pri[front] = prio;
    }
    else
    {
        int i = front;
        for (; i < rear; i++)
        {
            if (prio > pri[i])
            {
                break;
            }
        }
        if (i < rear)
        {
            int j = rear;
            for (; j >= i; j--)
            {
                arr[j] = arr[j - 1];
                pri[j] = pri[j - 1];
            }
            arr[i] = data;
            pri[i] = prio;
        }
        else
        {
            arr[rear] = data;
            pri[rear] = prio;
        }
    }
    flag = 1;
    return data;
}
int frontt(int *arr)
{
    if (isempty())
    {
        flag = 0;
        return -1;
    }
    flag = 1;
    return arr[front];
}
void printqueue(int *arr, int *pri)
{
    if (isempty())
    {
        printf("Empty Queue\n");
        return;
    }
    for (int i = front; i < rear; i++)
        printf("| %d | P:%d |-> ", arr[i], pri[i]);
    printf("| %d | P:%d |\n ", arr[rear], pri[rear]);
}
void main()
{
    printf("Enter Size of Queue : ");
    scanf("%d", &size);
    int arr[size];
    int pri[size];
    char ch;
    do
    {
        printf("1.Enqueue\n");
        printf("2.Dequeue\n");
        printf("3.Front\n");
        printf("4.Printqueue\n");
        int num = 0;
        getchar();
        printf("Enter Choice : ");
        scanf("%d", &num);
        switch (num)
        {

        case 1:
        {
            int val = enqueue(arr, pri);
            if (flag != 0)
                printf("%d is enqued\n", val);
            else
                printf("Queue is Full\n");
        }
        break;
        case 2:
        {
            int val = dequeue(arr);
            if (flag != 0)
                printf("%d is dequed\n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 3:
        {
            int val = frontt(arr);
            if (flag != 0)
                printf("%d is at front \n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 4:
        {
            printqueue(arr, pri);
        }
        break;
        default:
            break;
        }
        getchar();
        printf("Enter 'y/Y' to continue : ");
        scanf("%c", &ch);
    } while (ch != 'n');
}