#include <stdio.h>
#include <stdlib.h>

#define MAX_SIZE 100

int stack1[MAX_SIZE]; // Array representing the first stack for enqueue operations
int stack2[MAX_SIZE]; // Array representing the second stack for dequeue operations
int top1 = -1;        // Top index for the first stack
int top2 = -1;        // Top index for the second stack

// Function to check if a stack is empty
int isEmpty(int top)
{
    return top == -1;
}

// Function to check if a stack is full
int isFull(int top)
{
    return top == MAX_SIZE - 1;
}

// Function to push an element onto a stack
void push(int stack[], int *top, int data)
{
    if (isFull(*top))
    {
        printf("Error: Stack is full.\n");
        return;
    }
    stack[++(*top)] = data;
}

// Function to pop an element from a stack
int pop(int stack[], int *top)
{
    if (isEmpty(*top))
    {
        printf("Error: Stack is empty.\n");
        return -1;
    }
    return stack[(*top)--];
}

// Function to enqueue an element into the Queue
void enqueue(int data)
{
    // Push the element onto stack1
    push(stack1, &top1, data);
}

// Function to dequeue an element from the Queue
int dequeue()
{
    if (isEmpty(top1) && isEmpty(top2))
    {
        printf("Error: Queue is empty.\n");
        return -1;
    }

    // If stack2 is empty, transfer elements from stack1 to stack2
    if (isEmpty(top2))
    {
        while (!isEmpty(top1))
        {
            push(stack2, &top2, pop(stack1, &top1));
        }
    }

    // Pop the top element from stack2 (front of the queue)
    return pop(stack2, &top2);
}

// Main function to test Queue using Stacks (implemented using arrays)
int main()
{
    enqueue(10);
    enqueue(20);
    enqueue(30);

    printf("Dequeued element: %d\n", dequeue());
    printf("Dequeued element: %d\n", dequeue());

    enqueue(40);
    printf("Dequeued element: %d\n", dequeue());

    return 0;
}
