#include <stdio.h>
#include <stdlib.h>
typedef struct Demo
{
    int data;
    int priority;
    struct Demo *next;
} d;
d *front = NULL;
d *rear = NULL;
int size = 0, count = 0, flag = 0;
d *creatnode()
{
    d *newnode = malloc(sizeof(d));
    printf("Enter Data : ");
    scanf("%d", &newnode->data);
    newnode->priority = -1;

    while (newnode->priority < 0 || newnode->priority > 5)
    {
        printf("Enter Priority between 0 to 5 : ");
        scanf("%d", &newnode->priority);
    }
    newnode->next = NULL;
    return newnode;
}
int enqueue()
{
    if (count == size)
    {
        flag = 0;
        return -1;
    }
    d *newnode = creatnode();
    flag = 1;
    count++;
    int val = newnode->data;
    if (front == NULL)
    {
        front = newnode;
        rear = newnode;
        newnode->next = front;
    }
    else
    {
        d *temp = front;
        if (newnode->priority > temp->priority)
        {
            newnode->next = front;
            front = newnode;
            rear->next = front;
        }
        else
        {
            int cnt = 1, num;
            while (newnode->priority <= temp->priority && temp->next != front)
            {
                cnt++;
                temp = temp->next;
            }
            if (cnt <= count - 1 && newnode->priority > rear->priority)
            {
                d *temp = front;
                while (cnt - 2)
                {
                    temp = temp->next;
                    cnt--;
                }
                newnode->next = temp->next;
                temp->next = newnode;
            }
            else
            {
                rear->next = newnode;
                rear = newnode;
                rear->next = front;
            }
        }
    }
    return val;
}
int dequeue()
{
    if (front == NULL)
    {
        flag = 0;
        return -1;
    }
    else
    {
        flag = 1;
        int val = front->data;
        if (front == rear)
        {
            free(front);
            front = NULL;
            rear = NULL;
        }
        else
        {
            d *temp = front;
            front = front->next;
            rear->next = front;
            free(temp);
        }
        return val;
    }
}
void printqueue()
{
    if (front == NULL)
    {
        printf("Empty Queue\n");
        return;
    }
    d *temp = front;
    while (temp->next != front)
    {
        printf("|%d|P=%d|->", temp->data, temp->priority, temp);
        temp = temp->next;
    }
    printf("|%d|P=%d| \n", temp->data, temp->priority, temp);
}
int frontt()
{
    if (front == NULL)
    {
        flag = 0;
        return -1;
    }
    flag = 1;
    return front->data;
}
void main()
{
    printf("Enter Size of Queue : ");
    scanf("%d", &size);
    char ch;
    do
    {
        printf("1.Enqueue\n");
        printf("2.Dequeue\n");
        printf("3.Front\n");
        printf("4.Printqueue\n");
        int num = 0;
        int val;
        getchar();
        printf("Enter Choice : ");
        scanf("%d", &num);
        switch (num)
        {

        case 1:
        {
            for (int i = 0; i < 3; i++)
            {

                val = enqueue();
                /*if (flag != 0)
                    printf("%d is enqued\n", val);
                else
                    printf("Queue is Full\n");*/
                printqueue();
            }
        }
        break;
        case 2:
        {
            val = dequeue();
            if (flag != 0)
                printf("%d is dequed\n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 3:
        {
            val = frontt();
            if (flag != 0)
                printf("%d is at front \n", val);
            else
                printf("Queue is Empty\n");
        }
        break;
        case 4:
        {
            printqueue();
        }
        break;
        default:
            break;
        }
        getchar();
        printf("Enter n/N to Exit : ");
        scanf("%c", &ch);
    } while (ch != 'n');
}