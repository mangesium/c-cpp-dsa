#include <stdio.h>
int count = 0;
int binary(int *arr, int start, int end, int ele)
{
    if (start > end)
        return -1;

    count++;
    int mid = start + (end - start) / 2;

    if (arr[start] == ele)
        return start;
    if (arr[end] == ele)
        return end;
    if (arr[mid] == ele)
        return mid;
    if (ele > arr[mid])
        binary(arr, mid + 1, end, ele);
    else
        binary(arr, start, mid - 1, ele);
}

void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter Element at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    printf("Enter element to search : ");
    int n;
    scanf("%d", &n);
    int start = 0, end = size - 1;
    int index = binary(arr, start, end, n);
    if (index == -1)
        printf("Entered element not found\n");
    else
        printf("%d found at %d index\n", n, index);
    printf("Stack pushed : %d\n", count);
}
//[-1,0,3,5,9,12]
// 9