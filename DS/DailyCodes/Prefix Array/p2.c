#include <stdio.h>

void array(int *arr, int size)
{
    int p = 1;
    for (int i = 0; i < size; i++)
    {
        p *= arr[i];
    }
    for (int i = 0; i < size; i++)
    {
        int q = 0, k = p;
        while (k >= arr[i])
        {
            k = k - arr[i];
            q++;
        }
        arr[i] = q;
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    array(arr, size);
}