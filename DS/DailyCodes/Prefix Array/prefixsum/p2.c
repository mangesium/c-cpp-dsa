#include <stdio.h>
void Rangesum(int *arr, int n)
{
    int pfix[n];
    pfix[0] = arr[0];
    for (int i = 1; i < n; i++)
    {
        pfix[i] = pfix[i - 1] + arr[i];
    }
    int s = 0, e = 0, q = 0;
    printf("Enter Number of query : ");
    scanf("%d", &q);
    for (int i = 0; i < q; i++)
    {
        printf("Enter Start : ");
        scanf("%d", &s);
        printf("Enter End : ");
        scanf("%d", &e);
        if (s == 0)
        {
            printf("%d is sum of range\n", pfix[e]);
        }
        else
        {
            printf("%d is sum of range\n", pfix[e] - pfix[s - 1]);
        }
    }
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    Rangesum(arr, size);
}