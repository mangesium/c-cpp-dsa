#include <stdio.h>
#include <stdlib.h>
int *rightmax(int *arr, int n)
{
    int *rmax = malloc(sizeof(int) * n);
    rmax[n - 1] = arr[n - 1];
    for (int i = n - 2; i >= 0; i--)
    {
        if (arr[i] > rmax[i + 1])
            rmax[i] = arr[i];
        else
            rmax[i] = rmax[i + 1];
    }
    return rmax;
}

void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    int *ptr = rightmax(arr, size);
    for (int i = 0; i < size; i++)
    {
        printf("%d ", ptr[i]);
    }
    printf("\n");
}