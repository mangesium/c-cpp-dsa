#include <stdio.h>
#include <string.h>
int subsequence(char *cptr, char *c)
{
    int count = 0;
    int k=0;
    for(int i=0;i<strlen(cptr);i++){
        if(cptr[i]==c[0]){
            k++;
        }
        if(cptr[i]==c[1]){
            count+=k;
        }
    }
    return count;
}
void main()
{
    char arr[100] = "GAB";
    /*printf("Enter string : ");
    char ch;
    int i = 0;
    while ((ch = getchar()) != '\n')
    {
        arr[i] = ch;
        i++;
    }*/
    for (int i = 0; i < strlen(arr); i++)
    {
        printf("%c", arr[i]);
    }
    printf("\n");
    int p = subsequence(arr, "AG");
    printf("%d ", p);
    printf("\n");
}