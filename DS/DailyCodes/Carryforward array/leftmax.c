#include <stdio.h>
#include <stdlib.h>

int *leftmax(int *arr, int n)
{
    int *lmax = malloc(sizeof(int) * n);
    lmax[0] = arr[0];
    for (int i = 1; i < n; i++)
    {
        if (arr[i] > lmax[i - 1])
        {
            lmax[i] = arr[i];
        }
        else
        {
            lmax[i] = lmax[i - 1];
        }
    }
    return lmax;
}
void main()
{
    int size;
    printf("Enter size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element in %d index : ", i);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < size; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    int *ptr = leftmax(arr, size);
     for (int i = 0; i < size; i++)
    {
        printf("%d ", ptr[i]);
    }
    printf("\n");
}