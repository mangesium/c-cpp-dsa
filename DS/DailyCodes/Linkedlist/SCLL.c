#include <stdio.h>
#include <stdlib.h>
typedef struct demo
{
    int data;
    struct demo *next;
} d;
d *creatnode()
{
    d *node = malloc(sizeof(d));
    printf("Enter data : ");
    scanf("%d", &node->data);
    node->next = NULL;
    return node;
}
int count(d *head)
{
    d *temp = head;
    if (head == NULL)
        return 0;
    int count = 0;
    while (temp->next != head)
    {
        count++;
        temp = temp->next;
    }
    return count + 1;
}
void printll(d *head)
{
    if (head == NULL)
        printf("Empty LinkedList");
    else
    {
        d *temp = head;
        while (temp->next != head)
        {
            printf("|%d|->", temp->data);
            temp = temp->next;
        }
        printf("|%d|", temp->data);
    }
    printf("\n");
}
void addnode(d **head)
{
    d *node = creatnode();
    if (*head == NULL)
    {
        *head = node;
        (*head)->next = *head;
    }
    else
    {
        d *temp = *head;
        while (temp->next != *head)
        {
            temp = temp->next;
        }
        temp->next = node;
        node->next = *head;
    }
}
void addfirst(d **head)
{
    d *node = creatnode();
    if (*head == NULL)
    {
        *head = node;
        (*head)->next = *head;
    }
    else
    {
        d *temp = *head;
        while (temp->next != *head)
        {
            temp = temp->next;
        }
        temp->next = node;
        node->next = *head;
        *head = node;
    }
}
void addatpos(int pos, d **head)
{
    if (*head == NULL)
        printf("LinkedList is empty\n");
    else if (pos < 1 || pos > count(*head) + 1)
        printf("Enter Valid Position\n");
    else if (pos == 1)
        addfirst(head);
    else if (pos == count(*head))
        addnode(head);
    else
    {
        d *node = creatnode();
        d *temp = *head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        node->next = temp->next;
        temp->next = node;
    }
}
void deletefirst(d **head)
{
    if (*head == NULL)
    {
        printf("LinkedList is empty\n");
    }
    else if (count(*head) == 1)
    {
        free(*head);
        *head = NULL;
    }
    else
    {
        d *temp = *head;
        d *temp2 = *head;
        while (temp->next != (*head))
        {
            temp = temp->next;
        }
        *head = (*head)->next;
        temp->next = *head;
        free(temp2);
    }
}
void deletelast(d **head)
{
    if (*head == NULL)
        printf("LinkedList is empty\n");
    else if (count(*head) == 1)
    {
        free(*head);
        *head = NULL;
    }
    else
    {
        d *temp = *head;
        while (temp->next->next != (*head))
        {
            temp = temp->next;
        }
        free(temp->next);
        temp->next = *head;
    }
}
void deleteatpos(int pos, d **head)
{
    if ((*head) == NULL)
    {
        printf("Linkedlist is empty\n");
        return;
    }
    if (pos == 1)
        deletefirst(head);
    else if (pos == count(*head))
        deletelast(head);
    else if (pos <= 0 || pos > count(*head))
    {
        printf("Enter Valid Input\n");
    }
    else
    {
        d *temp = *head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        d *temp2 = temp->next;
        temp->next = temp->next->next;
        free(temp2);
    }
}

void reverse(d **head)
{
    if (*head == NULL)
    {
        printf("LinkedList is Empty\n");
        return;
    }
    d *temp1 = *head;
    d *temp2 = *head;
    int x = 0, n = 0, k = 0;
    while (count(*head) / 2 - x)
    {
        temp2 = *head;
        k = count(*head) - 1 - n;
        while (k)
        {
            temp2 = temp2->next;
            k--;
        }
        int swap = temp1->data;
        temp1->data = temp2->data;
        temp2->data = swap;
        temp1 = temp1->next;
        x++;
        n++;
    }
}
void imreverse(d **head)
{

    if (*head == NULL)
    {
        printf("Linked list is empty\n");
        return;
    }
    if ((*head)->next == (*head))
        return;
    d *temp1 = NULL;
    d *temp2 = (*head);
    d *temp3 = (*head);

    while (temp1 != (*head))
    {
        temp1 = temp2->next;
        temp2->next = temp3;
        temp3 = temp2;
        temp2 = temp1;
    }
    (*head)->next = temp3;
    *head = temp3;
}
void main()
{
    d *head = NULL;

    char choice;

    do
    {

        printf("--------------------------------------------------------------\n");
        printf("1.Addnode\n");
        printf("2.Printll\n");
        printf("3.Count the nodes\n");
        printf("4.Add node at first position\n");
        printf("5.Add node at given position\n");
        printf("6.Delete first node\n");
        printf("7.Delete last node\n");
        printf("8.Delete node at position\n");
        printf("9.Reverse LinkedList\n");
        printf("10.Implace Reverse\n");

        int ch;
        printf("Enter choice: ");
        scanf("%d", &ch);
        switch (ch)
        {

        case 1:
        {
            for (int i = 0; i < 5; i++)
                addnode(&head);
        }
        break;

        case 2:
            printll(head);
            break;

        case 3:
        {
            printf("node count=%d\n", count(head));
        }
        break;

        case 4:
            addfirst(&head);
            break;

        case 5:
        {
            int pos;
            printf("Enter at which position you want to enter the node:\n");
            scanf("%d", &pos);
            addatpos(pos, &head);
        }
        break;

        case 6:
            deletefirst(&head);
            break;

        case 7:
            deletelast(&head);
            break;
        case 8:
        {
            int pos;
            printf("Enter at which position you want to delete the node:\n");
            scanf("%d", &pos);
            deleteatpos(pos, &head);
        }
        break;
        case 9:
        {
            reverse(&head);
        }
        break;
        case 10:
        {
            imreverse(&head);
        }
        break;
        default:
            printf("Enter valid Input\n");
        }

        getchar();
        printf("If you want to continue enter y/Y :");
        scanf("%c", &choice);
    } while (choice != 'n');
}
