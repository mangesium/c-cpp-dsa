#include <stdio.h>
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
typedef struct demo
{
    int data;
    struct demo *next;
} d;
d *creatnode()
{
    d *node = malloc(sizeof(d));
    printf("Enter data : ");
    scanf("%d", &node->data);
    node->next = NULL;
    return node;
}
void addnode(d **head)
{
    d *node = creatnode();
    if (*head == NULL)
        *head = node;
    else
    {
        d *temp = *head;
        while (temp->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = node;
    }
}
void printll(d *head)
{
    while (head != NULL)
    {
        if (head->next == NULL)
            printf("|%d|", head->data);
        else
            printf("|%d|->", head->data);
        head = head->next;
    }
    printf("\n");
}
void findsnum(d *head, int n)
{
    int p1 = INT_MIN, p2 = INT_MIN, count = 0;
    while (head != NULL)
    {
        count++;
        if (n == head->data)
        {
            p2 = p1;
            p1 = count;
        }
        head = head->next;
    }
    if (p2 != INT_MIN)
    {
        printf("%d's second last occourance is at %d in linked list\n", n, p2);
    }
    else if (p1 != INT_MIN)
    {
        printf("%d's occourance is once at %d position in linked list\n", n, p1);
    }
    else
        printf("%d is not present in linked list\n", n);
}
void main()
{
    d *head = NULL;
    int k = 0;
    char ch;
    do
    {
        printf("1.addnode()\n");
        printf("2.printll()\n");
        printf("3.second occrance()\n");
        printf("Enter choice : ");
        scanf("%d", &k);
        switch (k)
        {
        case 1:
        {
            int num = 0;
            printf("Enter number of you want in linked list :");
            scanf("%d", &num);
            for (int i = 0; i < num; i++)
            {
                addnode(&head);
            }
        }
        break;
        case 2:
        {
            if (head == NULL)
                printf("Linkedlist is empty\n");
            else
                printll(head);
        }
        break;
        case 3:
        {
            int num;
            if (head == NULL)
                printf("Linkedlist is empty\n");
            else
            {
                printf("Enter number see it's second last occurance : ");
                scanf("%d", &num);
                findsnum(head, num);
            }
        }
        break;
        default:
            printf("Invalid Input\n");
        }
        printf("Do you want to continue ? ");
        getchar();
        scanf("%c", &ch);
    } while (ch == 'y');
}