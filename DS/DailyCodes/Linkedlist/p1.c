#include<stdio.h>
#include<string.h>
typedef struct Demo{
	int num;
	struct Demo* next;
}demo;

void main(){
	demo obj1,obj2,obj3,obj4,obj5;
	demo* head=&obj1;
	obj1.num=10;
	head->next=&obj2;
	obj2.num=20;
	head->next->next=&obj3;
	obj3.num=30;
	head->next->next->next=&obj4;
	obj4.num=40;
	head->next->next->next->next=&obj5;
	obj5.num=50;
	head->next->next->next->next->next=NULL;
	demo *temp=head;
	while(temp!=NULL){
		if(temp->next!=NULL)
			printf("%d->",temp->num);
		else
			printf("%d",temp->num);
		temp=temp->next;
	}
	printf("\n");
}
