#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct movie
{
	char mName[20];
	float imdb;
	struct movie *next;
} mov;

mov *head = NULL;
void addnode()
{

	mov *newnode = (mov *)malloc(sizeof(mov));
	printf("Enter Name of Movie : ");
	fgets(newnode->mName, 15, stdin);
	printf("Enter Imdb Rating : ");
	scanf("%f", &newnode->imdb);
	getchar();
	newnode->next = NULL;
	if (head == NULL)
	{
		head = newnode;
	}
	else
	{
		mov *temp = head;
		while (temp->next != NULL)
		{

			temp = temp->next;
		}
		temp->next = newnode;
	}
}

void printll()
{
	mov *temp = head;
	while (temp != NULL)
	{
		temp->mName[strlen(temp->mName) - 1] = '\0';
		printf("|%s->%f|", temp->mName, temp->imdb);
		temp = temp->next;
	}
}

void main()
{
	addnode();
	addnode();
	addnode();
	printll();
}
