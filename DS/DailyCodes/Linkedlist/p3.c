#include<stdio.h>
#include<string.h>
#include<stdlib.h>
typedef struct demo{
	int num;
	struct demo* next;
}d;

void printnode(d* head){
	d* temp=head;
	while(temp!=NULL){
		if(temp->next!=NULL)
			printf("%d->",temp->num);
		else
			printf("%d",temp->num);
		temp=temp->next;
	}
	printf("\n");
}
void newnode(d** head){
	d* node=malloc(sizeof(d));
	printf("Enter Element : ");
	scanf("%d",&node->num);
	if(*head==NULL)
		*head=node;
	else{
		d* temp=*head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=node;
	}
}
void main(){
	char ch='a';
	d* head=NULL;
	do{
		newnode(&head);				//Adding new node to a local head pointer.
		printf("Enter y/Y to add node to linked list or n/N to exit : ");
		getchar();
		scanf("%c",&ch);
	}while(ch!='n');

	printnode(head);
}
