#include<stdio.h>
#include<string.h>
#include<stdlib.h>
typedef struct demo{
	int num;
	struct demo* next;
}d;
d* head=NULL;
void printnode(){
	d* temp=head;
	while(temp!=NULL){
		if(temp->next!=NULL)
			printf("%d->",temp->num);
		else
			printf("%d",temp->num);
		temp=temp->next;
	}
	printf("\n");
}
void newnode(){
	d* node=malloc(sizeof(d));
	printf("Enter Element : ");
	scanf("%d",&node->num);
	if(head==NULL)
		head=node;
	else{
		node->next=head;
		head=node;
	}
}
void deletefirst()
{
    if (head == NULL)
    {
        printf("Linkedlist is empty\n");
        return;
    }
    d *temp = head;
    head = head->next;
    free(temp);
}
void deletelast()
{
	d* temp=head;
    if (head == NULL)
    {
        printf("Linkedlist is empty\n");
        return;
    }
    while (temp->next->next != NULL)
    {
        temp = temp->next;
    }
    free(temp->next);
    temp->next = NULL;
}
void deleteatpos(int pos)
{
    if (head == NULL)
    {
        printf("Linkedlist is empty\n");
    }
    else if (pos == 1)
        deletefirst();
    else if (pos == count())
        deletelast();
    else if (pos <= 0 || pos > count())
    {
        printf("Enter Valid Input\n");
    }
    else
    {
        d *temp = head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        d *rm = temp->next;
        temp->next = temp->next->next;
        free(rm);
    }
}
void main(){
	char ch;
	do{
		newnode();
		printf("Enter y/Y to add node to linked list or n/N to exit : ");
		getchar();
		scanf("%c",&ch);
	}while(ch!='n');

	printnode(head);

}
