#include <stdio.h>
#include <stdlib.h>
typedef struct demo
{
    int data;
    struct demo *next;
} d;
d *creatnode()
{
    d *node = malloc(sizeof(d));
    printf("Enter data : ");
    scanf("%d", &node->data);
    node->next = NULL;
    return node;
}
void addnode(d **head)
{
    d *node = creatnode();
    if (*head == NULL)
        *head = node;
    else
    {
        d *temp = *head;
        while (temp->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = node;
    }
}
void printll(d *head)
{
    if (head == NULL)
        printf("Empty LinkedList");
    else
    {
        while (head != NULL)
        {
            if (head->next == NULL)
                printf("|%d|", head->data);
            else
                printf("|%d|->", head->data);
            head = head->next;
        }
    }
    printf("\n");
}
int count(d *head)
{
    int count = 0;
    if (head == NULL)
        return 0;
    while (head != NULL)
    {
        count++;
        head = head->next;
    }
    return count;
}
void addfirst(d **head)
{
    d *node = creatnode();
    if (head == NULL)
        *head = node;
    else
    {
        node->next = (*head);
        *head = node;
    }
}
void addatpos(int pos, d **head)
{
    d *node;
    if (*head == NULL)
    {
        printf("Linkedlist is empty adding 1st node\n");
        node = creatnode();
        *head = node;
    }
    else if (pos == 1)
    {
        addfirst(head);
    }
    else if (pos == count(*head) + 1)
        addnode(head);
    else if (pos <= 0 || pos > count(*head))
        printf("Enter Valid input as linked list out of bound \n");
    else
    {
        node = creatnode();
        d *temp = *head;
        while (pos - 2)
        {
            *head = (*head)->next;
            pos--;
        }
        node->next = (*head)->next;
        (*head)->next = node;
    }
}
void deletefirst(d **head)
{
    if ((*head) == NULL)
    {
        printf("Linkedlist is empty\n");
        return;
    }
    d *temp = *head;
    *head = (*head)->next;
    free(temp);
}
void deletelast(d **head)
{
    if ((*head) == NULL)
    {
        printf("Linkedlist is empty\n");
        return;
    }
    while ((*head)->next->next != NULL)
    {
        *head = (*head)->next;
    }
    free((*head)->next);
    (*head)->next = NULL;
}
void deleteatpos(int pos, d **head)
{
    if ((*head) == NULL)
    {
        printf("Linkedlist is empty\n");
        return;
    }
    if (pos == 1)
        deletefirst(head);
    else if (pos == count(*head))
        deletelast(head);
    else if (pos <= 0 || pos > count(*head))
    {
        printf("Enter Valid Input\n");
    }
    else
    {
        d *temp = *head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        d *rm = temp->next;
        temp->next = temp->next->next;
        free(rm);
    }
}
void reverse(d **head)
{
    if (*head == NULL)
    {
        printf("LinkedLinked is empty\n");
        return;
    }
    d *temp1 = *head;
    d *temp2 = *head;
    int x = 0, cnt = count(*head);

    while ((cnt / 2) - x)
    {
        temp2 = *head;
        int k = cnt - 1 - x;
        while (k)
        {
            temp2 = temp2->next;
            k--;
        }
        int swap = temp1->data;
        temp1->data = temp2->data;
        temp2->data = swap;
        temp1 = temp1->next;
        x++;
    }
}
void imreverse(d **head)
{
    if (*head == NULL)
    {
        printf("Linked list is empty\n");
        return;
    }
    d *temp1 = NULL;
    d *temp2 = NULL;
    while (*head != NULL)
    {
        temp2 = (*head)->next;
        (*head)->next = temp1;
        temp1 = (*head);
        (*head) = temp2;
    }
    *head = temp1;
}
void main()
{
    d *head = NULL;

    char choice;

    do
    {

        printf("--------------------------------------------------------------\n");
        printf("1.Addnode\n");
        printf("2.Printll\n");
        printf("3.Count the nodes\n");
        printf("4.Add node at first position\n");
        printf("5.Add node at given position\n");
        printf("6.Delete first node\n");
        printf("7.Delete last node\n");
        printf("8.Delete node at position\n");
        printf("9.Reverse LinkedList\n");
        printf("10.Implace Reverse\n");

        int ch;
        printf("Enter choice: ");
        scanf("%d", &ch);
        switch (ch)
        {

        case 1:
        {
            for (int i = 0; i < 5; i++)
                addnode(&head);
        }
        break;

        case 2:
            printll(head);
            break;

        case 3:
        {
            printf("node count=%d\n", count(head));
        }
        break;

        case 4:
            addfirst(&head);
            break;

        case 5:
        {
            int pos;
            printf("Enter at which position you want to enter the node:\n");
            scanf("%d", &pos);
            addatpos(pos, &head);
        }
        break;

        case 6:
            deletefirst(&head);
            break;

        case 7:
            deletelast(&head);
            break;
        case 8:
        {
            int pos;
            printf("Enter at which position you want to delete the node:\n");
            scanf("%d", &pos);
            deleteatpos(pos, &head);
        }
        break;
        case 9:
        {
            reverse(&head);
        }
        break;
        case 10:
        {
            imreverse(&head);
        }
        break;
        default:
            printf("Enter valid Input\n");
        }

        getchar();
        printf("If you want to continue enter y/Y :");
        scanf("%c", &choice);
    } while (choice == 'y' || choice == 'Y');
}
