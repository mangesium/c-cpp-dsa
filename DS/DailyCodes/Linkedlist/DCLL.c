#include <stdio.h>
#include <stdlib.h>
typedef struct Demo
{
    struct Demo *prev;
    int data;
    struct Demo *next;
} d;
d *createnode()
{
    d *node = (d *)malloc(sizeof(d));
    printf("Enter data : ");
    scanf("%d", &node->data);
    node->prev = NULL;
    node->next = NULL;
    return node;
}
void printll(d *head)
{
    if (head == NULL)
    {
        printf("Linked List is empty\n");
        return;
    }
    d *temp = head;
    while (temp->next != head)
    {
        // printf(" %ld | %d | %ld->", temp->prev, temp->data, temp->next);
        printf("|%d|->", temp->data);

        temp = temp->next;
    }
    // printf(" %ld | %d | %ld\n", temp->prev, temp->data, temp->next);
    printf("|%d|\n", temp->data);
}
int count(d *head)
{
    if (head == NULL)
    {
        printf("LinkedList is empty\n");
        return 0;
    }
    int count = 1;
    d *temp = head;
    while (temp->next != head)
    {
        count++;
        temp = temp->next;
    }
    return count;
}
void addnode(d **head)
{
    d *newnode = createnode();
    if (*head == NULL)
    {
        *head = newnode;
        (*head)->prev = (*head);
        (*head)->next = (*head);
    }
    else
    {
        d *temp = *head;
        while (temp->next != (*head))
        {
            temp = temp->next;
        }
        temp->next = newnode;
        newnode->prev = temp;
        newnode->next = *head;
        (*head)->prev = newnode;
    }
}
void addfirst(d **head)
{
    d *newnode = createnode();
    if (*head == NULL)
    {
        *head = newnode;
        newnode->next = *head;
        newnode->prev = *head;
    }
    else
    {
        newnode->next = *head;
        newnode->prev = (*head)->prev;
        (*head)->prev->next = newnode;
        newnode->next->prev = newnode;
        (*head) = newnode;
    }
}
void addatpos(int pos, d **head)
{
    int cnt = count(*head);
    if (*head == NULL)
    {
        printf("Empty LinkedList\n");
        return;
    }
    else if (pos > cnt + 1 || pos < 1)
    {
        printf("Position out of range\n");
        return;
    }
    else if (pos == 1)
    {
        addfirst(head);
    }
    else if (pos == cnt + 1)
    {
        addnode(head);
    }
    else
    {
        d *temp = *head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        d *newnode = createnode();
        newnode->prev = temp;
        newnode->next = temp->next;
        newnode->next->prev = newnode;
        newnode->prev->next = newnode;
    }
}
void deletefirst(d **head)
{
    if (*head == NULL)
    {
        printf("Empty LinkedList\n");
        return;
    }
    else if ((*head)->next == (*head))
    {
        free(*head);
        *head = NULL;
    }
    else
    {
        (*head)->next->prev = (*head)->prev;
        (*head) = (*head)->next;
        free((*head)->prev->next);
        (*head)->prev->next = *head;
    }
}
void deletelast(d **head)
{
    if (*head == NULL)
    {
        printf("Empty LinkedList\n");
        return;
    }
    else if ((*head)->next == (*head))
    {
        free(*head);
        *head = NULL;
    }
    else
    {
        (*head)->prev = (*head)->prev->prev;
        free((*head)->prev->next);
        (*head)->prev->next = *head;
    }
}
void deleteatpos(int pos, d **head)
{
    if (*head == NULL)
    {
        printf("Empty LinkedList\n");
        return;
    }
    else if (pos > count(*head) || pos < 1)
    {
        printf("Position out of range\n");
        return;
    }
    else if (pos == 1)
    {
        deletefirst(head);
    }
    else if (pos == count(*head))
    {
        deletelast(head);
    }
    else
    {
        d *temp = *head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        temp->next = temp->next->next;
        free(temp->next->prev);
        temp->next->prev = temp;
    }
}
void reverse(d **head)
{
    if (*head == NULL)
    {
        printf("Empty LinkedList\n");
        return;
    }
    d *temp = *head;
    d *temp1 = *head;
    int x = 0;
    while (temp->next != *head)
        temp = temp->next;
    while (count(*head) / 2 - x)
    {
        int swap = temp->data;
        temp->data = temp1->data;
        temp1->data = swap;
        temp1 = temp1->next;
        temp = temp->prev;
        x++;
    }
}

// void imreverse(d **head)
// {
//     if (*head == NULL)
//     {
//         printf("Empty LinkedList\n");
//         return;
//     }
//     else if ((*head)->next == (*head))
//         return;

//     d *temp1 = NULL;
//     d *temp2 = NULL;
//     d *temp3 = *head;

//     while (temp1 != *head)
//     {
//         temp1 = temp3->next;
//         temp3->prev = temp3->next;
//         temp3->next = temp2;
//         temp2 = temp3;
//         temp3 = temp1;
//     }
//     (*head)->next = temp2;
//     *head = temp2;
// }

void imreverse(d **head)
{
    if (*head == NULL)
    {
        printf("Empty LinkedList\n");
        return;
    }
    else if ((*head)->next == (*head))
        return;

    d *temp1 = *head;
    d *temp2 = NULL;
    while (1)
    {
        temp2 = temp1->prev;
        temp1->prev = temp1->next;
        temp1->next = temp2;
        temp1 = temp1->prev;
        if (temp1 == (*head))
            break;
    }
    *head = (*head)->next;
}

void main()
{
    d *head = NULL;

    char choice;

    do
    {

        printf("--------------------------------------------------------------\n");
        printf("1.Addnode\n");
        printf("2.Printll\n");
        printf("3.Count the nodes\n");
        printf("4.Add node at first position\n");
        printf("5.Add node at given position\n");
        printf("6.Delete first node\n");
        printf("7.Delete last node\n");
        printf("8.Delete node at position\n");
        printf("9.Reverse LinkedList\n");
        printf("10.Implace Reverse\n");

        int ch;
        printf("Enter choice: ");
        scanf("%d", &ch);
        switch (ch)
        {

        case 1:
        {
            addnode(&head);
        }
        break;

        case 2:
            printll(head);
            break;

        case 3:
        {
            printf("node count=%d\n", count(head));
        }
        break;

        case 4:
            addfirst(&head);
            break;

        case 5:
        {
            int pos;
            printf("Enter at which position you want to enter the node:\n");
            scanf("%d", &pos);
            addatpos(pos, &head);
        }
        break;

        case 6:
            deletefirst(&head);
            break;

        case 7:
        {
            deletelast(&head);
            // printll(head);
        }
        break;
        case 8:
        {
            int pos;
            printf("Enter at which position you want to delete the node:\n");
            scanf("%d", &pos);
            deleteatpos(pos, &head);
        }
        break;
        case 9:
        {
            reverse(&head);
        }
        break;
        case 10:
        {
            imreverse(&head);
        }
        break;
        default:
            printf("Enter valid Input\n");
        }

        getchar();
        printf("If you want to continue enter y/Y :");
        scanf("%c", &choice);
    } while (choice != 'n');
}
