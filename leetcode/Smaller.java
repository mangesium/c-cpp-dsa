import java.util.ArrayList;
import java.util.List;

public class Smaller {
    public int[] smallerNumbersThanCurrent(int[] nums) {
        int[] arr = new int[102];
        for (int i = 0; i < nums.length; i++)
            arr[nums[i] + 1]++;

        for (int i = 1; i < 102; i++)
            arr[i] += arr[i - 1];

        for (int i = 0; i < nums.length; i++)
            nums[i] = arr[nums[i]];

        return nums;
    }
    public String restoreString(String s, int[] A) {
        char carr[]=new char[A.length];  
        for(int i=0;i<A.length;i++){
            carr[A[i]]=s.charAt(i);
        }
        return new String(carr);
      }
    public int[] createTargetArray(int[] nums, int[] index) {
        List<Integer> ll = new ArrayList<Integer>();
        for (int i = 0; i < nums.length; i++) {
            ll.add(index[i], nums[i]);
        }
        int i = 0;
        for (int part : ll) {
            nums[i] = part;
            i++;
        }
        return nums;
    }
    public boolean isAcronym(List<String> words, String s) {
        if(words.size()!=s.length()) return false;
        int k=0;
        char ch=s.charAt(0);
        for(int i=0;i<words.size();i++){
            if(k<s.length()){
                ch=s.charAt(k);
                k++;
            }
            if(words.get(i).charAt(0)!=ch)
                return false;
        }
        
        return true;
    }
    public int arithmeticTriplets(int[] nums, int diff) {
        int cnt=0;
        for(int i=0;i<nums.length;i++){
            int temp=0,k=nums[i];
            for(int j=i+1;j<nums.length;j++){
                if(k+diff==nums[j]){
                    k+=diff;
                    temp++;
                }
                if(temp==2){
                    cnt++;
                    break;
                }
            }
        }
        return cnt;
    }
    public String reversePrefix(String word, char ch) {
        int i=0;
        char carr[]=word.toCharArray();
        for(i=0;i<carr.length;i++){
            if(word.charAt(i)==ch){
                break;
            }
        }
        if(i==carr.length)
            return word;
        for(int j=0;j<=i/2;j++){
            char temp=carr[j];
            carr[j]=carr[i-j];
            carr[i-j]=temp;
        }
        return new String(carr);
    }

    public static void main(String[] args) {
        System.out.println(new Smaller().smallerNumbersThanCurrent(new int[] { 1, 2, 3, 5, 8 }));
    }
}