import java.util.StringTokenizer;
public class EqualString{
    public boolean arrayStringsAreEqual(String[] word1, String[] word2) {

        String str1="",str2="";
        int n=(word1.length>word2.length)?word1.length:word2.length;
        for(int i=0;i<n;i++){
            if(i<word1.length)
                str1+=word1[i];
            if(i<word2.length)
                str2+=word2[i];
        }
        if(str1.equals(str2))
            return true;
          
        return false;
       
    }
    public String truncateSentence(String s, int k) {
        StringTokenizer st1 = new StringTokenizer(s, " ");
        String str = "";
        while(k!=0){
            str+=st1.nextToken()+" ";
            k--;
        }
        return str.trim();
    }
    public boolean isMonotonic(int[] nums) {
        if(nums.length==1)
            return true;
        int flag=0;
        int k=nums[0];
        int i=1;
        for(;i<nums.length;i++){
            if(k>nums[i]){
                flag=1;
                break;
            }   
            else if(k<nums[i]){
                flag=0;
                break;
            }
        }
        
        if(flag==0){
            for(int j=i;j<nums.length;j++){
                if(nums[j]>=k){
                    k=nums[j];
                    continue;
                } 
                return false;
            }
        }else{
            for(int j=i;j<nums.length;j++){
                    if(nums[j]<=k){
                        k=nums[j];
                        continue;
                    }
                    return false;
                }
        }
        return true;
    }
    public static void main(String[] args) {
        EqualString eq = new EqualString();
        System.out.println(eq.arrayStringsAreEqual(new String[]{"ab", "c"}, new String[]{"a", "bc"}));
       
    }
}