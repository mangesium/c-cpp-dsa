import java.util.*;

class Solution {
    public String reverseWords(String s) {

        char ch[] = s.toCharArray();
        char ret[] = new char[ch.length];
        int itr = 0;
        for (int i = ch.length - 1; i >= 0; i--) {
            int j = i, flag = 0;
            for (; j >= 0 && ch[j] != ' '; j--) {

            }
            for (int k = j + 1; k <= i && ch[k] != ' '; k++) {
                ret[itr] = ch[k];
                itr++;
                flag = 1;
            }
            if (itr != ch.length && flag == 1) {
                ret[itr] = ' ';
                itr++;
            }
            i = j;
        }

        return new String(ret, 0, itr);
    }

    public String restoreString(String s, int[] A) {
        char carr[] = new char[A.length];
        for (int i = 0; i < A.length; i++) {
            carr[A[i]] = s.charAt(i);
        }
        return new String(carr);
    }

    public int[] createTargetArray(int[] nums, int[] index) {
        List<Integer> ll = new ArrayList<Integer>();
        for (int i = 0; i < nums.length; i++) {
            ll.add(index[i], nums[i]);
        }
        int i = 0;
        for (int part : ll) {
            nums[i] = part;
            i++;
        }
        return nums;
    }
    public int[] sortArrayByParity(int[] nums) {
        int arr[]= new int[nums.length];
        int i=0,k=0,j=nums.length-1;
        for(;i<nums.length;i++){
            if(nums[i]%2==0){
                arr[k]=nums[i];
                k++;
            }else{
                arr[j]=nums[i];
                j--;
            }
        }
        return arr;
    }
    public boolean areOccurrencesEqual(String s) {
        int bucket[] = new int[26];
        for(int i=0;i<s.length();i++){
            bucket[s.charAt(i)-'a']++;
        }
        int k=bucket[s.charAt(0)-'a'];
        for(int i=0;i<26;i++){
            if(bucket[i]==0||bucket[i]==k)
                continue;
            return false;
        }
        return true;
    }
    public List<String> splitWordsBySeparator(List<String> words, char separator) {
        List<String> list = new ArrayList<>();
        for(String part : words){
            StringTokenizer st = new StringTokenizer(part,Character.toString(separator));
            while(st.hasMoreTokens()){
                list.add(st.nextToken());
            }  
        }
        return list;
    }

    public boolean isAcronym(List<String> words, String s) {
        if (words.size() != s.length())
            return false;
        int k = 0;
        char ch = s.charAt(0);
        for (int i = 0; i < words.size(); i++) {
            if (k < s.length()) {
                ch = s.charAt(k);
                k++;
            }
            if (words.get(i).charAt(0) != ch)
                return false;
        }

        return true;
    }

    public int arithmeticTriplets(int[] nums, int diff) {
        int cnt = 0;
        for (int i = 0; i < nums.length; i++) {
            int temp = 0, k = nums[i];
            for (int j = i + 1; j < nums.length; j++) {
                if (k + diff == nums[j]) {
                    k += diff;
                    temp++;
                }
                if (temp == 2) {
                    cnt++;
                    break;
                }
            }
        }
        return cnt;
    }

    public String reversePrefix(String word, char ch) {
        int i = 0;
        char carr[] = word.toCharArray();
        for (i = 0; i < carr.length; i++) {
            if (word.charAt(i) == ch) {
                break;
            }
        }
        if (i == carr.length)
            return word;
        for (int j = 0; j <= i / 2; j++) {
            char temp = carr[j];
            carr[j] = carr[i - j];
            carr[i - j] = temp;
        }
        return new String(carr);
    }

    public int mostWordsFound(String[] A) {
        int max = 0, cnt = 0;
        for (int i = 0; i < A.length; i++) {
            cnt = 0;
            String str = A[i];

            for (int j = 0; j < str.length(); j++) {
                if (str.charAt(j) == ' ')
                    cnt++;
            }
            max = cnt > max ? cnt : max;
        }
        return max + 1;
    }

    public static void main(String[] args) {
        System.out.println(new Solution().reverseWords("Echa mama cha pecha mama"));
    }
}