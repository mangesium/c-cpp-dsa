#include <stdio.h>
#include<stdlib.h>
struct ListNode
{
    int val;
    struct ListNode *next;
};
struct ListNode *deleteMiddle(struct ListNode *head)
{
    int cnt = 0;
    struct ListNode *temp = head;
    while (temp != NULL)
    {
        temp = temp->next;
        cnt++;
    }
    if (cnt == 1)
    {
        free(head);
        head = NULL;
    }
    else if (cnt == 2)
    {
        free(head->next);
        head->next = NULL;
    }
    else
    {
        temp = head;
        int pos = 0;
        pos = cnt / 2 + 1;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        struct ListNode *val = temp->next;
        temp->next = temp->next->next;
        free(val);
    }
    return head;
}
char * replaceDigits(char * s){
    for(int i=1;i<strlen(s);i++){
        if(s[i]>='a'&&s[i]<='z')
            continue;
        s[i]=(char)s[i-1]+s[i]-'0';
    }
    return s;
}
void main()
{
}