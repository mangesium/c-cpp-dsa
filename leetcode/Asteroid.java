public class Asteroid {
    int top = -1;

    void push(int stack[], int num) {
        stack[++top] = num;
    }

    void pop() {
        top--;
    }

    public int[] asteroidCollision(int[] A) {
        int stack[] = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            if (top == -1 || A[i] > 0) {
                push(stack, A[i]);
            } else {
                while (top != -1 && stack[top] > 0 && Math.abs(stack[top]) < Math.abs(A[i]))
                    pop();
                if (top != -1 && (-stack[top] == A[i] || stack[top] == -A[i]))
                    pop();
                else if (top == -1 || (top != -1 && stack[top] < 0 && A[i] < 0)) {
                    push(stack, A[i]);
                }
            }
        }
        int arr[] = new int[top + 1];
        for (int i = 0; i <= top; i++)
            arr[i] = stack[i];

        return arr;
    }
    public int sumOddLengthSubarrays(int[] arr) {
        
        for(int i=1;i<arr.length;i++)
            arr[i]+=arr[i-1];

        int sum=arr[arr.length-1];
        for(int i=3;i<=arr.length;i+=2){
            sum+=arr[i-1];
            for(int j=i;j<arr.length;j++){
                sum+=arr[j]-arr[j-i];
            }
        }
        return sum;
    }
    public boolean uniqueOccurrences(int[] arr) {
        int bucket[] = new int[2001];
        int trace[] = new int[1001];
        for(int i=0;i<arr.length;i++){
            if(arr[i]<0){
                bucket[1000-arr[i]]++;
            }else{
                bucket[arr[i]]++;
            }
        }
        for(int i=0;i<2001;i++){
             if(bucket[i] !=0 && ++trace[bucket[i]] > 1)
                return false;
            trace[bucket[i]]++;
        }
        return true;
    }
    public int gcd(int a, int b){
        if(b==0) return a;
        return gcd(b,a%b);
    }
    public ListNode insertGreatestCommonDivisors(ListNode head) {
        ListNode temp = head;
        while(temp.next!=null)
        {
            ListNode var = new ListNode(gcd(temp.val,temp.next.val));
            var.next=temp.next;
            temp.next=var;
            temp=var.next;
        }
        return head;
    }
    public static void main(String[] args) {
        int[] arr =new Asteroid().asteroidCollision(new int[]{-2,-2,1,-2});
        for(int i=0;i<arr.length;i++)
            System.out.print(arr[i]+" ");
    }
}
