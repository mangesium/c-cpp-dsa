class maxOne {
    public int longestOnes(int[] nums, int k) {
        // Initialize two pointers, max to keep track of the start of the subarray,
        // and i to traverse the array.
        int max = 0, i = 0;

        // Start a while loop to traverse the array.
        while (i < nums.length) {
            // If the current element in the array is 0, decrement the available
            // 'k' flips.
            if (nums[i] == 0)
                k--;

            // If 'k' becomes negative, it means we have used up all 'k' flips.
            if (k < 0) {
                // Check if the element at the 'max' pointer is 0.
                // If it is 0, it means we need to increment 'k' as we are
                // moving the start of the subarray forward.
                if (nums[max] == 0)
                    k++;

                // Move the 'max' pointer one step forward, effectively shrinking
                // the window from the left.
                max++;
            }

            // Move the 'i' pointer to the right, expanding the window.
            i++;
        }

        // Finally, return the length of the longest subarray of 1s with at most 'k'
        // flips.
        return i - max;
    }
    public int sumOfUnique(int[] nums) {
        int bucket[] = new int[101];
        int sum=0;
        for(int i=0;i<nums.length;i++){
            bucket[nums[i]]++;
        }
        for(int i=0;i<101;i++){
            if(bucket[i]==1)
                sum+=i;
        }
        return sum;
    }

    public static void main(String[] args) {
        System.out.println(
                new maxOne().longestOnes(new int[] { 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1 }, 3));
    }
}
