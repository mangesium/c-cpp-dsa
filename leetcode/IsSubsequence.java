import java.util.HashSet;
import java.util.Set;

public class IsSubsequence {

    public boolean isSubsequence(String s, String t) {
        if (s.equals(""))
            return true;
        int k = 0, idx = -1;
        for (int i = 0; i < t.length(); i++) {
            if ((k < s.length()) && (s.charAt(k) == t.charAt(i)) && ((idx == -1) ? true : (idx < i))) {
                k++;
                idx = i;
            }
        }
        if ((k == s.length()))
            return true;

        return false;
    }

    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums1.length; i++) {
            set.add(nums1[i]);
        }
        Set<Integer> ret = new HashSet<>();
        for (int i = 0; i < nums2.length; i++) {
            if (set.contains(nums2[i])) {
                ret.add(nums2[i]);
            }
        }
        System.out.println(ret);
        int arr[] = new int[ret.size()];
        int k = 0;
        for (int part : ret) {
            arr[k] = part;
            k++;
        }

        return arr;
    }

    public String removeDuplicates(String s) {
        int top = -1;
        char[] stack = s.toCharArray();
        for (char c : stack) {
            if (top >= 0 && stack[top] == c) {
                top--;
            } else {
                stack[++top] = c;
            }
        }
        return new String(stack, 0, top + 1);
    }

    public static void main(String[] args) {
        System.out.println(new IsSubsequence().isSubsequence("abc", "anvbpopopic"));
    }

}
