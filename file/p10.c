/*	putw() is a function who writes integer in a file when mode of pointer is "w".
 *	if we want to read the file it must be "w+" i.e. write + read or it will read -1 
 *	i.e. EOF.
 */
#include<stdio.h>
void main(){
	
	FILE *fp=fopen("demo.txt","w+");
	int num1=10;
	int num2=20;

	printf("%ld\n",ftell(fp));
	putw(num1,fp);
	putw(num2,fp);

	printf("%ld\n",ftell(fp));
	rewind(fp);
	printf("%ld\n",ftell(fp));
	printf("%d\n",getw(fp));
	printf("%d\n",getw(fp));
}
