/*	fprintf() function helps us to write on a given file if try to write again and again it will alaways start from
 *	end of last string's end. 
 *	fprintf("name of file to write or append","mode of operation"); 
  */
#include<stdio.h>
void main(){

	FILE *fp=fopen("msquare.txt","w");
	fprintf(fp,"Mangesh : Money is made by sitting not trading.\n");
	fprintf(fp,"Ganesh  : Khota nako bolu.\n");
	fprintf(fp,"Mangesh : Mat man MC.\n");
	
	fprintf(stdout,"Money is made by sitting not trading\n");	//Written on moniter's file.
	fprintf(stderr,"Money is made by sitting not trading\n");	//Written on moniter's file.
}
