#include<stdio.h>
#include<unistd.h>

void main(){

	printf("InSide main\n");

	int retval=fork();
	if(retval==0){

		retval=fork();
		sleep(10);
		printf("fork() processs id = %d\n",getpid());
		printf("\n");
	}else{
		printf("Return a.out value fork() = %d\n",retval);
		printf("\n");
	}
}

