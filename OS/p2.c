#include<stdio.h>
#include<unistd.h>

void main(){

	printf("InSide main\n");

	int retval=fork();
	if(retval==0){

		for(int i=0;i<10;i++){
			printf("fork() processs id = %d\n",getpid());
			printf("a.out id = %d\n",getppid());
			printf("Return new value fork() = %d\n",retval);
			printf("\n");
			sleep(10);
		}

	}else{
		for(int i=0;i<10;i++){
			printf("a.out id = %d\n",getpid());
			printf("shell id = %d\n",getppid());
			printf("Return a.out value fork() = %d\n",retval);
			printf("\n");
			sleep(10);
		}
	}
}

