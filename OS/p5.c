#include<stdio.h>
#include<pthread.h>
#include<unistd.h>
int count=0;
void* fun(){
	count++;
	printf("********************thread = %d********************************\n",count);
	printf("Start fun\n");
	printf("%ld\n",pthread_self());
	printf("End fun\n");
	pthread_exit(NULL);		
}

void main(){

	long int tid;
	printf("Start main\n");
	for(int i=0;i<10;i++)
	pthread_create(&tid,NULL,fun,NULL);
	//pthread_exit(NULL);
	//sleep(5);	
	printf("%ld\n",pthread_self());
	//pthread_join(tid,NULL);
	printf("****************************End main*************************\n");
	pthread_exit(NULL);		
}
//how to compile following code
//cc p5.c -pthread 
