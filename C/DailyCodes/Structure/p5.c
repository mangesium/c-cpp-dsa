#include<stdio.h>
struct demo{
	int x;
	double y;
};
struct momo{
	char x;
	char y;
};
void main(){
	double x=100000.00;
	struct demo obj={100,2};
	struct momo mobj={'a','b'};
	printf("%p\n",x);
	printf("%p\n",obj);
	printf("%p\n",mobj);
	//obj++;		----->Error=Wrong type of argument to increment;
	printf("%lf\n",x);
	printf("%d\n",obj);
	printf("%c\n",mobj);
}
