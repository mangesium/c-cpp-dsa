#include<stdio.h>
struct sdemo{

	int x;
	int y;
}sobj;
void gfun(){

	printf("IN Global fun\n");
}

union udemo{
	int x;
	int y;
	//struct sdemo obj;
	void (*ptr)();
}uobj;
void fun(struct sdemo* sptr,union udemo* uptr){
	printf("In fun\n");
}
void main(){
	//fun(&sobj,&uobj);
	printf("%d\n",uobj.x);
	printf("%d\n",uobj.y);
	uobj.ptr=gfun;
	uobj.ptr();
	printf("%ld\n",sizeof(uobj));
}
