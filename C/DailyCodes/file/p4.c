//Here we open file in read mode if the given file is not available in given area it will return "NULL",
//If given file is available then it will return address to structure of that file
#include<stdio.h>
void main(){
	FILE *fp=fopen("notafile.txt","r");
	printf("%p\n",fp);
	FILE *fp1=fopen("demo.txt","r");
	printf("%p\n",fp1);
}
