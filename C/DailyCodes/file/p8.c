/*	ftell() shows us where last character is at.
 */
#include<stdio.h>
void main(){
	FILE *fp=fopen("info.txt","w");
	printf("%ld\n",ftell(fp));
	fprintf(fp,"AS Construction\n");
	printf("%ld\n",ftell(fp));
	fprintf(fp,"AS Construction Services\n");
	printf("%ld\n",ftell(fp));
	fprintf(fp,"Yogesh Suppliers\n");
	printf("%ld\n",ftell(fp));
	fprintf(fp,"Manjare Construction\n");
	printf("%ld\n",ftell(fp));
}
