/*	fscanf() is a function which helps us to read and write on a perticular file or variable
 *	fscanf("pointer to a file or variable for to read","format specifier","address of ptr to file or variable for to Write")
 */
#include<stdio.h>
void main(){
	FILE *fp=fopen("msquare.txt","a");
	char arr[100];
	char *carr=arr;
	printf("Enter Input String : ");
	fscanf(stdin,"%s",arr);
	while(*carr!='\0'){
		printf("%c",*carr);
		carr++;
	}
	printf("\n");
}
