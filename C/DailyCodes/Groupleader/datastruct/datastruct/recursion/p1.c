#include<stdio.h>


int reverse(int x){
	static int temp=0;
	if(x==0){
		return temp;
	}
	
	int rem=x%10;
	temp=temp*10+rem;
	reverse(x/10);
}



void main(){
	printf("%d\n",reverse(123));
}
