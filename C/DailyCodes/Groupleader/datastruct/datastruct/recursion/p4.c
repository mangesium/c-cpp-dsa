#include<stdio.h> 
#include<stdbool.h>
#include<string.h>

_Bool pelistr(char str[],int l){
	static int i=0;
	if(i>l/2)
	{
		return true;
	}

	if(str[i]!=str[l-i])
		return false;

	i++;

	pelistr(str,l);	
	
}

void main(){
	char str[]="madam";

	int l=sizeof(str)/sizeof(char);

	printf("%d\n",pelistr(str,l-2));
}

