#include <stdio.h>
#include <stdlib.h>

struct Node
{
	int data;
	struct Node *next;
};
struct Node *head = NULL;
struct Node *top = NULL;

void push(int data){

	struct Node *node = malloc(sizeof(struct Node));
	if (node == NULL)
	{
		return;
	}
	node->data = data;
	node->next = NULL;

	if (head == NULL)
	{
		head = node;
		top = node;
		return;
	}
	top->next = node;
	top = node;
}
int pop(){
	if (head == NULL)
	{
		printf("stack Underflow\n");
		return 0;
	}
	if (head == top)
	{
		int rm = top->data;
		printf("popped data is %d\n",rm);
		free(top);
		head = NULL;
		top = NULL;
		return rm;
	}

	struct Node *temp = head;
	while (temp->next != top)
	{
		temp = temp->next;
	}
	int rm = top->data;
	printf("popped data is %d\n",rm);
	free(top);
	top = temp;
	top->next = NULL;
	return rm;
}

void printStack()
{
	if (head == NULL)
	{
		return;
	}

	struct Node *temp = head;

	while (top != head)
	{
		temp = head;
		while (temp->next != top)
		{
			temp = temp->next;
		}
		printf("%d\n", top->data);
		top = temp;
	}
	printf("%d\n", top->data);
	while (top->next != NULL)
	{
		top = top->next;
	}
}

void main(){

	int rm = pop();
	push(10);
	push(20);
	push(30);
	printStack();
	pop();
	pop();
	pop();
	pop();
}

