#include<stdio.h>
#include<stdlib.h>

struct node{
	
	int data;
	struct node*next;
};

struct node *head=NULL;
//struct node *top=NULL;

void push(int data){
	
	struct node* newnode=malloc(sizeof(struct node));

	newnode->data=data;
	newnode->next=NULL;

	//top=newnode;

	if(head==NULL){

		head=newnode;
	}
	else{
		newnode->next=head;
		head=newnode;
	}
}

void printnode(){

	struct node*temp=head;

	while(temp!=NULL){

		printf("%d\n",temp->data);
		temp=temp->next;
	}
}

void pop(){
	struct node* temp=head;
	if(head==NULL){
	printf("Stack Underflow\n");
	}

	else{

	head=temp->next;
	free(temp);
	}
}

void main(){

	push(10);
	push(20);
	push(30);

	printnode();

	pop();
	printnode();
	
	pop();
	printnode();
	
	pop();
	printnode();
	pop();
	printnode();
	pop();
	printnode();



	
}

