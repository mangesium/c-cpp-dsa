#include <stdio.h>

void main(){

    int n;
    printf("Enter the size of array");
    scanf("%d", &n);
    int arr[n];
    printf("Enter elements of array");
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }
    int flag = 0;
    int index[n];
    for (int i = 0; i < n; i++){
        flag = 0;
        index[0] = i;
        for (int j = i+1; j < n; j++){
            if (arr[i] == arr[j]){
                index[flag+1] = j;
                flag++;
            }
        }
        if(flag!=0){
            for(int x = flag; x >= 0; x--){
                for(int y = index[x]; y < n-1; y++){
                    arr[y] = arr[y] + arr[y+1];
                    arr[y+1] = arr[y] - arr[y+1];
                    arr[y] = arr[y] - arr[y+1];
                }
                n--;
            }
            i--;
        }
    }
    for(int i = 0; i < n; i++){
        printf("%d ",arr[i]);
    }
}
