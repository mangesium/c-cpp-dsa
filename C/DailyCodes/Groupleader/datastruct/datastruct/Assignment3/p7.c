#include<stdio.h>

void main(){
    int x,y;
    printf("Enter number of rows :");
    scanf("%d",&x);

    printf("Enter number of col :");
    scanf("%d",&y);

    int arr[x][y];
    printf("Enter elements in array \n");

    for(int i=0;i<x;i++){
        for(int j=0;j<y;j++){
            scanf("%d",&arr[i][j]);
        }
    }

    for(int i=0;i<x;i++){
        int count=0;
        for(int j=0;j<y;j++){
            if(j%2==0 && count==2){
              	int temp=arr[i][j];
                arr[i][j]=arr[i][j-2];
                arr[i][j-2]=temp;
            }
            count++;
        }
    }
    printf("Result\n");
    for(int i=0;i<x;i++){
        for(int j=0;j<y;j++){
            printf("%d ",arr[i][j]);
        }
        printf("\n");
    }
}
