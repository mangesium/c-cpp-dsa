#include <stdio.h>
int digits(int n){
    int count = 0;
    for(; ;){
        n=n/10;
        count++;
        if(n==0){
            break;
        }
    }
    return count;
}
void main(){
    int num;
    printf("Enter the number :");
    scanf("%d",&num);
    int digi = digits(num);
    int arr[digi];
    for(int i = digi; i > 0; i--){
        arr[i] = num%10;
        num = num/10;
    }
    arr[digi]+=2;
    arr[0]=0;
    for(int i = digi; i >= 0; i--){
        if(digits(arr[i])==2 && i!=0){
            arr[i]=arr[i]%10;
            arr[i-1]++;
        }
        else if(digits(arr[i])==2 && i==0){
            arr[i]++;
        }
    }
    printf("\n[ ");
    for(int i = 0; i <= digi; i++){
        printf("%d ", arr[i]);
    }
    printf("]");
}
