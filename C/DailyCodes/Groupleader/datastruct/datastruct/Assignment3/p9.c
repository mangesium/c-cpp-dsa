#include<stdio.h>

void main(){

	int x;

	printf("Enter size of array\n");

	scanf("%d",&x);

	int arr[x];

	printf("Enter elements of array\n");

	for(int i=0;i<x;i++){

		scanf("%d",&arr[i]);
	}
	
	printf("Enter element to search in array\n");

	int num;
	int flag=0;

	scanf("%d",&num);

	for(int i=0;i<x;i++){

		if(num==arr[i]){

			printf("The element is at index %d\n",i);
			flag++;
		}
	}

	if(flag==0){

		printf("Element not present in array\n");
	}
}
