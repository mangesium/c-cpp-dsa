#include <stdio.h>
void main()
{
    int n = 3;
    int condn = n * 2 + 1;
    for(int i = 0; i < condn; i++){
        for(int j = 0; j < condn; j++){
            if ((i == 0 || i == condn - 1) && (j == 0 || j == condn - 1))
            {
                printf("  ");
            }
            else if (i==0 || i==condn-1 || j==0)
            {
                printf("* ");
            }
            else if (j >= condn - n && i >= 1)
            {
                if(j-1 == i || condn - j == i){
                    printf("* ");
                }
                else{
                    printf("  ");
                }
            }
            else{
                printf("  ");
            }
        }
        printf("\n");
    }
}
