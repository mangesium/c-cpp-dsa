#include<stdio.h>

int count=0;
int ternarysearch(int arr[],int size,int ele,int start,int end){

		
	count++;
	if(start>end)
		return -1;
		
		if(arr[start]==ele)
			return start;
		if(arr[end]==ele)
			return end;

		int mid1=start+(end-start)/3;
		int mid2=end-(end-start)/3;
		int mid = (mid1+mid2)/2;
		
		if(arr[mid1]==ele)
			return mid1;
		if(arr[mid2]==ele)
			return mid2;
		
		if(arr[mid]==ele)
			return mid;
		
		if(arr[mid1]>ele)
			return ternarysearch(arr,size,ele,start,mid1-1);

		else if(arr[mid2]<ele)
			return ternarysearch(arr,size,ele,mid2+1,end);
		
		else{
			return ternarysearch(arr,size,ele,mid1+1,mid2-1);
		}

		if(start==end && arr[start]==ele)
			return start;

		
	

}

void main(){

	int size;
	printf("Enter size of array: ");
	scanf("%d",&size);
	int arr[size];
	printf("Enter elements of array\n");
	for(int i=0;i<size;i++)
	{
		scanf("%d",&arr[i]);
	}

	int ele;
	printf("Enter element to search\n");
	scanf("%d",&ele);
	int start=0;
	int end=size-1;

	int index=ternarysearch(arr,size,ele,start,end);

	printf("itrations are %d\n",count);

	if(index==-1)
		printf("Element not present in array\n");
	else
		printf("Element present at index %d\n",index);
}

