#include<stdio.h>


int linearsearch(int arr[],int size,int ele){

/*	for(int i=0;i<size;i++)
	{
		if(arr[i]==ele)
			return i;
	}

	return -1;*/

	static int i=0;

	if(i==size)
		return -1;

	if(arr[i]==ele)
		return i;

	i++;
	linearsearch(arr,size,ele);

}
void main(){

	int size;
	printf("Enter size of array: ");
	scanf("%d",&size);
	int arr[size];
	printf("Enter elements of array\n");
	for(int i=0;i<size;i++)
	{
		scanf("%d",&arr[i]);
	}

	int ele;
	printf("Enter element to search\n");
	scanf("%d",&ele);

	int index=linearsearch(arr,size,ele);

	if(index==-1)
		printf("Element not present in array\n");
	else
		printf("Element present at index %d\n",index);
}

