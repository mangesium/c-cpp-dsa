#include<stdio.h>


int binarysearch(int arr[],int size,int ele,int start,int end){

/*	while(start<=end)
	{
		int mid=(start+end)/2;
		if(arr[start]==ele)
			return start;

		if(arr[end]==ele)
			return end;

		if(arr[mid]==ele)
			return mid;

		if(arr[mid]<ele)
			start=mid+1;
		else
			end=mid-1;

		if(arr[end]==ele)
			return end;
	}

	return -1;*/

	if(start>end)
		return -1;

		int mid=(start+end)/2;

		if(arr[start]==ele)
			return start;

		if(arr[end]==ele)
			return end;

		if(arr[mid]==ele)
			return mid;

		if(arr[mid]<ele)
			return binarysearch(arr,size,ele,mid+1,end);
		else
			return binarysearch(arr,size,ele,start,mid-1);
				

}

void main(){

	int size;
	printf("Enter size of array: ");
	scanf("%d",&size);
	int arr[size];
	printf("Enter elements of array\n");
	for(int i=0;i<size;i++)
	{
		scanf("%d",&arr[i]);
	}

	int ele;
	printf("Enter element to search\n");
	scanf("%d",&ele);
	int start=0;
	int end =size-1;

	int index=binarysearch(arr,size,ele,start,end);

	if(index==-1)
		printf("Element not present in array\n");
	else
		printf("Element present at index %d\n",index);
}

