#include<stdio.h>
int count=0;
int interpolation(int arr[],int size,int ele){

	int start=0;
	int end=size-1;

	if(arr[start]==ele)
		return start;
	if(arr[end]==ele)
		return end;

	while(start<=end)
	{
		count++;
		int index=start+(end-start)*(ele-arr[start])/(arr[end]-arr[start]);
		if(arr[index]==ele)
			return index;

		if(arr[index]<ele)
			start=index+1;

		if(arr[index]>ele)
			end=index-1;
	}

	return -1;
}

void main(){
	int size;
	printf("Enter size of array:");
	scanf("%d",&size);

	int arr[size];
	printf("Enter elements of array.*elements must be sorted and uniform\n");
	for(int i=0;i<size;i++)
	{
		scanf("%d",&arr[i]);
	}
	int ele;
	printf("Enter element to search in array:");
	scanf("%d",&ele);

	int index=interpolation(arr,size,ele);

	printf("itrations are %d\n",count);

	if(index==-1)
		printf("Element not found in array\n");
	else
		printf("Element found at index %d\n",index);
}
