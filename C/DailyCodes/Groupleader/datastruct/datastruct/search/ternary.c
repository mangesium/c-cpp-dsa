#include<stdio.h>

int count=0;
int ternarysearch(int arr[],int size,int ele){

	int start =0;
	int end = size-1;
		if(arr[start]==ele)
			return start;
		if(arr[end]==ele)
			return end;

	while(start<end)
	{	
		count++;
		int mid1=start+(end-start)/3;
		int mid2=end-(end-start)/3;
		if(arr[mid1]==ele)
			return mid1;
		if(arr[mid2]==ele)
			return mid2;
		if(arr[mid1]>ele)
			end=mid1-1;
		else if(arr[mid2]<ele)
			start=mid2+1;
		else{
			start=mid1+1;
			end=mid2-1;
		}

		if(start==end && arr[start]==ele)
			return start;
	}
	return -1;

}

void main(){

	int size;
	printf("Enter size of array: ");
	scanf("%d",&size);
	int arr[size];
	printf("Enter elements of array\n");
	for(int i=0;i<size;i++)
	{
		scanf("%d",&arr[i]);
	}

	int ele;
	printf("Enter element to search\n");
	scanf("%d",&ele);

	int index=ternarysearch(arr,size,ele);

	printf("itrations are %d\n",count);

	if(index==-1)
		printf("Element not present in array\n");
	else
		printf("Element present at index %d\n",index);
}

