#include <stdio.h>

/*int x=10;          here scope of  x is global stored in data section. pointer can access easily to that address. */         

int* fun(){

	int x =10;            /* her scope of x is local as stack pops out &x  becomes out of process also while compiling it gives us warning */
	 return &x;


}

void main(){

	int *iptr=fun();

	printf("%d\n",*iptr);
}

