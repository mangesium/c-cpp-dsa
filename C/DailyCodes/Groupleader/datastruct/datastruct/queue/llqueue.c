#include<stdio.h>
#include<stdlib.h>

struct node{

	int data;
	struct node*next;
};

struct node* front=NULL;
struct node* rear=NULL;
void enqueue(int data){
	struct node* newnode=malloc(sizeof(struct node));
		newnode->data=data;
		newnode->next=NULL;

		if(newnode==NULL){

			printf("Queue is full\n");
			return ;
		}

		if(front==NULL&&rear==NULL){
			front=newnode;
			rear=newnode;
		}
		else{
			rear->next=newnode;
			rear=newnode;
		}
 
}

int dequeue(){

	if(front==NULL){

		printf("Queue is empty\n");
		return 0;
	}else{

		struct node* temp=front;
		int del=temp->data;
		front=front->next;
		free(temp);
		return del;
	}
}



void printqueue(){
	if(front==NULL){

		printf("Queue is empty\n");
		return;
	}
	else{
	struct node*temp=front;

	while(temp!=NULL)
	{
		printf("|%d|",temp->data);
		temp=temp->next;
	}
	printf("\n");
	}
}

void main(){

	enqueue(10);
	enqueue(20);
	enqueue(30);
	enqueue(40);
	enqueue(50);
	enqueue(60);
	enqueue(70);
	enqueue(80);
	printqueue();	

	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();
	dequeue();
	printqueue();

}

