#include<stdio.h>

int count=0;
void bubblesort(int arr[],int size){

	int flag=0;

	if(size==1)
		return;

	for(int i=0;i<size-1;i++){

		if(arr[i]>arr[i+1])	
		{
			int temp=arr[i];	
			arr[i]=arr[i+1];
			arr[i+1]=temp;
			flag=1;
		}
	}
	
		if(flag==0)
			return;

		size=size-1;
	
		return bubblesort(arr,size);
	
} 
void main(){

	int size;
	printf("Enter size of array:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter elements of array\n");

	for(int i=0;i<size;i++){

		scanf("%d",&arr[i]);
	}

	printf("Array Before Sorting\n");

	for(int i=0;i<size;i++)
	{
		printf("%d ",arr[i]);
	}
	printf("\n");
	
	bubblesort(arr,size);
	
	//printf("itrations %d\n",count);
	printf("Array after Sorting\n");

	for(int i=0;i<size;i++)
	{
		printf("%d ",arr[i]);
	}
}
