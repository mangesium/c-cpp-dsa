#include <stdio.h>
void bubblesort(int arr[], int size)
{
	int flag = 0;
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
				flag = 1;
			}
		}
		if (flag == 0)
			return;
	}
}

void main()
{

	int size;
	printf("Enter size of array:");
	scanf("%d", &size);

	int arr[size];

	printf("Enter elements of array\n");

	for (int i = 0; i < size; i++)
	{

		scanf("%d", &arr[i]);
	}

	printf("Array Before Sorting\n");

	for (int i = 0; i < size; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
	bubblesort(arr, size);

	printf("Array after Sorting\n");

	for (int i = 0; i < size; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
