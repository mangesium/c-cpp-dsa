#include<stdio.h>

void merge(int *arr,int start,int mid ,int end){

	int p=end-start+1;
	int A[p];
	int i=start,j=mid+1,k=0;
	while(i<=mid&&j<=end){
		if(arr[i]<=arr[j]){
			A[k]=arr[i];
			i++;
		}else{
			A[k]=arr[j];
			j++;
		}
		k++;
	}

	while(i<=mid){
		A[k]=arr[i];
		i++;
		k++;
	}

	while(j<=end){
		A[k]=arr[j];
		j++;
		k++;
	}
	
	for(int i=0,j=start;i<p;i++,j++){
		arr[j]=A[i];
	}

}

void mergesort(int arr[],int start,int end){
	
	if(start<end){

		int mid=(start+end)/2;

		mergesort(arr,start,mid);
		mergesort(arr,mid+1,end);
		merge(arr,start,mid,end);
	}

}

void main(){

	int size;
	printf("Enter size of array: ");
	scanf("%d",&size);     
	int arr[size];
	printf("Enter Elements in array\n");

	for(int i=0;i<size;i++){

		scanf("%d",&arr[i]);
	}

	printf("Array before sort\n");

	for(int i=0;i<size;i++){

		printf("%d ",arr[i]);
	}

	int start=0;
	int end =size-1;

	mergesort(arr,start,end);
	
	printf("\nArray after sort\n");

	for(int i=0;i<size;i++){

		printf("%d ",arr[i]);
	}
	printf("\n");

}
