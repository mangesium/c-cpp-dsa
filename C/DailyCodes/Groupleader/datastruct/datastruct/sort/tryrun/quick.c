#include<stdio.h>
void swap(int *x,int *y){
	int temp=*x;
	*x=*y;
	*y=temp;
}

int part(int arr[],int start,int end){

	int pivot = arr[end];

	int index=start;

	for(int i=start;i<end;i++){

		if(arr[i]<=arr[end]){
			swap(&arr[index],&arr[i]);
			index++;
		}
	}
	swap(&arr[end],&arr[index]);

	return index;
}

void quick(int arr[],int start,int end){


	if(start<end){

		int pivot=part(arr,start,end);
		quick(arr,start,pivot-1);
		quick(arr,pivot+1,end);

	}
}

void main(){
	
	int arr[]={5,4,3,2,1};

	int len=sizeof(arr)/sizeof(int);
	printf("Before sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
	int start=0;
	int end=len-1;
	quick(arr,start,end);
	printf("After sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
}
