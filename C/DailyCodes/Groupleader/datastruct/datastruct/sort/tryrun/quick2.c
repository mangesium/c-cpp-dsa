#include<stdio.h>
void quick(int arr[],int start,int end);
int fun(int arr[],int start,int end);
void swap(int*x,int*y);
void main(){
    int arr[] ={10,9,8,7,6,5,4,3,2,1};
    int len=sizeof(arr)/sizeof(int);
    int start=0,end=len-1;

    quick(arr,start,end);

    for(int i=0;i<len;i++){
        printf("%d ",arr[i]);
    }
    printf("\n");
}

void quick(int arr[],int start,int end){
    if(start<end){

        int pivot=fun(arr,start,end);
        
        quick(arr,start,pivot-1);
        quick(arr,pivot+1,end);
    }
}

int fun(int arr[],int start,int end){

    int pivot=arr[end];
    int index=start;

    for(int i=start;i<end;i++){
        if(arr[i]<=arr[end]){
            swap(&arr[i],&arr[index]);
            index++;
        }
    }
    swap(&arr[end],&arr[index]);
    return index;
}

void swap(int*x,int*y){
    int temp=*x;
    *x=*y;
    *y=temp;
}