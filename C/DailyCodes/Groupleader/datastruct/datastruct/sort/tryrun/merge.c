#include<stdio.h>

void mergesort(int arr[],int start,int end,int mid){

	int x,y;

	x=mid-start+1;
	y=end-mid;
	int arr1[x],arr2[y];

	for(int i=0;i<x;i++)
		arr1[i]=arr[start+i];
	
	for(int i=0;i<y;i++)
		arr2[i]=arr[mid+1+i];

	int i=0,j=0,t=start;

	while(i<x&&j<y){

		if(arr1[i]<arr2[j]){
			arr[t]=arr1[i];
			i++;
		}
		else{
			arr[t]=arr2[j];
			j++;
		}
		t++;
	}
	while(i<x){
		arr[t]=arr1[i];
		i++;
		t++;
	}
	while(j<y){
		arr[t]=arr2[j];
		j++;
		t++;
	}
}
void merge(int arr[],int start,int end){

	if(start<end){

		int mid =(start+end)/2;

		merge(arr,start,mid);
		merge(arr,mid+1,end);
		mergesort(arr,start,end,mid);
	}
}


void main(){

	int arr[]={5,4,3,2,1};

	int len=sizeof(arr)/sizeof(int);
	printf("Before sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
	int start=0;
	int end=len-1;
	merge(arr,start,end);
	printf("After sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
}
