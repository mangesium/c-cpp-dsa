#include<stdio.h>

void mergesort(int arr[],int start,int end,int mid){


	int i=start,j=mid+1,t=start,l=start+end;
	
	int arr1[l];

	while(i<=mid&&j<end){

		if(arr[i]<=arr[j]){
			arr1[t]=arr1[i];
			i++;
		}
		else{
			arr[t]=arr[j];
			j++;
		}
		t++;
	}
	while(i<=mid){
		arr1[t]=arr[i];
		i++;
		t++;
	}
	while(j<end){
		arr[t]=arr[j];
		j++;
		t++;
	}
	for(int i=start;i<end;i++){
		arr[i]=arr1[i];
	}
}
void merge(int arr[],int start,int end){

	if(start<end){

		int mid =(start+end)/2;

		merge(arr,start,mid);
		merge(arr,mid+1,end);
		mergesort(arr,start,end,mid);
	}
}


void main(){

	int arr[]={5,4,3,2,1};

	int len=sizeof(arr)/sizeof(int);
	printf("Before sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
	int start=0;
	int end=len-1;
	merge(arr,start,end);
	printf("After sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
}
