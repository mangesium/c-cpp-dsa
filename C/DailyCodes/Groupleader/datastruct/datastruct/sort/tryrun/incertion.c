#include<stdio.h>

void insertion(int arr[],int len){

	for(int i=1;i<len;i++){

		int temp=arr[i];
		int j=0;

		for(j=i-1;j>=0&&temp<arr[j];j--){

			arr[j+1]=arr[j];			
		}
		arr[j+1]=temp;
	}
}

void main(){

	int arr[]={5,4,3,2,1};

	int len=sizeof(arr)/sizeof(int);
	printf("Before sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
	insertion(arr,len);
	printf("After sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
}
