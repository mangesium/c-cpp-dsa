#include<stdio.h>

void bubble(int arr[],int len){

	for(int i=0;i<len;i++){

		for(int j=0;j<len-1-i;j++){

			if(arr[j]>arr[j+1]){
				int swap=arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=swap;
			}
		}
	}
}

void main(){

	int arr[]={5,4,3,2,1};

	int len=sizeof(arr)/sizeof(int);
	printf("Before sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
	bubble(arr,len);
	printf("After sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
}
