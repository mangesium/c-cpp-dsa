#include<stdio.h>

void selection(int arr[],int len){

	for(int i=0;i<len;i++){

		for(int j=i;j<len;j++){

			if(arr[i]>arr[j]){
				int swap=arr[i];
				arr[i]=arr[j];
				arr[j]=swap;
			}
		}
	}
}

void main(){

	int arr[]={5,4,3,2,1};

	int len=sizeof(arr)/sizeof(int);
	printf("Before sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
	selection(arr,len);
	printf("After sorting\n");
	for(int i=0;i<len;i++){
		printf("%d ",arr[i]);
	}
}
