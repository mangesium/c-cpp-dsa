#include<stdio.h>
void insertionsort(int arr[],int len){

	for(int i=1;i<len;i++){
		int temp=arr[i];
		int j;
		for(j=i-1;j>=0&&temp<arr[j];j--){

			arr[j+1]=arr[j];
		}
		arr[j+1]=temp;
	}
}
void main(){
	int size;
	printf("Enter size of array: ");
	scanf("%d",&size);
	int arr[size];
	printf("Enter Elements in array\n");

	for(int i=0;i<size;i++){

		scanf("%d",&arr[i]);
	}

	printf("Array before sort\n");

	for(int i=0;i<size;i++){

		printf("%d ",arr[i]);
	}

	int start=0;
	int end =size-1;

	insertionsort(arr,size);
	
	printf("\nArray after sort\n");

	for(int i=0;i<size;i++){

		printf("%d ",arr[i]);
	}
	printf("\n");

}
