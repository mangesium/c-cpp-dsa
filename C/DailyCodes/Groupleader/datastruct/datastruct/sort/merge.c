#include<stdio.h>

void merge(int *arr,int start,int mid ,int end){

	int x=mid-start+1;
	int y=end-mid;
	int arr1[x];
	int arr2[y];

	for(int i=0;i<x;i++){
		arr1[i]=arr[start+i];
	}
	for(int i=0;i<y;i++){
		arr2[i]=arr[mid+1+i];
	}

	int i=0,j=0,t=start;

	while(i<x&&j<y){

		if(arr1[i]<arr2[j]){
			arr[t]=arr1[i];
			i++;
		}
		else{
			arr[t]=arr2[j];
			j++;
		}
		t++;
	}

	while(i<x){
		arr[t]=arr1[i];
		i++;
		t++;
	}

	while(j<y){
		arr[t]=arr2[j];
		j++;
		t++;
	}
	
}

void mergesort(int arr[],int start,int end){
	
	if(start<end){

		int mid=(start+end)/2;

		mergesort(arr,start,mid);
		mergesort(arr,mid+1,end);
		merge(arr,start,mid,end);
	}

}

void main(){

	int size;
	printf("Enter size of array: ");
	scanf("%d",&size);     
	int arr[size];
	printf("Enter Elements in array\n");

	for(int i=0;i<size;i++){

		scanf("%d",&arr[i]);
	}

	printf("Array before sort\n");

	for(int i=0;i<size;i++){

		printf("%d ",arr[i]);
	}

	int start=0;
	int end =size-1;

	mergesort(arr,start,end);
	
	printf("\nArray after sort\n");

	for(int i=0;i<size;i++){

		printf("%d ",arr[i]);
	}
	printf("\n");

}
