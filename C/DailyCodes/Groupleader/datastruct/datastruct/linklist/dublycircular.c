#include<stdio.h>
#include<stdlib.h>

struct Node{

	struct Node* prev;

	int data;

	struct Node* next;
};

struct Node *head=NULL;
struct Node *tail=NULL;

void printlist(){

	struct Node *temp = head;

	do{
	
		printf("|%d|->",temp->data);
		temp=temp->next;

	}while(temp!=head);

	/*if(head!=tail){
		printf("|%d|->",temp->data);
	}*/
}

void addAtfirst(){

	struct Node* newNode = malloc(sizeof(struct Node));

	printf("\nEnter data in firstnode\n");

	scanf("%d",&newNode->data);

	newNode->next=head;
	head->prev=newNode;
	newNode->prev=tail;
	tail->next=newNode;
	head=newNode;

	/*newNode->next=head;
	newNode->prev=tail;
	tail->next=newNode;
	head=newNode; */

}

void addAtLast(){

	struct Node* newNode = malloc(sizeof(struct Node));
	
	printf("\nEnter data in lastnode\n");

	scanf("%d",&newNode->data);

	newNode->prev=tail;
	tail->next=newNode;
	newNode->next=head;
	head->prev=newNode;
	tail=newNode;
	
}

void addAtpos(int pos){


	struct Node* newNode = malloc(sizeof(struct Node));
	
	printf("Enter data in node\n");

	scanf("%d",&newNode->data);

	struct Node *temp=head;

	/*if(pos==1){

		deleteFirst();
	}*/

	

	while(pos-2){

		temp=temp->next;
		pos--;
		
	}

	newNode->prev=temp;
	newNode->next=temp->next;
	temp->next->prev=newNode;
	temp->next=newNode;
		
	
}
void deleteFirst(){

	struct Node* temp=head;

	head=head->next;
	tail->next=head;
	free(temp);
}

void deleteLast(){

	struct Node* temp=head;

	while(temp->next->next!=head)
	{
		temp=temp->next;
	}

	temp->next=head;
	tail=temp;
}

void count(){

	int cnt=0;
	struct Node* temp=head;

	do{
		cnt++;

		temp=temp->next;

	}while(temp!=head);

	printf("%d",cnt);
}


void deleteAtpos(int pos){
		
	struct Node* temp=head;

	if(pos==1){

		deleteFirst();
	}

	else{

	while(pos-1)
	{
		temp=temp->next;
		pos--;
	}

	temp->prev->next=temp->next;
	temp->next->prev=temp->prev;

	if(temp->next==head){

		tail=temp->prev;
		free(temp);
	}

		else{

			free(temp);
			}
	}
}

void reverse(){

	struct Node* temp=tail;

	do{

		printf("|%d|->",temp->data);

		temp=temp->prev;
	}while(temp!=tail);

	count();

}



void main(){

	struct Node* newNode = malloc(sizeof(struct Node));

	head = tail= newNode;
	

	newNode->prev=newNode;
	newNode->data=10;
	newNode->next=newNode;

	printlist();
	count();

	addAtfirst();
	printlist();
	count();

	addAtLast();
	printlist();
	count();
	
	int pos;
	printf("Enter position\n");
	scanf("%d",&pos);
	addAtpos(pos);
	printlist();
	count();

	printf("\nReverse\n");
	reverse();

	printf("\nDelete first node\n");
	deleteFirst();
	printlist();
	count();

	printf("\nDelete last node\n");
	deleteLast();
	printlist();
	count();

	printf("\nDelete at position\n");
	int x;
	printf("Enter position\n");
	scanf("%d",&x);
	deleteAtpos(x);
	printlist(); 
	count();

}
	
