#include<stdio.h>
#include<stdlib.h>

	struct Demo{

		int age;
		struct Demo* next;
	};

	void main(){

		struct Demo* ptr1=(struct Demo*)malloc(sizeof(struct Demo));

		ptr1->age=21;

		ptr1->next=NULL;


		struct Demo* ptr2=(struct Demo*)malloc(sizeof(struct Demo));

		ptr2->age=22;

		ptr2->next=NULL;

		ptr1->next=ptr2;

		 ptr2=(struct Demo*)malloc(sizeof(struct Demo));

		ptr2->age=31;

		ptr2->next=NULL;

		ptr1->next->next=ptr2;

		ptr2=NULL;

		while(ptr1!=NULL){

			printf("|%d|",ptr1->age);
			printf("%p|->",ptr1->next);

			ptr1=ptr1->next;
		}
			printf("\n");
			printf("|%p|\n",ptr2);
	}

