#include<stdio.h>
int fact(int n){
    int temp=1;
    while(n!=0){
        temp*=n;
        n--;
    }
    return temp;
}
int C(int n,int r){

    return (fact(n)/fact(n-r)/fact(r));

}
void main(){
    int row=0;
    printf("Enter No. of rows : ");
    scanf("%d",&row);
    int k=0,r=0;
    for(int i=1;i<=row;i++){
        k=1;
        r=0;
        for(int j=1;j<=2*row-1;j++){
            if(j>=row+1-i&&j<=row+i-1&&k){
                printf("%2d",C(i-1,r));
                r++;
                k=0;
            }else{
                printf("  ");
                k=1;
            }
        }
        printf("\n");
    }
}