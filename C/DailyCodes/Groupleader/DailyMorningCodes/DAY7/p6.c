#include<stdio.h>

void main(){
    int num=0;
    printf("Enter number : ");
    scanf("%d",&num);
    int sum=0;
    while(num!=0){
        int rem=num%10;
        if(rem%2!=0){
            sum+=rem;
        }
        num/=10;
    }
    printf("%d is the sum of all odd digits from number",sum);
}