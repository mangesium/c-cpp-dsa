/*
      1       
    1 2 1
  1 2 3 2 1
1 2 3 4 3 2 1
*/
#include<stdio.h>
void main(){
    int row=-1;
    while(row<=0){
        printf("Enter No of Rows : ");
        scanf("%d",&row);
    }
    int k=0,i=1,j=1;
    for(i=1;i<=row;i++){
        int num=1;
        for(j=1;j<=2*row-1;j++){
            if(j>=row-k&&j<=row+k){
                printf("%d ",num);
                if(j>=row)
                    num--;
                else
                    num++;
            }else{
                printf("  ");
            }
        }
        printf("\n");
        k++;
    }
}