#include<stdio.h>
void main(){
    int num=0;
    printf("Enter number : ");
    scanf("%d",&num);
    int temp=num,sum=0,freq=0;
    while(temp!=0){
        int rem=temp%10;
        freq=1;
        while(rem!=0){
            freq*=rem;
            rem--;
        }
        sum+=freq;
        temp/=10;
    }
    if(sum==num)
        printf("%d is a Strong Number\n",sum);
    else
        printf("%d is not a Strong Number\n",num);
}