#include<stdio.h>
#include<limits.h>
void main(){
    int num=0;
    printf("Enter Number : ");
    scanf("%d",&num);
    int temp=num,min=10,max=-10;
    while(temp!=0){
        int rem=temp%10;
        if(rem>max)
            max=rem;
        if(rem<min)
            min=rem;
        temp/=10;
    }
    printf("max digit = %d,min digit = %d\n",max,min);
}