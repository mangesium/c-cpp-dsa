#include<stdio.h>
#include<stdbool.h>
bool isprime(int num){
    int count=0;
    for(int j=1;j<=num/2;j++){
            if(num%j==0)
                count++;
            if(count>1)
                break;
        }
        if(count==1)
            return true;
        return false;
}


void main(){
    int num=0;
    printf("Enter Number : ");
    scanf("%d",&num);
    int flag=0,val1=0,val2=0,sum=num,q=0,p=0;
    for(int i=2;i<num;i++){
        p=0;
        val1=i;
        if(isprime(val1))
            p=val1;
        else
            continue;
        for(int k=num-1;k>=2;k--){
            val2=k;
            q=0;
            if(isprime(val2))
                q=val2;
            else
                continue;
            if(p+q==sum){
                printf("%d + %d = %d\n",p,q,sum);
                flag=1;
                break;
            }
        }
        if(flag==1)
            break;
    }
    if(flag==0)
        printf("%d number is not a addition of two prime numbers\n",num);


}