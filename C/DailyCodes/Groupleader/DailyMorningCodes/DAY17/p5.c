#include<stdio.h>
void main(){
    int n=0;
    printf("Enter count of numbers : ");
    scanf("%d",&n);
    float arr[n],sum=0;
    for(int i=0;i<n;i++){
        printf("Enter Number : ");
        scanf("%f",&arr[i]);
        sum+=arr[i];
    }
    float Avg=sum/n;
    float variance=0;
    for(int i=0;i<n;i++){
        variance+=((arr[i]-Avg)*(arr[i]-Avg))/(n-1);
    }
    printf("Variance of Number = %f\n",variance);
}