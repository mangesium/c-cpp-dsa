#include<stdio.h>
#include<limits.h>
void maxmin(int *arr,int *max1,int *min1,int len){
    int max=INT_MIN,min=INT_MAX;
    for(int i=0;i<len;i++){
        if(arr[i]>max)
            max=arr[i];
        if(min>arr[i])
            min=arr[i];
    }    
    *max1=max;
    *min1=min;
}
void main(){
    int num=0;
    printf("Enter Number : ");
    scanf("%d",&num);
    int temp=num;
    int ecnt=0,ocnt=0;
    while(temp!=0){
        int rem=temp%10;
        if(rem%2==0)
            ecnt++;
        else
            ocnt++;
        temp/=10;
    }
    temp=num;
    int earr[ecnt];
    int oarr[ocnt];
    int i=ecnt-1,j=ocnt-1;
    while(temp!=0){
        int rem=temp%10;
        if(rem%2==0){
            earr[i]=rem;
            i--;
        }else{
            oarr[j]=rem;
            j--;
        }
        temp/=10;
    }
    int emax=INT_MIN,emin=INT_MAX;
    printf("Even Array : ");
    for(int i=0;i<ecnt;i++)
        printf("%d ",earr[i]);
    printf("\n");
    maxmin(earr,&emax,&emin,ecnt);
    printf("max=%d,min=%d\n",emax,emin);
    printf("Even Array : ");
    for(int i=0;i<ocnt;i++)
        printf("%d ",oarr[i]);
    emax=INT_MIN,emin=INT_MAX;
    printf("\n");
    maxmin(oarr,&emax,&emin,ocnt);
    printf("max=%d,min=%d\n",emax,emin);
    
}