#include <stdio.h>
int facto(int n)
{
    int fact = 1;
    while (n > 0)
    {
        fact *= n;
        n--;
    }
    return fact;
}
int digicount(int n)
{
    int count = 0;
    while (n)
    {
        count++;
        n /= 10;
    }
    return count;
}
int divisorsum(int n)
{
    int sum = 0;
    for (int i = 1; i <= n / 2; i++)
    {
        if (n % i == 0)
            sum += i;
    }
    return sum;
}
void isperfect(int n)
{
    if (n == divisorsum(n))
        printf("%d is a Perfect Number\n", n);
    else
        printf("%d is not a Perfect Number\n", n);
}
void isamicable(int n, int m)
{
    int num1 = divisorsum(n);
    int num2 = divisorsum(m);
    if (num1 == m && num2 == n)
        printf("%d and %d are Amicable Numbers\n", n, m);
    else
        printf("%d and %d are not Amicable Numbers\n", n, m);
}
int digisum(int n)
{
    int sum = 0;
    while (n != 0)
    {
        sum += n % 10;
        n /= 10;
    }
    return sum;
}
void isdisarium(int n)
{
    int count = 0;
    int sum = 0, temp = n;
    while (temp != 0)
    {
        count = (temp);
        int rem = temp % 10;
        int prod = 1;
        while (count)
        {
            prod *= rem;
            count--;
        }
        sum += prod;
    }
    if (sum == n)
        printf("%d is a Disarium Number\n", n);
    else
        printf("%d is not a Disarium Number\n", n);
}
void isdeficient(int n)
{
    int sum = 0;
    for (int i = 1; i <= n / 2; i++)
    {
        if (n % i == 0)
        {
            sum += i;
        }
    }
    if (sum < n)
        printf("%d is a Deficient Number\n", n);
    else
        printf("%d is not a Deficient Number\n", n);
}
void issqroot(int n)
{
    float sq = n / 2, temp = 0;
    while (sq != temp)
    {
        temp = sq;
        sq = (n / sq + sq) / 2;
    }
    printf("%f is the Squareroot of following Number\n", sq);
}
void isharshad(int n)
{
    int num = digisum(n);
    if (n % num == 0)
        printf("%d is a Harshad Number\n", n);
    else
        printf("%d is not a Harshad Number\n", n);
}
void isautomorphic(int num)
{
    long long int sq = num * num;
    int sum1 = 0, sum2 = 0, n = num;
    while (num != 0)
    {
        sum1 = sum1 * 10 + num % 10;
        num /= 10;
        sum2 = sum2 * 10 + sq % 10;
        sq /= 10;
    }
    if (sum1 == sum2)
        printf("%d is an  Automorphic number\n", n);
    else
        printf("%d is not an Automorphic number\n", n);
}
void isDuck(int num)
{
    int flag = 0, n = num;
    if (num == 0)
        flag = 1;
    while (n != 0)
    {
        if (n % 10 == 0)
        {
            flag = 1;
            break;
        }
        n /= 10;
    }
    if (flag == 1)
        printf("Enter Number is a Duck Number\n", n);
    else
        printf("Enter Number is not a Duck Number\n", n);
}
void ispalindrome(int n)
{
    int num = 0, val = n;
    while (val != 0)
    {
        num = num * 10 + val % 10;
        val /= 10;
    }
    if (num == n)
        printf("%d is Pelindrome Number\n", n);
    else
        printf("%d is Pelindrome Number\n", n);
}
void isarmstrong(int n)
{
    int count = digicount(n);
    int temp = n, sum = 1, prod = 1;
    while (temp != 0)
    {
        prod = 1;
        int cnt = count;
        int rem = temp % 10;
        while (cnt)
        {
            prod *= rem;
            cnt--;
        }
        sum += prod;
        temp /= 10;
    }
    if (prod == n)
        printf("%d is an Armstrong Number\n", n);
    else
        printf("%d is not an Armstrong Number\n", n);
}
void isstrong(int n)
{
    int temp = n, fact = 0;
    while (temp != 0)
    {
        int rem = temp % 10;
        fact += facto(rem);
        temp /= 10;
    }
    if (fact == n)
        printf("%d is a Strong Number\n", n);
    else
        printf("%d is a not Strong Number\n", n);
}
void iscomposit(int n)
{
    int count = 0;
    for (int i = 1; i <= n / 2; i++)
    {
        if (n % i == 0)
            count++;
        if (count > 1)
            break;
    }
    if (count == 1)
        printf("%d is a Prime Number\n", n);
    else
        printf("%d is a Composit Number\n", n);
}
void isprime(int n)
{
    int count = 0;
    for (int i = 1; i <= n / 2; i++)
    {
        if (n % i == 0)
            count++;
        if (count > 1)
            break;
    }
    if (count == 1)
        printf("%d is a Prime Number\n", n);
    else
        printf("%d is not a Prime Number\n", n);
}
void main()
{
    int x = 0;
    printf("1.Prime Number\n");
    printf("2.Composit Number\n");
    printf("3.Strong Number\n");
    printf("4.Armstrong Number\n");
    printf("5.Palindrome Number\n");
    printf("6.Duck Number\n");
    printf("7.Automorphic Number\n");
    printf("8.Harshad Number\n");
    printf("9.Squareroot of a Number\n");
    printf("10.Deficient Number\n");
    printf("11.Deserium Number\n");
    printf("12.Amicable Number\n");
    printf("13.Perfect Number\n");
    printf("Enter Number to check : ");
    scanf("%d", &x);
    switch (x)
    {
    case 1:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isprime(num);
        break;
    }
    case 2:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        iscomposit(num);
        break;
    }
    case 3:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isstrong(num);
        break;
    }
    case 4:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isarmstrong(num);
        break;
    }
    case 5:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        ispalindrome(num);
        break;
    }
    case 6:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isDuck(num);
        break;
    }
    case 7:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isautomorphic(num);
        break;
    }
    case 8:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isharshad(num);
        break;
    }
    case 9:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        issqroot(num);
        break;
    }
    case 10:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isdeficient(num);
        break;
    }
    case 11:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isdisarium(num);
        break;
    }
    case 12:
    {
        int num1 = 0, num2 = 0;
        printf("Enter Numbers to check   : \n");
        printf("Enter Number1 : ");
        scanf("%d", &num1);
        printf("Enter Number2 : ");
        scanf("%d", &num2);
        isamicable(num1, num2);
        break;
    }
    case 13:
    {
        int num = 0;
        printf("Enter Number : ");
        scanf("%d", &num);
        isperfect(num);
        break;
    }

    default:
        printf("Chongya enter valid input");
        break;
    }
}