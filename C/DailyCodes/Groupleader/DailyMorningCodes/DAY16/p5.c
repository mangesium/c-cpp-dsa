#include<stdio.h>
float squareroot(float num){
    float temp=0,sqrt=0;
    sqrt=num/2;
    while(sqrt!=temp){
        temp=sqrt;
        sqrt=(num/sqrt+sqrt)/2;
    }
    return sqrt;
}
void main(){
    int x1,x2,x3,y1,y2,y3;
    printf("Enter co-ordinates at x1 : ");
    scanf("%d",&x1);
    printf("Enter co-ordinates at y1 : ");
    scanf("%d",&y1);
    printf("Enter co-ordinates at x2 : ");
    scanf("%d",&x2);
    printf("Enter co-ordinates at y2 : ");
    scanf("%d",&y2);
    printf("Enter co-ordinates at x3 : ");
    scanf("%d",&x3);
    printf("Enter co-ordinates at y3 : ");
    scanf("%d",&y3);
    float val=(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
    float s1=squareroot(val);
    val=(x3-x1)*(x3-x1)+(y3-y1)*(y3-y1);
    float s2=squareroot(val);
    val=(x3-x2)*(x3-x2)+(y3-y2)*(y3-y2);
    float s3=squareroot(val);

    float S=(s1+s2+s3)/2;
    val=S*(S-s1)*(S-s2)*(S-s3);
    float Area=squareroot(val);
    printf("%f\n",Area);
    printf("%f is radius of in circle of triangle\n",Area/S);
}