#include<stdio.h>
int isprime(int n){
    int count=0;
    for(int i=1;i<=n/2;i++){
        if(n%i==0)
            count++;
        if(count>1)
            break;
    }
    if(count==1)
        return 1;
    return 0;
}
void main(){
    int num=0;
    printf("Enter Number : ");
    scanf("%d",&num);
    int sum=0;
    for(int i=1;i<=num/2;i++){
        if(num%i==0&&isprime(i))
            printf("%d ",i);
    }
    printf("\n");
}
