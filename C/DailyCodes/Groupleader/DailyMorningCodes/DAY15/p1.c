//WAP to swap first and last digits from number
#include<stdio.h>
int rev(int n){
    int temp=0;
    while(n!=0){
        temp=temp*10+n%10;
        n/=10;
    }
    return temp;
}
void main(){
    int num=0;
    printf("Enter Number : ");
    scanf("%d",&num);
    int count=0;
    int n=num;
    int temp1=n%10,sum=0;
    n/=10;
    while(n!=0){
        count++;
        sum=sum*10+n%10;
        n/=10;
    }
    int k=sum%10;
    sum/=10;
    int math=rev(sum);
    while(count-1){
        temp1*=10;
        count--;
    }
    temp1+=math;
    temp1=temp1*10+k;
    printf("swap of two digits of num %d ---> %d\n",num,temp1);


}