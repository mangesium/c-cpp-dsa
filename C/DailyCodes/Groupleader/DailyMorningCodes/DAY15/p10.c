#include<stdio.h>
void main(){
    int row=0;
    printf("Enter number : ");
    scanf("%d",&row);
    int i=1,j=1,k=0,flag=0,l=0;
    if(row%2==0)
        l=1;
    else
        l=2;
    for(i=1;i<=row;i++){
        if(i==1||i==row)
            flag=1;
        else
            flag=0;
        if(flag==0){
            if(i<=row/2+1)
                k++;
            else
                k--;
        }
        for(j=1;j<=row;j++){
            if(flag==1)
                printf("* ");
            else{
                if(j>=row/2+l-k&&j<=row/2+k)
                    printf("  ");
                else 
                    printf("* ");
            }
        }
        printf("\n");
    }
}