#include<stdio.h>
void main(){
    int num=0;
    printf("Enter Number : ");
    scanf("%d",&num);
    int fact=1,rem=0,temp=num;
    while(temp!=0){
        rem = temp%10;
        fact=1;
        if(rem%2==0){
            while(rem!=0){
                fact*=rem;
                rem--;
            }
            printf("%d is the factorial of %d\n",fact,temp%10);
        }
        temp/=10;
    }
}