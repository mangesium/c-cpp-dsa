#include<stdio.h>
void main(){
    int num=0;
    printf("Enter number : ");
    scanf("%d",&num);
    int flag=0;
    if(num<0){
        num=-num;
        flag=1;
    }
    int hash[10]={};
    while(num!=0){
        int rem=num%10;
        hash[rem]++;
        num/=10;
    }
    for(int i=0;i<10;i++){
        if(hash[i]>0)
            printf("freq. of %d is %d\n",i,hash[i]);
    }
    
}