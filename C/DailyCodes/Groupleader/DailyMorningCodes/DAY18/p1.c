/*
1
1 2
2 3 4
4 5 6 7
7 8 9 10 11
*/
#include <stdio.h>
void main()
{
    int row = 0;
    printf("Enter No. of rows :");
    scanf("%d", &row);
    int i = 1, j = 1, k = 1;

    for (i = 1; i <= row;)
    {
        if (i == j)
        {
            printf("%d\n", k);
            j = 1;
            i++;
        }
        else
        {
            printf("%d ", k);
            k++;
            j++;
        }
    }
}