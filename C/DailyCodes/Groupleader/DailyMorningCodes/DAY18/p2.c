/*
        1
      1 b
    1 b 2
  1 b 2 d
1 b 2 d 3
*/
#include <stdio.h>
void main()
{
    int row = 0;
    printf("Enter No. of rows : ");
    scanf("%d", &row);
    int i = 1, j = row, k = 1, flag = 0, ch = 97;
    for (i = 1; i <= row;)
    {
        if (j == 1)
        {
            if (flag == 0)
            {
                printf("%d\n", k);
                flag = 1;
            }
            else
            {
                printf("%c\n", ch);
                flag = 0;
            }
            i++;
            j = row;
            k = 1;
            ch = 97;
            flag = 0;
        }
        else
        {
            if (j <= i)
            {
                if (flag == 0)
                {
                    printf("%d ", k);
                    flag = 1;
                    k++;
                }
                else
                {
                    printf("%c ", ch);
                    flag = 0;
                }
                ch++;
            }
            else
            {
                printf("  ");
            }
            j--;
        }
    }
}