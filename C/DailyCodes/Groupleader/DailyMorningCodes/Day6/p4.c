/*
1 
1 2 
1 2 3 
1 2 
1 
*/

#include<stdio.h>
void main(){
    int row=-1;
    while(row<=0){
        printf("Enter No of rows : ");
        scanf("%d",&row);
    }
    int col=row/2+1;
    int i=0,j=0,k=0;
    for(i=1;i<=row;i++){
        if(i<=col)
            k++;
        else
            k--;
        for(j=1;j<=col;j++){
            if(j<=k)
                printf("%d ",j);
        }
        printf("\n");
    }
}