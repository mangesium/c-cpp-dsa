//Program to print palindrome in range
#include<stdio.h>
void main(){
    int start=0,end=0,flag=0;
    printf("Enter Start Number : ");
    scanf("%d",&start);
    printf("Enter End Number : ");
    scanf("%d",&end);

    for(int i=start;i<=end;i++){
        int temp=i,sum=0;
        while(temp!=0){
            sum=sum*10+temp%10;
            temp/=10;
        }
        if(sum==i)
            printf("%d ",i);
    }
    printf("\n");
}

