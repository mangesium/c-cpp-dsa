/*
A B C D 
  B C D
    C D
      D
*/
#include<stdio.h>
void main(){
    int row=-1;
    while(row<=0){
        printf("Enter No of Rows : ");
        scanf("%d",&row);
    }
    int i=0,j=1,ch=64;
    for(i=0;i<row;){
        if(j==row){
            printf("%c\n",ch+j);
            i++;
            j=1;
        }else{
            if(j>i)
                printf("%c ",ch+j);
            else
                printf("  ");
            j++;
            continue;
        }
    }
}