//Program to print reverse of given input
#include<stdio.h>
void main(){
    int num=0,flag=0;
    printf("Enter Number : ");
    scanf("%d",&num);
    int sum=0,temp=num;
    while(num!=0){
        sum=sum*10+num%10;
        num/=10;
        flag=1;
    }

    if(temp!=0)
        printf("reverse of %d is %d\n",temp,sum);
    else
        printf("0\n");
}