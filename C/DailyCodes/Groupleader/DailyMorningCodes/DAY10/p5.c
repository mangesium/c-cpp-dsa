#include<stdio.h>
#include<limits.h>
void main(){
    int row=0;
    printf("Enter No. of rows : ");
    scanf("%d",&row);
    int i=0,count=0,rem=0,sum=0,j=1;
    for(i=0;i<INT_MAX;i++){
        int num=i;
        sum=0;
        while(num!=0){
            rem=num%10;
            sum=sum*10+rem;
            num/=10;
        }
        if(sum==i){
            if(j<row){
                printf("%2d ",i);
                j++;
            }else{
                printf("%2d\n",i);
                j=1;
            }
            count++;
        }
        if(count==row*row)
            break;
    }

}