#include<stdio.h>
void main(){
    int row=0;
    printf("Enter Number of Rows : ");
    scanf("%d",&row);
    int num=2*(row+1),ch=64+2*(row+1);
    for(int i=1;i<=row;i++){
        for(int j=1;j<=row;j++){
            if(j<=i){
                if(i%2==0)
                    printf("%c ",ch);
                else
                    printf("%d ",num);
                num--;
                ch--;
            }
        }
        printf("\n");
    }
}