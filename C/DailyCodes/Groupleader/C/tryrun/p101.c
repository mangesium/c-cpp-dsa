#include <stdio.h>
int search(char *carr, int len, char ch)
{
    static int i = -1;
    i++;
    if (i == len)
        return -1;
    else if (ch == carr[i])
        return i;
    else
        return search(carr, len, ch);
}
void main()
{
    char carr[] = {'a', 'b', 'c', 'd', 'e', 'f'};
    int len = sizeof(carr) / sizeof(char);
    printf("Enter character to search : ");
    char ch;
    scanf("%c", &ch);
    getchar();
    int retval = search(carr, len, ch);
    if (retval == -1)
    {
        printf("Entered Character Not found\n");
    }
    else
    {
        printf("%c is found at index %d", ch, retval);
    }
}