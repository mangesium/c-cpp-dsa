#include<stdio.h>

void main(){
    int arr1[3]={1,2,3};
    int arr2[3]={4,5,6};
    
    int** iparr[]={&arr1,&arr2};
    printf("%p\n",iparr[0]);
    printf("%p\n",iparr[1]);

    printf("%d\n",*(iparr[0]+1));
    printf("%d\n",*(iparr[1]+0));
}