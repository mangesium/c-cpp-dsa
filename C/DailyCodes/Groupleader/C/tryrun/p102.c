#include <stdio.h>
#include <string.h>
int ispalindrome(char *str, int len)
{
    static int i = -1;
    i++;
    if (str[i] != str[len - 1 - i])
    {
        return 0;
    }
    else if (i == len / 2)
        return 1;
    else
        return ispalindrome(str, len);
}
void main()
{
    char str[500];
    printf("Enter String : ");
    char ch;
    int i = 0;
    while ((ch = getchar()) != '\n')
    {
        str[i] = ch;
        i++;
    }
    str[i] = '\0';
    int len = strlen(str);
    if (ispalindrome(str, len))
    {
        printf(" %s is a Palindrome String\n", str);
    }
    else
    {
        printf("%s is not a Palindrome String\n", str);
    }
}