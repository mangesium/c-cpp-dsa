import java.util.*;
public class p74 {
    public static void main(String[] args){
        int[] arr=new int[]{2,1,1,1};
        
        Arrays.sort(arr);
        int count=0;
        for(int i=arr.length-1;i>=0;i--){
            for(int j=i+1;j<arr.length;j++){
                if(2*arr[i]<=arr[j]&&arr[j]>0){
                    count++;
                    arr[j]=-1;
                    break;
                }
            }            
        }
        System.out.println(arr.length-count);
    }
    
}
