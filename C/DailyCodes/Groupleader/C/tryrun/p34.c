#include<stdio.h>
#include<string.h>
#include<stdlib.h>
char * convert(char * s, int numRows){
    char* new_s = (char*)calloc( (strlen(s) + 1), sizeof(char));
	
    if (numRows == 1) {
	    strcpy(new_s, s);
		return s;
	}

	int last_new = 0;
	for (int i = 0; i < numRows; i++) {
		int j = i;
		int direction =( i != numRows - 1) ? 0 : 1;
		while (j < strlen(s)) {
			new_s[last_new] = s[j];
            printf("last %d = %c of %d of j\n",last_new,new_s[last_new],j);
			last_new++;
			if (direction == 0) {
				j += (numRows - i - 1) * 2;
			}
			else {
				j += i * 2;
			}
			if (i != 0 && i != numRows - 1) {
				direction = 1 - direction;
			}
		}
	}
	return new_s;
}
void main(){

    char* carr = convert("PAYPALISHIRING",3);
    for(int i=0;i<=13;i++){
        printf("%c ",carr[i]);
    }
    printf("\n");

}