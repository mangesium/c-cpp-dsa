#include<stdio.h>
void main(){
    int row=0;
    scanf("%d",&row);
    for(int i=0;i<row;i++){
        char ch1='a';
        char ch2='A';
        for(int j=row-1;j>=0;j--){
            if(j<=i){
                if(j%2==1)
                    printf("* ",ch1);
                else
                    printf("* ",ch2);
            }
            else
                printf("  ");
            ch1++;
            ch2++;
        }
        printf("\n");
    }
}