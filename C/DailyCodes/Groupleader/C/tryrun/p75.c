#include <stdio.h>
void main()
{
    int num = 123123;
    int arr[10];
    for (int i = 0; i < 10; i++)
        arr[i] = 0;
    while (num != 0)
    {
        int rem = num % 10;
        arr[rem]++;
        num /= 10;
    }
    for (int i = 0; i < 10; i++)
    {
        if (arr[i] != 0)
            printf("%d =%d \n", i, arr[i]);
    }
}

