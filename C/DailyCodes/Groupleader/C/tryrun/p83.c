#include<stdio.h>
#include<stdlib.h>

typedef struct Employee {

        char name[20] ;
        int id ;
        struct Employee *next ;
} emp ;

emp *head = NULL ;

void addNode() {

        emp* newNode = (emp*)malloc(sizeof(emp)) ;

        printf("Enter name : ") ;

        char ch ;
        int i = 0 ;
        while((ch = getchar()) != '\n'){
                (*newNode).name[i] = ch ;  
                i++;                            
        }

        printf("Enter id : ") ;
        scanf("%d",&newNode->id) ;
        getchar();

        newNode->next = NULL ;

        // connecting new node 
        if(head == NULL) {
                head = newNode ;
        }else {
                // temp pointer to move pointer to 2nd last node
                emp *temp = head ;

                while(temp->next != NULL) {
                        temp = temp->next ;
                }

                // connecting new node
                temp->next = newNode ;
        }
}
void printLL(){

        // temp pointer to traverse linked list
        emp *temp = head ;

        while(temp != NULL) {
                printf("|%s->",temp->name) ;
                printf("%d| ",temp->id) ;
                temp = temp->next ;
        }
}

void main() {

        addNode() ;
        addNode() ;
        addNode() ;
        printLL() ;
}