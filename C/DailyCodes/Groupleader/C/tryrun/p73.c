#include<stdio.h>
int fun(int* arr,int len){
    int count=0;
    for(int i=0;i<len;i++){
        for(int j=0;j<len;j++){
            if(i==j)
                continue;
            if(2*arr[i]<=arr[j]&&arr[i]>0){
                arr[j]=-1;
                count++;
                break;
            }
        }
    }
    return len-count;
}
void main(){
    int arr[]={1,1,1,2};
    int len=sizeof(arr)/sizeof(int);
    printf("%d\n",fun(arr,len));
}