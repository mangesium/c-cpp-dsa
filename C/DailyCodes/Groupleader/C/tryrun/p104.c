// Merge SORT

#include <stdio.h>

void merge(int *arr, int start, int mid, int end)
{

    int size1 = mid - start + 1;
    int size2 = end - mid;

    int arr1[size1];
    int arr2[size2];

    for (int i = 0; i < size1; i++)
        arr1[i] = arr[start + i];

    for (int j = 0; j < size2; j++)
        arr2[j] = arr[mid + 1 + j];

    int itr1 = 0;
    int itr2 = 0;
    int itr3 = start;

    while (itr1 < size1 && itr2 < size2)
    {

        if (arr1[itr1] <= arr2[itr2])
        {

            arr[itr3] = arr1[itr1];
            itr1++;
        }
        else
        {
            arr[itr3] = arr2[itr2];
            itr2++;
        }
        itr3++;
    }

    while (itr1 < size1)
    {

        arr[itr3] = arr1[itr1];
        itr1++;
        itr3++;
    }

    while (itr2 < size2)
    {

        arr[itr3] = arr1[itr2];
        itr2++;
        itr3++;
    }
}

void mergesort(int arr[], int start, int end)
{

    if (start < end)
    {

        int mid = (start + end) / 2;
        mergesort(arr, start, mid);
        mergesort(arr, mid + 1, end);
        merge(arr, start, mid, end);
    }
}

void main()
{

    int size;
    printf("Enter size of array:\n");
    scanf("%d", &size);

    int arr[size];

    printf("Enter elements into array:\n");

    for (int i = 0; i < size; i++)
        scanf("%d", arr + i);

    printf("Array before sorted:\n");

    for (int i = 0; i < size; i++)
        printf("| %d |", *(arr + i));

    printf("\n");

    mergesort(arr, 0, size - 1);

    printf("Array After sorted:\n");

    for (int i = 0; i < size; i++)
        printf("| %d |", *(arr + i));

    printf("\n");
}
