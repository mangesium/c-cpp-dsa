#include <stdio.h>

int rotatedarray(int *arr, int size, int ele)
{

    int start = 0, end = size - 1, index = -1, mid = 0;

    while (start <= end)
    {
        int mid = start + (end - start) / 2;

        if (arr[mid] == ele)
        {
            return mid;
        }

        if (arr[end] > arr[mid])
        {
            if (arr[mid] < ele && ele <= arr[end])
            {
                start = mid + 1;
            }
            else
            {
                end = mid - 1;
            }
        }
        else
        {

            if (arr[start] <= ele && ele < arr[mid])
            {
                end = mid - 1;
            }
            else
            {
                start = mid + 1;
            }
        }
    }
    return -1;
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    int ele;
    printf("Enter number to search : ");
    scanf("%d", &ele);
    int index = rotatedarray(arr, size, ele);
    if (index == -1)
    {
        printf("%d element not found in array\n", ele);
    }
    else
    {
        printf("%d element found at index %d\n", ele, index);
    }
}