#include<stdio.h>
#include<limits.h>

int maxprod(int* arr,int len){
    if(len==0){
        printf("Empty array\n");
        return 0;
    }
    int max1=INT_MIN,max2=INT_MIN;
    for(int i=0;i<len;i++){
        if(max1<arr[i]){
            max2=max1;
            max1=arr[i];
        }
        else if(max2<arr[i]&&max2<=max1){
            max2=arr[i];
        }
    }
    printf("%d %d\n",max1,max2);
    return max1*max2;
}

void main(){
    int len;
    printf("Enter size of array : ");
    scanf("%d",&len);
    int arr[len];
    printf("Enter elements in array : ");
    for(int i=0;i<len;i++){
        scanf("%d",&arr[i]);
    }
    int max=maxprod(arr,len);
    printf("%d is the maximum product of two numbers in following array\n",max);
}