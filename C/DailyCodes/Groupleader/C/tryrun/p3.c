#include<stdio.h>
void part(int*arr,int start,int mid,int end){

    int size=end-start+1;
    int A[size];
    int i=start,j=mid+1,k=0;
    while(i<=mid&&j<=end){
        if(arr[i]<=arr[j]){
            A[k]=arr[i];
            i++;
        }else{
            A[k]=arr[j];
            j++;
        }
        k++;
    }
    while(i<=mid){
        A[k]=arr[i];
        i++;
        k++;
    }
    while(j<=end){
        A[k]=arr[j];
        j++;
        k++;
    }
    for(int i=0,j=start;i<size;i++,j++){
        arr[j]=A[i];
    }
}

void merge(int* arr,int start,int end){
    if(start<end){
        int mid=(start+end)/2;
        merge(arr,start,mid);
        merge(arr,mid+1,end);
        part(arr,start,mid,end);
    }
}

void main(){
    int arr[] ={5,4,3,2,1};
    int len=sizeof(arr)/sizeof(int);
    int start=0,end=len-1;
   
    merge(arr,start,end);
   
    for(int i=0;i<len;i++){
        printf("%d ",arr[i]);
    }
    printf("\n");
}