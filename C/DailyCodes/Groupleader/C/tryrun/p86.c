#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void insert(int *arr, int len, int pos, int num)
{
    if (pos == len - 1)
    {
        arr[pos] = num;
        return;
    }
    if (pos == 0)
    {
        arr[pos] = num;
        return;
    }
     for (int i = len - 1; i >= pos; i--)
        arr[i] = arr[i - 1];

    arr[pos] = num;
}
void main()
{
    int pos = 2;
    int arr[] = {1, 2, 3, 4, 5, 6};
    int len = sizeof(arr) / sizeof(int);
    for (int i = 0; i < len; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
    int num = -1;
    insert(arr, len, pos - 1, num);
    for (int i = 0; i < len; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n");
}
