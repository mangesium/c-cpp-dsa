#include <stdio.h>

int Floor(int *arr, int size, int ele)
{
    int start = 0, end = size - 1, mid = 0, store = -1;
    while (start <= end)
    {
        mid = start + (end - start) / 2;
        if (arr[mid] == ele)
        {
            return mid;
        }
        if (arr[mid] > ele)
        {
            store = mid;
            end = mid - 1;
        }
        else
        {
            start = mid + 1;
        }
    }
    return store;
}
void main()
{
    int size;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        printf("Enter element at %d index : ", i);
        scanf("%d", &arr[i]);
    }
    int ele;
    printf("Enter number to search : ");
    scanf("%d", &ele);
    int index = Floor(arr, size, ele);
    if (index == -1)
    {
        printf("Ceiling value for %d element not found in array\n", ele);
    }
    else
    {
        printf("floor element for %d = %d\n", ele, arr[index]);
    }
}