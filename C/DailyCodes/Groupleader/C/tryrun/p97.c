#include <stdio.h>
int sumnum(int x)
{
    if (x == 1 )
        return 1;
    return sumnum(x - 1) + x;
}
void main()
{
    printf("Sum = %d\n", sumnum(4));
}