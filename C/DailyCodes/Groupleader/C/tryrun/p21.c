#include<stdio.h>
#include<stdbool.h>
#include<string.h>
bool isPalindrome(char * s){
    int len=strlen(s);
    char ch[len];
    int j=0;
    for(int i=0;i<len;i++){
        if(s[i]>='a'&&s[i]<='z'||s[i]>='A'&&s[i]<='Z'){
            if(s[i]>='A'&&s[i]<='Z')
                ch[j]=s[i]+32;
            else                
                ch[j]=s[i];
            j++;
        }
    }
    for(int i=0;i<j;i++){
        printf("%c",ch[i]);
    }
    printf("\n");
    for(int i=0;i<j/2;i++){
        if(ch[i]==ch[j-1-i]){
            printf("%c==%c\n",ch[i],ch[j-1-i]);
            continue;
        }
        else{
            printf("%d point of breaking %c==%c\n",i,ch[i],ch[j-1-i]);
            return false;
        }
    }
    return true;

}
void main(){
    printf("%d\n",isPalindrome("0P"));
}