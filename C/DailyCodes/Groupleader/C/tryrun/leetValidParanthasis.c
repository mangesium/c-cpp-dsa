#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>
bool isValid(char * s){
    
    int len = strlen(s), ptr = 0;
    char *stack = malloc(len+1); //used to keep track of the last open bracket, brace, or parenthesis
    int a = 0, b = 0, c = 0; //(flags)
    
    while(*s != 0)
    {
        if(*s == '(') {
            a++;
            stack[++ptr] = 1;
        }        
        if(*s == '[') {
            b++;
            stack[++ptr] = 2;
        }
        if(*s == '{') {
            c++;
            stack[++ptr] = 3;
        }
        if(*s == ')')
        {
            if(stack[ptr] == 1) {
                a--;
                ptr--;

            }
            else return false;
        }
        if(*s == ']')
        {
            if(stack[ptr] == 2) {
                b--;
                ptr--;
            }
            else return false;

        }
        if(*s == '}')
        {
            if(stack[ptr] == 3) {
                c--;
                ptr--;
            }
            else return false;
        }
        s++;
    }    
    if(a > 0 || b > 0 || c > 0) //if the bracket isn't closed off
        return false;
    return true; //valid parentheses
}
void main(){
    char* c="(([]){})";
    //printf("%d\n",strlen(c));
    printf("%d\n",isValid("(([]){})()()(())"));
}