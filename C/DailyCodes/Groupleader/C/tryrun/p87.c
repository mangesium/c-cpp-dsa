#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct demo
{
    struct demo *prev;
    int data;
    struct demo *next;
} d;

d *creatnode()
{
    d *newnode = malloc(sizeof(d));
    newnode->prev = NULL;
    printf("Enter Data : ");
    scanf("%d", &newnode->data);
    newnode->next = NULL;
    return newnode;
}
int count(d *head)
{
    int count = 0;
    while (head != NULL)
    {
        count++;
        head = head->next;
    }
    return count;
}
void addnode(d **temp)
{
    d *node = creatnode();
    if ((*temp) == NULL)
    {
        *temp = node;
    }
    else
    {
        d *temp1 = *temp;
        while ((temp1)->next != NULL)
        {
            temp1 = (temp1)->next;
        }
        (temp1)->next = node;
        node->prev = temp1;
    }
}
void printll(d *temp)
{
    if (temp == NULL)
        printf("Empty LinkedList");

    while (temp != NULL)
    {
        if (temp->next != NULL)
            printf("|%d|->", temp->data);
        else
            printf("|%d|", temp->data);
        temp = temp->next;
    }
    printf("\n");
}
void addatfirst(d **head)
{
    d *newnode = creatnode();
    if ((*head) == NULL)
    {
        *head = newnode;
    }
    else
    {
        newnode->next = (*head);
        (*head)->prev = newnode;
        (*head) = newnode;
    }
}
void addatpos(d **head, int pos)
{
    if (pos < 1 || pos > count(*head) + 1)
        printf("InValid Position\n");
    else if (pos == 1)
        addatfirst(head);
    else if (pos == count(*head) + 1)
        addnode(head);
    else
    {
        d *newnode = creatnode();
        d *temp = *head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        newnode->next = temp->next;
        newnode->prev = temp;
        temp->next = newnode;
        newnode->next->prev = newnode;
    }
}
void deleteatfirst(d **head)
{
    if (*head == NULL)
    {
        printf("LinkedList is empty\n");
    }
    else if (count(*head) == 1)
    {
        free(*head);
        *head = NULL;
    }
    else
    {
        *head = (*head)->next;
        free((*head)->prev);
        (*head)->prev = NULL;
    }
}
void deleteatlast(d **head)
{
    if (*head == NULL)
    {
        printf("LinkedList is empty\n");
    }
    else if (count(*head) == 1)
    {
        free(*head);
        *head = NULL;
    }
    else
    {
        d *temp = *head;
        while (temp->next->next != NULL)
        {
            temp = temp->next;
        }
        free(temp->next);
        temp->next = NULL;
    }
}
void deleteatpos(d **head, int pos)
{
    if (pos < 1 || pos > count(*head))
        printf("InValid Position\n");
    else if (pos == 1)
        deleteatfirst(head);
    else if (pos == count(*head))
        deleteatlast(head);
    else
    {
        d *temp = *head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        temp->next = temp->next->next;
        free(temp->next->prev);
    }
}
void main()
{
    d *head = NULL;
    char choice;
    do
    {
        printf("1.Addnode\n");
        printf("2.Addnodefirst\n");
        printf("3.Addnodeposition\n");
        printf("4.nodecount\n");
        printf("5.deletenodefirst\n");
        printf("6.deletenodelast\n");
        printf("7.deletenodeposition\n");
        printf("8.printll\n");
        int ch;
        printf("Enter Choice : ");
        scanf("%d", &ch);
        switch (ch)
        {
        case 1:
        {
            addnode(&head);
        }
        break;
        case 2:
        {
            addatfirst(&head);
        }
        break;
        case 3:
        {
            int pos;
            printf("Enter position to insert element : ");
            scanf("%d", &pos);
            addatpos(&head, pos);
        }
        break;
        case 4:
        {
            printf("Node count = %ld\n", count(head));
        }
        break;
        case 5:
        {
            deleteatfirst(&head);
        }
        case 6:
        {
            deleteatlast(&head);
        }
        case 7:
        {
            int pos;
            printf("Enter position to delete element : ");
            scanf("%d", &pos);
            deleteatpos(&head, pos);
        }
        case 8:
        {

            printll(head);
        }
        break;
        default:
            break;
        }

    } while (choice != 'n');
}