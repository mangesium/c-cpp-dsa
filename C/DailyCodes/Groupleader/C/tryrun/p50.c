#include<stdio.h>
#include<limits.h>
void main(){
    int max1=INT_MIN,max2=INT_MIN;
    int size=0;
    printf("Enter size of array : ");
    scanf("%d",&size);
    int arr[size];
    for(int i=0;i<size;i++){
        printf("Enter element in %dth index : ",i);
        scanf("%d",&arr[i]);
        if(max1<arr[i]){
            max2=max1;
            max1=arr[i];
        }
        if(max2<arr[i]&&arr[i]<max1)
            max2=arr[i];
    }
    if(max2==INT_MIN)
        printf("Second max not present\n");
    else{
        printf("%d is second maximum in following array\n",max2);
        printf("%d is maximum in following array\n",max1);
    }

}