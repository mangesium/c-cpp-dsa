#include<stdio.h>
void main(){
    int x=10;
    void *ptr=&x;   //void pointer
    //printf("%d\n",*ptr);      Error
    printf("%d\n",*((int*)ptr));    //Derefencing of void pointer

    int* iptr1; //Wild pointer
    printf("%d\n",*iptr1);

    int* iptr2=NULL;    //Null Pointer
    printf("%p\n",*iptr2);          //Try this code in wsl
}