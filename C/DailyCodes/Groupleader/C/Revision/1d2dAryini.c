#include<stdio.h>

int garr1[]={1,2,3,4,5};  // 1D-Global Array Initialization
int garr2[][3]={1,2,3,4,5,6}; // 2D-Global Array Initialization Here No. of columns are compulsary

void main(){
    int marr1[]={10,20,30,40,50};    // 1D-Array Initialization in main Method
    int marr2[][3]={1,2,3,4,5,6};   // 2D-Array Initialization in main Method
    for(int i=0;i<5;i++){
       // printf("%d ",garr1[i]);
        printf("%d ",marr1[i]);     //Accessing array elements in 1D-Array
    }
    printf("\n");
    for(int i=0;i<2;i++){
        for(int j=0;j<3;j++){
            printf("%d ",garr2[i][j]);  //Accessing array elements in 2D-Array
        }
        printf("\n");
    }

}