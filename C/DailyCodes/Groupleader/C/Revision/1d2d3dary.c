#include<stdio.h>

int garr1[5];  // Global 1D-Array Declaration
int garr2[3][3]; // Global 2D-Array Declaration
int garr3[2][3][3]; // Global 3D-Array Declaration Here Rows And columns are complusary


void main(){
    int marr1[5];    // 1D-Array Declaration in main Method
    int marr2[3][3]; // 2D-Array Declaration in main Method
    int marr3[2][3][3]={1,2,3,4,5,6,7,8,9,10,11,12}; // 3D-Array Declaration in main Method
    printf("%d\n",*(marr3[0][0]));    
}