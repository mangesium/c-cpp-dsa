#include <stdio.h>
#include <stdlib.h>
typedef struct whatsappgroup
{
	char gName[20];
	int members;
	struct whatsappgroup *next;
}wg;
wg *head = NULL;

wg *createnode()
{

	wg *newnode = (wg *)malloc(sizeof(wg));
	getchar();
	printf("Enter Group name : ");
	char ch;
	int i = 0;
	while ((ch = getchar()) != '\n')
	{
		(*newnode).gName[i] = ch;
		i++;
	}
	printf("Enter members: ");
	scanf("%d", &newnode->members);

	newnode->next = NULL;
	return newnode;
}
void addnode()
{

	wg *newnode = createnode();

	if (head == NULL)
	{
		head = newnode;
	}
	else
	{
		wg *temp = head;

		while (temp->next != NULL)
		{

			temp = temp->next;
		}
		temp->next = newnode;
	}
}
int countnode()
{

	int count = 0;

	wg *temp = head;

	while (temp != NULL)
	{
		temp = temp->next;
		count++;
	}

	return count;
}
void printnode()
{

	if (head == NULL)
	{
		printf("Zero whatsapp group\n");
	}
	else
	{

		wg *temp = head;

		while (temp != NULL)
		{
			if (temp->next == NULL)
			{
				printf("|%s", temp->gName);
				printf("= %d |", temp->members);
			}
			else
			{
				printf("|%s", temp->gName);
				printf("= %d |->", temp->members);
			}
			temp = temp->next;
		}
		printf("\n");
	}
}
void addfirst(){

	wg *newnode = createnode();

	if(head == NULL){
		head =newnode;
	}else{
		newnode->next = head;
		head =newnode;
	}
}
void addlast(){

	addnode();
}
void addatpos(int pos){

	
	int count = countnode();

	if(pos <= 0 || pos > count+1){
		printf("Invalid Position\n");
	}else if(pos == 1){
		addfirst();
	}else if(pos == count +1){
		addlast();
	}else{
		wg *newnode = createnode();

		wg *temp = head;

		while(pos-2){
			temp=temp->next;
		}
		newnode->next=temp->next;
		temp->next =newnode;
	}
}
void deletefirst(){

	if(head == NULL){
		printf("Linkedlist is empty\n");
	}else if(head->next == NULL){
		free(head);
		printf("Node is sucessfully deleted\n");
	}else{
		wg *temp = head;
		head=temp->next;
		free(temp);
		printf("Node is sucessfully deleted\n");
	}
}
void deletelast(){

	if(head == NULL){
		printf("Linkedlist is empty\n");
	}else if(head->next == NULL){
		free(head);
		printf("Node is sucessfully deleted\n");
	}else{
		wg *temp = head;
		while(temp->next->next != NULL){
			temp=temp->next;
		}
		free(temp->next);
		temp->next = NULL;
		printf("Node is sucessfully deleted\n");
	}
}
void deleteatpos(int pos){

	int count = countnode();

	if(pos <= 0 || pos > count){
		printf("INVALID INPUT...\n");
	}else if(head == NULL){
		printf("Linkedlist is empty\n");
	}else if(pos  == 1){
		deletefirst();
	}else if(pos == count){
		deletelast();
	}else{
		wg *temp1 = head;

		while(pos-2){
			temp1=temp1->next;
		}
		wg *temp2 = temp1;
		temp1->next=temp1->next->next;
		free(temp2);
	}
}

void main()
{

	char choice;

	do
	{

		printf("--------------------------------------------\n");
		printf("1.Addnode\n");
		printf("2.Printnode\n");
		printf("3.Count of nodes\n");
		printf("4.Add node at first position\n");
		printf("5.Add node at given position\n");
		printf("6.Delete the first node\n");
		printf("7.Delete the last node\n");
		printf("8.Delete node at position\n") ;
		int ch;
		printf("Enter choice:\n");
		scanf("%d", &ch);
		switch (ch)
		{

		case 1:
			addnode();
			break;

		case 2:
			printnode();
			break;

		case 3:
			countnode();
			break;

		case 4:
			addfirst();
			break;

		case 5:
		{
			int x;
			printf("Enter at which position you want to enter the node:\n");
			scanf("%d", &x);
			addatpos(x);
		}
		break;

		case 6:
			deletefirst();
			break;

		case 7:
			deletelast();
			break;

		case 8:
		{
			int x;
			printf("Enter at which position you want to delete the node:\n");
			scanf("%d", &x);
			deleteatpos(x);

		}
			
			break;

		default:
			printf("Enter valid input\n");
		}

		getchar();
		printf("Do you want to continue: ");
		scanf("%c", &choice);
	} while (choice != 'n');
}