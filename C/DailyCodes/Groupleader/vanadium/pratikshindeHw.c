//WAP take user input for creating a node
//take one number from user
//cheak wether the number is present in linked list or not
//if it is present 1 time print the position and metion occured only one time
//if it is present multiple time print second last position of that data in the linked list

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;



struct Node* createnode(){

	struct Node *newnode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;

	return newnode;
}

void printnode(){

	if(head == NULL){
		printf("THERE IS NO NODE PRINT... PLZ CREATE NODE FIRST\n");
	}else{
		
		struct Node*temp = head;

		while(temp != NULL){
			printf("| %d |",temp->data);
			temp=temp->next;
		}
	 	printf("\n");
	}
}
void addnode(){

	struct Node* newnode = createnode();

	if(head == NULL){
		head = newnode;
	}else{
		struct Node *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next= newnode;
	}
}

void findnum(int num){

	struct Node *temp1 = head;
	int count=1;
	int x1=0,x2=0;
	
	while(temp1 != NULL){
		
		if(temp1->data == num){
			x2=x1;
			x1=count;
		}
		count++;
		temp1=temp1->next;
	}

	if(x2 == 0 && x1 != 0){
		printf("%d is occurred only once in the linked list at position %d \n",num,x1);
	}else if(x1 != 0 && x2 != 0){
		printf("The second last position of %d is :%d\n",num,x2);
	}else{
		printf("%d is not occured in the linked list..\n",num);
	}
}



void main(){

	int countnode;
	printf("Enter no. of nodes you want to create:\n");
	scanf("%d",&countnode);
	
	for(int i=1;i<=countnode;i++)
		addnode();

	
	
	char ch;
	
	do{	
		int num;
		printf("Enter a number do you  want to find:\n");
		scanf("%d",&num);
	
		findnum(num);
	
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&ch);

	}while(ch == 'y' || ch == 'Y');

	printnode();

}

