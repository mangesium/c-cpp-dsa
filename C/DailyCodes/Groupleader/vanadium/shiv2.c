#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{
	int data;
	struct Demo *next;
}Demo;

Demo *head = NULL;

Demo* createNode(){

	Demo* newNode = (Demo*)malloc(sizeof(Demo));
	
	printf("Enter Data:");
	scanf("%d",&newNode->data);
	
	(*newNode).next=NULL;
	
	return newNode;	
}
void addNode(){
	Demo* newNode=createNode();
	
	if(head==NULL)
		head=newNode;
	else{
		Demo *temp = head;
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newNode;
	}
}
void addFirst(){
	Demo* newNode = createNode();

	if(head==NULL){
		head=newNode;
	}else{
		newNode->next=head;
		head=newNode;
	}
}
int countNode(){
	int count = 0;
	Demo *temp = head;
	
	while(temp!=NULL){
		
		count++;
		temp=temp->next;
	
	}
		return count;
}
int addAtPos(){
	int pos;
	
	printf("Enter Position to add the node::");
	scanf("%d",&pos);
	int cnt = countNode(); 
	if(pos<=0 || pos>=cnt+2){

		printf("Invalid Node Position\n");
		return -1;
	}else{
		if(pos == cnt+1){
			addNode();
		}else if(pos == 1){
			addFirst();
		}else{
		
			Demo* newNode = createNode();
			Demo* temp = head;
		
			while(pos-2){
				temp=temp->next;
				pos--;
			}
			newNode->next=temp->next;
			temp->next=newNode;
		}
		return 0;
	}

}
void deleteFirst(){
	
	Demo *temp = head;
	if(head!=NULL){
		head=temp->next;
		free(temp);
	}else{
		printf("Invalid operation\n");
	}


}
void deleteLast(){
	Demo *temp = head;
	Demo *temp1 = NULL;
	
	if(head == NULL){
		
		printf("Nothing to delete\n");
	}else if(temp->next == NULL){

		temp1 = temp->next;
		head = NULL;
	}else{
		while(temp->next != NULL){

			temp1 = temp;
			
			temp=temp->next;
		}

		temp1->next=NULL;
		free(temp);
	}
}
int deleteAtPos(){
	
	int cnt = countNode();
	Demo *temp = head;
	Demo *temp1 = NULL;
	int pos;
	printf("Enter position :\n");
	scanf("%d",&pos);
	if(pos <= 0 || pos >= cnt+2){
		
		printf("Invalid Node Position\n");
		return -1;
	}else{
		if(pos == 1){
			deleteFirst();
		}else if(pos == cnt){
			deleteLast();
		}else if(pos > 1){
			while(pos-2){
				temp = temp->next;
		}
			temp1 = temp->next;
			temp->next = temp1->next;
			free(temp1);
		}
		return 0;
	}

				

}
void printll(){
	
	Demo *temp = head;

	while(temp!=NULL){
		printf("|%d|",temp->data);
		temp=temp->next;
	}

}

void main(){

	char Choice;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.printll\n");
		printf("5.deleteFirst\n");
		printf("6.deleteLast\n");
		printf("7.deleteAtPos\n");
		
		int ch;
		printf("Enter operation Number:");
		scanf("%d",&ch);

		switch(ch){
		
			case 1 :
				addNode();
				break;
			case 2 :
				addFirst();
				break;
			case 3 :
				addAtPos();
				break;
			case 4 :
				printll();
				break;
			case 5 :
				deleteFirst();
				break;
			case 6 :
				deleteLast();
				break;
			case 7 :
				deleteAtPos();
				break;
			default:
				printf("Invalid Input");	
		
		}
		getchar();
		printf("\nDo you want to continue:");
		scanf("%c",&Choice);
	}while(Choice=='Y'||Choice=='y');

}
