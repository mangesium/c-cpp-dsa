
/*
 	========================================================
	Linked list structure :
	struct node {
	char str[20]; // strs element
	struct node *next, // Address of next node
	};
	========================================================

	Program #8.

	Write a program that accepts a singly linear linked list from the user.
	Take a number from the user and only keep the elements that are equal in
	length to that number and delete other elements. And print the Linked list
	Length of Shashi = 6
	Input: linked list: |Shashi |-> | Ashish|-> |Kanha |-> | Rahul |-> | Badhe |
	Input: 6
	Output : linked list: |Shashi |-> | Ashish|
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Node{
	char str[20] ;
	struct Node *next ;
} Node ;

// head pointer
Node *head = NULL ;

Node* createNode(){
	Node *newNode = (Node*)malloc(sizeof(Node)) ;

	printf("Enter name :\n") ;
	
	int i = 0 ;
	char ch ;
	getchar();
	while((ch = getchar()) != '\n') {
		(*newNode).str[i++] = ch ;
	}

	newNode->next = NULL ;
	
	return newNode ;
}

void addNode() {

	Node *newNode = createNode() ;

	if(head == NULL) {
		head = newNode ;
	
	} else {
		Node *temp = head ;

		while(temp->next != NULL) {
			temp = temp->next ;
		}
		temp->next = newNode ;
	}
}

int countNode() {
	
	Node *temp = head ;
	int count = 0 ;

	while(temp != NULL) {
		count++ ;
		temp = temp->next ;
	}	
	return count ;
}

void deleteFirst() {

	Node *temp = head ;
	head = temp->next ;
	free(temp) ;
}

void deleteLast() {

	Node *temp = head ;
	
       while(temp->next->next != NULL) {
		temp = temp->next ;
       }
    
       free(temp->next) ;

       temp->next = NULL ;
}	

int delAtPos(int pos) {
	
	int count = countNode() ;

	if(pos <= 0 || pos > count) {
		printf("Invalid node position\n") ;
		return -1 ;
	}
	else{
		if(pos == count) 
			deleteLast() ;
		else if(pos == 1)
			deleteFirst() ;
		else{ 
		       
			Node *temp1 = head ;
			Node *temp2 = head ;
			
			while(pos-2) {
				temp1 = temp1->next ;
				temp2 = temp2->next ;
				pos-- ;
			}
			temp2 = temp2->next ;
			temp1->next = temp1->next->next ;	
			free(temp2) ;
		}
		
		return 0 ;
	}
}

void delStr(int len){
	
	int i = 0;

	Node *temp = head ;

	while(temp != NULL) {
		i++ ;
		
		if(len != strlen(temp->str))
			delAtPos(i);
		
		temp = temp->next ;
	}
}

int printLL() {

	if(head == NULL) {
		printf("Linked-list empty\n") ;
		return -1 ;
	
	} else { 
		Node* temp = head ;

		while(temp != NULL) {
			
			printf("|%s|->",temp->str) ;
			temp = temp->next ;
		}
		return 0 ;
	}
}

void main() {
	
	int nodes ;

	printf("Enter node count :\n") ;
	scanf("%d",&nodes) ;
		
	getchar() ;

	for(int i = 1 ; i<= nodes ; i++) {
		addNode() ;
	}

	printLL() ;
	
	int len ;

	printf("Enter length of string : \n") ;
	scanf("%d",&len) ;
	
	delStr(len) ;

	printLL() ;
}

