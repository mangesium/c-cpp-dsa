//IMPLEMENTING PRIORITY QUEUE USING LINKED LIST(using head ptr)

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	int priority;
	struct Node *next;
}Node;

Node *head = NULL;
int flag =0;



//For creating a node
Node* createNode(){

	Node *newnode = (Node*)malloc(sizeof(Node));

	printf("Enter data:");
	scanf("%d",&newnode->data);

	newnode->priority =-1;

	while(newnode->priority < 0 || newnode->priority > 5){
		printf("Enter priority (plz give priority b/w 0-5):");
		scanf("%d",&newnode->priority);

		if(newnode->priority < 0 || newnode->priority > 5)
			printf("Please enter valid input...\n");

	}


	newnode->next = NULL;

	return newnode;
}

//For adding Element in the Queue

int addNode(){

	Node *newnode = createNode();

	if(head == NULL){

		head=newnode;
	}else{

		if(head->priority < newnode->priority ){
			newnode->next=head;
			head=newnode;
		}
		else{
			Node *temp = head;

			if(temp->next == NULL){
				temp->next = newnode;
			}else{

				while(temp->next->priority >= newnode->priority){
					
					temp=temp->next;
					if(temp->next == NULL){
						break;
					}
				}

				newnode->next = temp->next;
				temp->next = newnode;
				
			}
		}
		
	}
	return 0;
}


//For deleting First element of Queue

int deleteFirst(){


	if(head == NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		int  val= head->data;
		if(head->next == NULL){

			free(head);
			head = NULL;
		}else{

			Node *temp = head ;
			head=head->next ;
			free(temp);

		}
		return val;

	}
}


//For print the Queue

int printQueue(){

	if(head == NULL){
		return -1;
	}else{

		Node *temp =head;

		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);

		return 0;
	}

}

void main(){

	int ch = 'y';


	do{

		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.PrintQueue\n");
		printf("4.Exit\n");


		int choice;
		printf("Enter choice:");
		scanf("%d",&choice);


		switch(choice){

			case 1:
				{
					int ret = addNode();

					if(ret == -1)
						printf("Queue Overflow...\n");
				}
				break;

			case 2:
				{
					int ret = deleteFirst();

					if(flag == 0)
						printf("Queue Underflow...\n");
					else
						printf("%d Dequeued\n",ret);
				}
				break;
			case 3:
				{
					int ret = printQueue();

					if(ret == -1)
						printf("Queue is empty...\n");
				}
				break;

			case 4:
				ch = 'n';
				break;

			default:
				printf("invalid input...\n");

		}
	}while(ch =='y');
}
