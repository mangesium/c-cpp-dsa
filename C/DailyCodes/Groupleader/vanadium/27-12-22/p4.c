#include<stdio.h>
#include<stdlib.h>

typedef struct PriorityQ{
	int data;
	int priority;
	struct PriorityQ *next;
}PriorityQ;
int flag;
PriorityQ *head = NULL;
PriorityQ * createNode() {
	PriorityQ* newNode = (PriorityQ*)malloc(sizeof(PriorityQ));
	printf("Enter Data : ");
	scanf("%d",&newNode->data);
	printf("Enter Priority : ");
	int temp = 1;
	int Priority = 0;
	while(temp) {
		scanf("%d",&Priority);
		if(Priority > 0) {
			newNode->priority = Priority;
			temp--;
		}
		else {
			printf("Enter Valid Priority\n");
		}
	}
	newNode->next = NULL;
	return newNode;
}
void EnQueue() {
	PriorityQ * newNode = createNode();
	PriorityQ *temp = head;
	if (head == NULL) {
		head = newNode;
	}
	else if(head->priority < newNode->priority){
		newNode->next = head;
		head = newNode;
	}
	else {
		while(temp->next!=NULL && temp->next->priority > newNode->priority){
			temp = temp->next;
		}
		newNode->next = temp->next;
		temp->next = newNode;
	}
}

int DeQueue() {
	if(head == NULL) {
		flag = 1;
		return -1;
	}
	else if(head->next == NULL) {
		int data = head->data;
		free(head);
		head = NULL;
		flag = 0;
		return data;
	}
	else {
		PriorityQ *temp = head->next;
		int data = head->data;
		free(head);
		head = temp;
		flag = 0;
		return data;
	}
}

void PrintQ(){
	PriorityQ *temp = head;
	if(head == NULL){
		printf("Empty Queue\n");
	}
	else {
		while(temp!=NULL) {
			printf("|%d|p=%d|-> ",temp->data,temp->priority);
			
			temp = temp->next;
		}
		printf("\n");
	}
}

void main() {
	int ch;
	char choice;
	do {
		printf("1 = EnQueue\n");
		printf("2 = DeQueue\n");
		printf("3 = PrintQueue\n");
		
		printf("Enter Choice:\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				EnQueue();
				break;
			case 2:
				{
				int Ret = DeQueue();
				if (flag == 1) 
					printf("Empty Queue\n");
				
				else 
					printf("%d Popped\n",Ret);
				}
				break;
			case 3:
				PrintQ();
				break;
			default :
				printf("Invalid Choice\n");
		}
		getchar();
		printf("Enter 'y' or 'Y' to Continue\n");
		choice = getchar();
	}while(choice == 'y' || choice == 'Y');
}



		
