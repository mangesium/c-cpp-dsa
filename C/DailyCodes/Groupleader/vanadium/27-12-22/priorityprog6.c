//Implementing priority queue using linked list.


#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	int priority;
	struct Node *next;
}Node;

Node *head = NULL;
int flag =0;

//For make Node-
Node* createNode(){

	Node *newnode = (Node*)malloc(sizeof(Node));

	printf("Enter data:");
	scanf("%d",&newnode->data);
	getchar();

	newnode->priority =-1;

	while(newnode->priority < 0 || newnode->priority > 5){
		printf("Enter priority between 0 to 5\n");
		scanf("%d",&newnode->priority);
		getchar();

		if(newnode->priority < 0 || newnode->priority > 5)
			printf("Please enter valid input..\n");

	}


	newnode->next = NULL;

	return newnode;
}
//For adding data in the Queue-
int addNode(){

	Node *newnode = createNode();

	if(head == NULL){

		head=newnode;
	}else{

		if(head->priority < newnode->priority ){
			newnode->next=head;
			head=newnode;
		}
		else{
			Node *temp = head;

			if(temp->next == NULL){
				temp->next = newnode;
			}else{

				while(temp->next->priority >= newnode->priority){
					
					temp=temp->next;
					if(temp->next == NULL){
						break;
					}
				}

				newnode->next = temp->next;
				temp->next = newnode;
				
			}
		}
		
	}
	return 0;
}
//For deleting First data in the Queue-
int deleteFirst(){
	if(head == NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		int  val= head->data;
		if(head->next == NULL){

			free(head);
			head = NULL;
		}else{

			Node *temp = head ;
			head=head->next ;
			free(temp);

		}
		return val;

	}
}
//For print the Queue-
int printqueue(){

	if(head == NULL){
		return -1;
	}else{

		Node *temp =head;

		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);

		return 0;
	}

}

void main(){
	printf("please select option.+-\n");
	char choice;
        do{

		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.PrintQueue\n");

		printf("\n");


		int ch;
		printf("Enter choice:");
		scanf("%d",&ch);
		getchar();


		switch(ch){

			case 1:
				{
				int ret = addNode();
				if(ret == -1)
				    	printf("Queue is Overflow...\n");
				}
				break;

			case 2:
				{
				int ret = deleteFirst();
				if(flag == 0)
					printf("Queue is Underflow.\n");
				else
				        printf("%d is  Dequeued\n",ret);
				}
				break;
			case 3:
				{
				int ret = printqueue();
				if(ret == -1)
					printf("Queue is empty.\n");
				}
				break;

			default:
				printf("You entered the invalid input.\n");

		}
		printf("Do you want to continue ?\nPlease enter Y or y\n");
		scanf("%c",&choice);
	}
	while(choice =='y'||choice == 'Y');
}
