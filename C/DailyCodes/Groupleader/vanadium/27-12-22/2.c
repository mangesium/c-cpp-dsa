#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int data;
    int priority;
    struct Node *next;
} Node;

Node *head = NULL;

Node *createNode()
{

    Node *newNode = (Node *)malloc(sizeof(Node));

    printf("Enter Data:: ");
    scanf("%d", &newNode->data);

    printf("Enter Priority:: ");
    scanf("%d", &newNode->priority);
    while (newNode->priority < 0 || newNode->priority > 5)
    {
        printf("Enter no. between 0 to 5 :: ");
        scanf("%d", &newNode->priority);
        break;
    }
    newNode->next = NULL;
    return newNode;
}
void enqueue()
{

    Node *newNode = createNode();

    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        Node *temp = head;
        if (head->priority < newNode->priority)
        {
            newNode->next = head;
            head = newNode;
        }
        else
        {
            while (temp->next != NULL)
            {
                if (temp->next->priority > newNode->priority)
                {
                    temp = temp->next;
                }
                else
                {
                    newNode->next = temp->next;
                    temp->next = newNode;
                }
            }
        }
    }
}
int dequeue()
{
    if (head->next == NULL)
    {
        int val = head->data;
        free(head);
        head = NULL;
        return val;
    }
    else
    {
        Node *temp = head;
        int val = head->data;
        head = head->next;
        free(temp);
        return val;
    }
}
void PrintLL()
{

    if (head == NULL)
    {
        printf("LL is empty\n");
    }
    else
    {
        Node *temp = head;
        while (temp != NULL)
        {

            printf("|%d|->", temp->data);
            printf("|%d||", temp->priority);
            temp = temp->next;
        }
    }
    printf("\n");
}
void main()
{

    char choise;
    do
    {
        printf("1.enqueue\n");
        printf("2.dequeue\n");
        printf("3.PrintLL\n");

        int ch;
        printf("Enter Choise:: ");
        scanf("%d", &ch);

        switch (ch)
        {

        case 1:
            enqueue();
            break;
        case 2:
        {
            int ret = dequeue();
            printf("%d is dequeued\n", ret);
        }
        break;
        case 3:
            PrintLL();
            break;
        default:
            printf("Wrong Choise\n");
            break;
        }
        getchar();
        printf("Do you wnat to continue [Y/y]:: ");
        scanf("%c", &choise);
    } while (choise == 'Y' || choise == 'y');
}