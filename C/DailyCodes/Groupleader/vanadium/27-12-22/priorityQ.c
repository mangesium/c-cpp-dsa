
//	Implementing priority Queue using linked-list

#include<stdio.h>
#include<stdlib.h>

int flag = 0 ;

typedef struct Node {
	int data ;
	int pr ;			//	priority
	struct Node *next ;
} Node ;

// head pointer
Node *head = NULL ;

Node* createNode() {

	Node *newNode = (Node*)malloc(sizeof(Node)) ;
	
	if(newNode == NULL){
		printf("Memory full\n") ;
		exit(0) ;
	}

	printf("Enter data : \n") ;
	scanf("%d",&newNode->data) ;

	do{
		printf("Enter priority : \n") ;
		scanf("%d",&newNode->pr) ;
	}while(newNode->pr < 0 && newNode->pr > 5) ;

	newNode->next = NULL ;

	return newNode ;
}
void enqueue() {
	
	Node* newNode = createNode() ;

	if(head == NULL) 
		head = newNode ;
	else{
		if(head->pr < newNode->pr){
			newNode->next = head ;
			head = newNode ;
		}else{
			Node *temp = head ;

			if(temp->next == NULL)
				temp->next = newNode ;
			else{
				while(newNode->pr <= temp->next->pr){
					temp = temp->next ;
					
					if(temp->next == NULL)
						break ;
				}
				newNode->next = temp->next ;
				temp->next = newNode ;

			}
		}
		
		
	}
}

int dequeue() {
	
	if(head == NULL){
		flag = 0 ;
		return -1 ;
	}
	else{
		flag = 1 ;
		int val = head->data ;

		if(head->next == NULL){
			free(head) ;
			head = NULL ;
		}else{	
			Node *temp = head ;
			head = temp->next ;
			free(temp) ;
		}

		return val ;
	}
}

// front function
int frontt() {
		
	if(head == NULL){
		flag = 0 ;
		return -1 ;
	}else{
		flag = 1 ;
		return head->data ;
	}
}	

int printQueue() {	

	if(head == NULL){
		flag = 0 ;
		return -1 ;
	}else{
		flag = 1 ;
		Node *temp = head ;

		while(temp != NULL) {
			printf("|%d|",temp->data) ;
			temp = temp->next ;
		}
		printf("\n") ;
		
		return 0 ;
	}
}
void main() {

	char choice ;

	do {

		printf("\n1. Enqueue\n") ;
		printf("2. Dequeue\n") ;
		printf("3. Front\n") ;
		printf("4. PrintQueue\n\n") ;

		int ch ;
		printf("Enter choice : \n") ;
		scanf("%d",&ch) ;

		switch(ch) {
		
			case 1 :
				enqueue() ;
				break ;

			case 2 : 
				{
				int ret = dequeue() ;
				if(flag == 1)
					printf("%d dequeued\n",ret) ;
				else
					printf("Queue Underflow\n") ;
				}
				break ;

			case 3 : 
				{
				int ret = frontt() ;
				if(flag == 1)
					printf("Front is : %d\n",ret) ;
				else
					printf("Queue empty\n") ;
				}
				break ;

			case 4 : 
				{
				int ret = printQueue() ;
				if(ret == -1)
					printf("Queue empty\n") ;
				}
				break ;

			default :
				printf("Invalid choice \n") ;
		}
		
		getchar() ;
		printf("Do you want to continue ?\n") ;
		scanf("%c",&choice) ;

	}while(choice == 'Y' || choice == 'y') ;
}

