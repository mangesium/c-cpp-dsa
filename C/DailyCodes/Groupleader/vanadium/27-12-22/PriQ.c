//priorotity Q using LL without using front and read pointer

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	int data;
	int pri;
	struct node *next;
}node;

node *head = NULL;

int high,flag=0;

node * createNode(){
	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter the data\n");
	scanf("%d",&newNode->data);

	do{
		printf("Enter priority\n");
		scanf("%d",&newNode->pri);
	
	}while(newNode->pri<0||newNode->pri>high);

	newNode->next = NULL;

	return newNode;
}

int enQ(){

	node * newNode = createNode();

	if(head == NULL){

		head = newNode;
		return 0;

	}

	if(head -> next == NULL){
	
		if(head->pri > newNode->pri){
			newNode->next = head ;
			head = newNode;
		}else{
			head->next = newNode;
		}

		return 0;
	}


	else{
		if(newNode->pri < head->pri){
			newNode->next = head;
			head = newNode;
			return 0;
		}

		node *temp = head;

		while(temp->next->next != NULL){

			if( temp->pri<=newNode->pri && newNode->pri<temp->next->pri){
			     	newNode->next = temp->next;
		     		temp->next = newNode;
				return 0;
			}

			temp = temp->next;
		}
		
		if( temp->pri<=newNode->pri&& newNode->pri < temp->next->pri){
		     	newNode->next = temp->next;
	     		temp->next = newNode;
		}else{
			temp = temp->next;
			temp -> next = newNode;
		}
		return 0;
	}
}

void printLL(){
	node *temp = head;
	while(temp != NULL){
		printf("|%d|-> |%d|\n",temp->data,temp->pri);
		temp = temp->next;
	}
}
int deQ(){
	
	if(head==NULL){
		return -1;
		flag = 0;
	}else{
		flag =1;
		int x=head->data;
		if(head->next==NULL){
			free(head);
			head=NULL;
		}else{
			node* temp=head;
			head=head->next;
			free(temp);
		}
		return x;
	}

}

void main(){

	
	char choice;
	printf("Enter Highest priority\n");
	scanf("%d",&high);
	

	
	do{
		printf("1.enQ\n");
		printf("2.PrintLL\n");
		printf("3.deQ\n");

		int ch;
		printf("Enter choice\n");
		scanf("%d",&ch);

		switch(ch){
			case 1:
				enQ();
				break;
			case 2:
				printLL();
				break;
			case 3:{
				int x=deQ();
				if(flag==0){
					printf("Q underflow\n");
				}else{
					
					printf("%d d Q\n",x);
				}
			       }
				break;
			default:
				printf("invalid\n");
				break;	
		}
		getchar();
		printf("Do you want to continue:??\n");
		scanf("%c",&choice);
	}while(choice=='Y'||choice=='y');
}

