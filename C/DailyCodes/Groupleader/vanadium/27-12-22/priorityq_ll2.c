//IMPLEMENTING PRIORITY QUEUE USING LINKED LIST(using front and rear ptr)

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {

	int data;
	int priority;
	struct Node *next;
}Node;

Node *front = NULL;
Node *rear = NULL;
int flag =0;



//For creating a node
Node* createNode(){

	Node *newnode = (Node*)malloc(sizeof(Node));

	printf("Enter data:");
	scanf("%d",&newnode->data);

	newnode->priority =-1;

	while(newnode->priority < 0 || newnode->priority > 5){
		printf("Enter priority (plz give priority b/w 0-5):");
		scanf("%d",&newnode->priority);

		if(newnode->priority < 0 || newnode->priority > 5)
			printf("Please enter valid input...\n");

	}


	newnode->next = NULL;

	return newnode;
}

//For adding Element in the Queue

int enqueue(){

	Node *newnode = createNode();

	if(front == NULL){

		front=newnode;
		rear=newnode;
	}else{

		if(front->priority < newnode->priority ){
			newnode->next=front;
			front=newnode;
			rear=newnode;
		}
		else{
			Node *temp = front;

			if(temp->next == NULL){
				temp->next = newnode;
			}else{

				while(temp->next->priority >= newnode->priority){
					
					temp=temp->next;
					if(temp->next == NULL){
						break;
					}
				}

				newnode->next = temp->next;
				temp->next = newnode;
				if(temp->next ==  NULL)
					rear=newnode;
				
			}
		}
		
	}
	return 0;
}


//For deleting First element of Queue

int dequeue(){


	if(front == NULL){
		flag=0;
		return -1;
	}else{
		flag=1;
		int  val= front->data;
		if(front->next == NULL){

			free(front);
			front = NULL;
		}else{

			Node *temp = front ;
			front=front->next ;
			free(temp);

		}
		return val;

	}
}


//For print the Queue

int printQueue(){

	if(front == NULL){
		return -1;
	}else{

		Node *temp =front;

		while(temp->next != NULL){
			printf("|%d|->",temp->data);
			temp=temp->next;
		}
		printf("|%d|\n",temp->data);

		return 0;
	}

}

void main(){

	int ch = 'y';


	do{

		printf("1.Enqueue\n");
		printf("2.Dequeue\n");
		printf("3.PrintQueue\n");
		printf("4.Exit\n");


		int choice;
		printf("Enter choice:");
		scanf("%d",&choice);


		switch(choice){

			case 1:
				{
					int ret = enqueue();

					if(ret == -1)
						printf("Queue Overflow...\n");
				}
				break;

			case 2:
				{
					int ret = dequeue();

					if(flag == 0)
						printf("Queue Underflow...\n");
					else
						printf("%d Dequeued\n",ret);
				}
				break;
			case 3:
				{
					int ret = printQueue();

					if(ret == -1)
						printf("Queue is empty...\n");
				}
				break;

			case 4:
				ch = 'n';
				break;

			default:
				printf("invalid input...\n");

		}
	}while(ch =='y');
}
