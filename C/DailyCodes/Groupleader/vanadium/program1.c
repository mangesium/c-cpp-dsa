#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;
	struct Demo *next;

}Demo;

Demo *head =NULL;

Demo* createnode(){

	Demo *newnode = (Demo*)malloc(sizeof(Demo));

	printf("Enter data :\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;
	return newnode;

}

void addnode(){

	Demo *newnode = createnode();

	if(head == NULL){
		head = newnode;
	}else{
		Demo *temp = head;

		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next =newnode;
	}
}

void countnode(){


	int count=0;
	Demo *temp = head;

	while(temp != NULL){
		 count++;
		 temp=temp->next;
	}

	printf("Nodes present in the linked list :%d\n",count);
	
}

void printnode(){

	
	Demo *temp = head;

	while(temp != NULL){

		printf("| %d |",temp->data);
		temp=temp->next;
	}
	printf("\n");

}

void addfirst(){

	Demo *newnode = createnode();

	if(head == NULL){
		head = newnode;
	}else{
		newnode->next = head;
		head =newnode;
	}
	printf("The node is added successfully at 1st position...\n");

}

void addatpos(int pos){
	 
	int n = pos;
	int count=0;
	Demo *temp =head;

	while(temp != NULL){
		count++;
	}
	
	if(pos ==1){
		
		addfirst();
			
	}else if(pos<count){
		
		Demo * newnode = createnode();

		Demo *temp = head;
	
		while(pos-2){
			temp=temp->next;
			pos--;
		}
		newnode->next=temp->next;
		temp->next = newnode;

		printf("The node is added successfully at %d postion\n",n);

	}else if(n	== count){
		
		addnode();
		printf("The node is added successfully at %d postion\n",n);
		
	
	}else{
		printf("The linked list  is to short .. sorry we cant add at %d postion..\n",n);
	}


}

void deletefirst(){

	if(head == NULL){
		printf("THERE IS NO NODE TO BE DELETE...\n");
	}else{

		Demo *temp = head;

		head = temp->next;
		free(temp);
		printf("1st Position node is successfully deleted\n");
	}
}

void deletelast(){

	if(head == NULL){
		printf("THERE IS NO NODE TO BE DELETE...\n");
	}else{

		Demo *temp = head;

		while(temp->next->next != NULL){
			temp=temp->next;
		}

		free(temp->next);
		temp->next = NULL;
		printf("Last node is deleted successfully\n");
	}
}

void main(){

	char choice;

	do{
		
		printf("----------LINKED LIST OPERATION----------\n");
		printf("1.Addnode\n");
		printf("2.Printnode\n");
		printf("3.Count the nodes\n");
		printf("4.Add node at first position\n");
		printf("5.Add node at given position\n");
		printf("6.Delete the first node\n");
		printf("7.Delete the last node\n");
		
		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);
		switch(ch){

			case 1:
				addnode();	
				break;

			case 2:
				printnode();
				break;	
			
			case 3:
				countnode();
				break;

			case 4:
				addfirst();
				break;

			case 5:
				{
					int x;
					printf("Enter at which position you want to enter the node:\n");
					scanf("%d",&x);
					addatpos(x);
				}
				break;

			case 6:
				deletefirst();	
				break;

			case 7:
				deletelast();
				break;
			
			default:
				printf("Nit Choice dya ki sheth\n");

		}
		
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	}while( choice == 'y' || choice == 'Y');
}

/*OUTPUT:
 
  ----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
5
Enter at which position you want to enter the node:
1
Enter data :
10
The node is added successfully at 1st position...
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
1
Enter data :
20
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
2
| 10 || 20 |
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
1
Enter data :
30
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
5
Enter at which position you want to enter the node:
6
The linked list  is to short .. sorry we cant add at 6 postion..
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
1
Enter data :
40
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
2
| 10 || 20 || 30 || 40 |
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
6
1st Position node is successfully deleted
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
2
| 20 || 30 || 40 |
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
7
Last node is deleted successfully
Do you want to continue:
y
----------LINKED LIST OPERATION----------
1.Addnode
2.Printnode
3.Count the nodes
4.Add node at first position
5.Add node at given position
6.Delete the first node
7.Delete the last node
Enter choice:
2
| 20 || 30 |
Do you want to continue:
n

*/
