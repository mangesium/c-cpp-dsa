//ALL TEST CASES HANDLED


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;

}Demo;

Demo *head = NULL;

Demo* createnode(){

	Demo *newnode = (Demo*)malloc(sizeof(Demo));

	printf("Enter data:\n");
	scanf("%d",&newnode->data);

	newnode->next = NULL;

	return newnode;
}

void addnode(){

	Demo *newnode = createnode();

	if(head == NULL){
		head = newnode;
	}else{
		Demo *temp = head;
		
		while(temp->next != NULL){
			
			temp=temp->next;
		}
		temp->next = newnode;
	}
}

int countnode(){

	int count =0;

	Demo *temp= head;

	while(temp != NULL){
		temp=temp->next;
		count++;
	}

	return count;
}

void printnode(){

	if(head == NULL){
		printf("THERE IS NO NODE PRINT... PLZ CREATE NODE FIRST\n");
	}else{
		
		Demo *temp = head;

		while(temp != NULL){
			printf("| %d |",temp->data);
			temp=temp->next;
		}
	 	printf("\n");
	}
}

void addfirst(){

	Demo *newnode = createnode();

	if(head == NULL){
		head =newnode;
	}else{
		newnode->next = head;
		head =newnode;
	}
	printf("NODE IS SUCCESFULLY ADDED AT 1ST POSITION...\n");
}

void addlast(){

	addnode();
}

void addatpos(int pos){

	
	int count = countnode();

	if(pos <= 0 || pos > count+1){
		printf("INVALID POSITION IS ENTERED...\n");
	}else if(pos == 1){
		addfirst();
	}else if(pos == count +1){
		addlast();
	}else{
		Demo *newnode = createnode();

		Demo *temp = head;

		while(pos-2){
			temp=temp->next;
		}
		newnode->next=temp->next;
		temp->next =newnode;
	}
}
 
void deletefirst(){

	if(head == NULL){
		printf("THERE IS NO NODE TO BE DELETE...\n");
	}else if(head->next == NULL){
		free(head);
		printf("NODE IS SUCCESFULLY DELETED...\n");
	}else{
		Demo *temp = head;
		head=temp->next;
		free(temp);
		printf("NODE IS SUCCESSFULLY DELETED...\n");
	}
}

void deletelast(){

	if(head == NULL){
		printf("THERE IS NO NODE TO BE DELETE...\n");
	}else if(head->next == NULL){
		free(head);
		printf("NODE IS SUCCESFULLY DELETED...\n");
	}else{
		Demo *temp = head;

		while(temp->next->next != NULL){
			temp=temp->next;
		}
		
		free(temp->next);
		temp->next = NULL;
		
		printf("NODE IS SUCCESSFULLY DELETED...\n");
	}
}

void deleteatpos(int pos){

	int count = countnode();

	if(pos <= 0 || pos > count){
		printf("INVALID INPUT...\n");
	}else if(head == NULL){
		printf("THERE IS NO NODE TO DELETE..\n");
	}else if(pos  == 1){
		deletefirst();
	}else if(pos == count){
		deletelast();
	}else{
		Demo *temp1 = head;
		Demo *temp2 = head;

		while(pos-2){
			temp1=temp1->next;
		}
		temp2 = temp1;
		temp1->next=temp1->next->next;
		free(temp2);
	}
}


void main(){

	int num;
	printf("Enter no. of nodes you want to create:\n");
	scanf("%d",&num);

	for(int i=1;i<=num;i++)
		addnode();

	char choice ='y';

	do{
		printf("----------*** LINKED LIST OPERATIONS ***----------\n");
		printf("1.Add node at first\n");
		printf("2.Add node at last\n");
		printf("3.Add node at given position\n");
		printf("4.Print the linked list\n");
		printf("5.Count the node\n");
		printf("6.Delete node at first\n");
		printf("7.Delete node at last\n");
		printf("8.Delete node at given position\n");
		printf("9.Exit\n");
		
		
		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);

		switch(ch){

			case 1:
				addfirst();
				break;

			case 2:
				addlast();
				break;

			case 3:
				{
					int pos;
					printf("Enter at position you want to insert a node:\n");
					scanf("%d",&pos);
					addatpos(pos);
				}
				break;

			case 4:
				printnode();
				break;
		
			case 5:
				printf("Nodes in the linked list:%d\n",countnode());
				break;

			case 6:
				deletefirst();
				break;

			case 7:
				deletelast();
				break;

			case 8:
				{
					int pos;
					printf("Enter at position you want to delete a node:\n");
					scanf("%d",&pos);

					deleteatpos(pos);
				}
				break;
			
			case 9:
				choice = 'n';
				break;

			default:
				printf("Wrong input entered...\n");
		}

	}while(choice == 'y' || choice == 'Y');

}


					 









