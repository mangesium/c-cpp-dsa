

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{

	int data;
	struct demo*next;
}dem;

  dem*head=NULL;
  
  dem*createnode(){

	  dem*newnode=(dem*)malloc(sizeof(dem));

	  printf("enter the data:\n");
	  scanf("%d",&newnode->data);

	  newnode->next=NULL;
	  return newnode;
  }

void addnode(){

	dem*newnode=createnode();

	if(head==null){
		head=newnode;
	}else{
		dem*temp=head;
		
		while(temp->next!=NULL){
			temp=temp->next;
		}
		temp->next=newnode;
	}
}

 unt countnode(){

	 int count=0;
	 dem*temp=head;

	 while(temp!=NULL){
		 count++;
		 temp=temp->next;
	 }

	 printf("nodes count is :%d",count);

	 return count;
 }

void printnode(){

	if(head==NULL){
		printf("there is no node , create new node\n");
	}else{
		dem*temp=head;

		while(temp!=NULL){

			printf("| %d |",temp->data);
			temp=temp->next;
		}

		printf("\n");
	}
}


 void addfirst(){

	 dem*newnode=createnode();

	 if(head==NULL){

	   head= newnode;
 }else{
	 newnode->next=head;
	 head=newnode;
 }
	 printf("the node is at first pos \n");
 }

 void addatpos(int pos){
	  
	  int count=0;

	  dem*temp=head;

	  while(temp!=NULL){

		  temp=temp->next;

		  count++;
	  }

	  int n=pos;

	  if(pos==1){

		  addfirst();

	  }else if(pos<=count && pos>0){

		  dem*newnode=createnode();

		  temp=head;

		  while(pos-2){

			  temp=temp->next;
			  pos--;

		  }
		  newnode->next=temp->next;
		  temp->next=newnode;

		   printf("the node is added sucessfully\n");

	  }else if(pos==count+1){
		  addnode();
		  printf("the node is at %d pos\n",n);
	  }else if(pos==0){
		  printf("unable to add node here\n");
	  }else if(pos<0){
		  printf("invalid pos\n");
	  }else{
		  printf("linkded list is too small\n");
	  }
 }
}


void deletefirst(){

	   if(head==NULL){
		   printf("there  is no node to delete\n");
	   }else{
		   dem*temp=head;

		   head=temp->next;
		   free(temp);
		   printf("node is deleted\n");
	   }
}
   void deletelast(){
	   if(head==NULL){
		   printf("there is no node to delete\n");
	   }else if(head->next==NULL){

		   deletefirst();

	   }else{
		   dem*temp=head;

		   while(temp->next->next !=NULL){
			   temp=temp->next;
		   }


		   free(temp->next);
		   temp->next=NULL;

		   printf("node is deleted\n");
	   }
   }


void main(){

	char choice;

	do{
		
		printf("----------LINKED LIST OPERATION----------\n");
		printf("1.Addnode\n");
		printf("2.Printnode\n");
		printf("3.Count the nodes\n");
		printf("4.Add node at first position\n");
		printf("5.Add node at given position\n");
		printf("6.Delete the first node\n");
		printf("7.Delete the last node\n");
		
		int ch;
		printf("Enter choice:\n");
		scanf("%d",&ch);
		switch(ch){

			case 1:
				addnode();	
				break;

			case 2:
				printnode();
				break;	
			
			case 3:
				countnode();
				break;

			case 4:
				addfirst();
				break;

			case 5:
				{
					int x;
					printf("Enter at which position you want to enter the node:\n");
					scanf("%d",&x);
					addatpos(x);
				}
				break;

			case 6:
				deletefirst();	
				break;

			case 7:
				deletelast();
				break;
			
			default:
				printf("invalid input\n");

		}
		
		getchar();
		printf("Do you want to continue:\n");
		scanf("%c",&choice);
	}while( choice == 'y' || choice == 'Y');
}


