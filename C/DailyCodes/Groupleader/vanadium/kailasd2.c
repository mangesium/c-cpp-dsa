#include<stdio.h>
#include<stdlib.h>
typedef struct employee{
	int id;
	struct employee *next;
}emp;
emp *head = NULL;
void PrintLL();
emp* createNode() {
	emp *newnode = (emp*)malloc(sizeof(emp));
	printf("Enter Id = ");
	scanf("%d",&newnode->id);
	newnode->next = NULL;
	return newnode;

}
void addNode(){
	emp *newnode = createNode();
	if (head == NULL) {
		head = newnode;
	}
	else {
		emp *temp = head;
		while (temp -> next != NULL) {
			temp = temp->next;
		}
		temp -> next = newnode;
	}
}
void addFirst() {
	emp *newnode = createNode();
	if (head == NULL) {
		head = newnode;
	}
	else {
		newnode -> next = head;
		head = newnode;
	}
	PrintLL();
}
void addLast(){
	addNode();
	PrintLL();
}
void add_at_position(int pos) {
	emp *newnode = createNode();
	emp *temp = head;
	while (pos-2) {
		temp = temp->next;
		pos--;
	}
	newnode->next = temp->next;
	temp->next = newnode;
	PrintLL();
}
int nodecount() {
	int cnt = 0;
	emp *temp = head;
	while (temp != NULL) {
		temp = temp->next;
		cnt++;
	}
	return cnt;
}

void removefirst(){
	if (head->next == NULL)   {
		free(head);
		printf("No nodes are present in Linkedlist now\n");
	}
	else {
		emp *temp = head;
		head = temp->next;
		free(temp);
		printf("first node deleted succesfully\n");
	}
	PrintLL();


}
void removelast(){
	if (head -> next == NULL) {
		free(head);
		printf("No nodes are present in Linkedlist now\n");
	}
	else {
		emp *temp = head;
		while (temp->next->next != NULL) {
			temp = temp->next;
		}

		free(temp->next);
		temp->next = NULL;
		printf("last node deleted succesfully\n");
	}
	PrintLL();
}
void remove_at_position(int pos) {
	int cnt = nodecount();
	if(pos == 1) {
		removefirst();
	}
	else if (pos == cnt) {
		removelast();
	}
	else {
		emp* temp = head;
		while (pos-2) {
			temp = temp->next;
			pos--;
		}
		emp *temp2 = temp -> next->next;
		free(temp->next);
		temp->next = temp2;
		PrintLL();
	}
}
void PrintLL() {
	emp *temp = head;
	while (temp != NULL) {
		printf("| %d |",temp->id);
		temp = temp->next;
	}
	printf("\n");
	
}
void main(){
	char ch;

	do {
		printf("choice 1 = addNode\n");
		printf("choice 2 = addFirst\n");
		printf("choice 3 = addLast\n");
		printf("choice 4 = add at Position\n");
		printf("choice 5 = count Nodes\n");
		printf("choice 6 = delete first Node\n");
		printf("choice 7 = delete Last Node\n");
		printf("choice 8 = Print delete at position\n");
		printf("choice 9 = print linkedlist\n");
		int choice;
		printf("Enter Choice No = ");
		scanf("%d",&choice);

		switch(choice){
			case 1:
				addNode();
				break;
			case 2 : 
				printf("adding node at first position\n");
				addFirst();
				break;
			case 3 :
				printf("adding node at last position\n");
				addLast();
				break;
			case 4 :
				{
				printf("adding node at given position\n");
				int pos;
				int no = 1;
				int cnt = nodecount();
				printf("enter the position that you want to add node\n");
				while (no) {
					scanf("%d",&pos);
					if (pos == 1) {
						addFirst();
						--no;
						break;
					}
					else if (pos>cnt+2 || pos <= 0) {
						printf("Enter an valid position between 1 to %d\n",cnt+1);
					}
					else if (pos > cnt+1) {
						addNode();
					}
					else {
						
						--no;
						add_at_position(pos);
					}
				}
				}
				break;
			case 5 :
				printf("counting Nodes in Linkedlist\n");
				int cnt = nodecount();
				printf("%d nodes are in given linkedlist till now\n",cnt);
				break;
			case 6 :
				printf("removing node from first position\n");
				removefirst();
				break;
			case 7:
				printf("removing node from last position\n");
				removelast();
				break;
			case 8:
			{
				int pos;
				printf("enter position for removing node\n");
				scanf("%d",&pos);
				cnt = nodecount();
				int no = 1;
				while (no){
					if (pos <= 0 || pos > cnt) {
						printf("enter valid position\n");
						scanf("%d",&pos);
					}
					else {
						remove_at_position(pos);
						no--;
					}
				}
				printf("removed node from given position\n");
			}
				break;
			
			case 9 :
				printf("output of Linkedlist = ");
				PrintLL();
				break;
			default :
				printf("Enter Valid Input\n");
		}
		getchar();
		printf("If you want to continue enter 'Y' or 'y' = ");
		scanf("%c",&ch);
	}while (ch != 'n');
}
