// Second largest occurance

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct node{
	int data;
	struct node *next;
}node;

node *head = NULL;
node *temp = NULL;

node *createnode(){
	node* new = (node*)malloc(sizeof(node));

	printf("Enter number : ");
	scanf("%d",&new->data);

	new->next = NULL;

	return new;
}

void addnode(){
	node* new = createnode();

	if(head==NULL){
		head = new;
		temp = new;
	}else{
		temp->next = new;
		temp = temp->next;
	}
}

void printll(){
	if(head==NULL){
		printf("no Node is created\n");
		return ;
	}
	node* ts = head;

	while(ts !=NULL){
		printf("--%d--||",ts->data);
		ts = ts->next;
	}
	printf("\n");
}

int occurance(int num){
	
	node* prev = head;
	node* curr = head;
	node* ts = head;
	int count = 0;
	while(ts!=NULL){
		if(ts->data==num){
			count++;
			if(prev !=curr){
				prev=curr;
			}
			curr = ts;

		}
		ts = ts->next;
	}
	ts = head;
	int ocount = 1;
	while(ts !=prev){
		ocount++;
		ts = ts->next;
	}
	if(count==0){
		printf("Zero Occurance of %d\n",num);
		return -1;
	}else if(count == 1){
		printf("Single occurance of %d\n",num);
	}else{
		printf("Second largest occurance is : %d\n",ocount);
	}
}

void main(){
	char choice;
	do{
		printf("1.addnode\n");
		printf("2.second occurance\n");
		printf("3.Print Linked List\n\n");
		int ch;
		printf("Enter coice number : ");
		scanf("%d",&ch);

		switch(ch){
			case 1 : {
					 int nCount;
					 printf("How many nodes you want to add : ");
					 scanf("%d",&nCount);
					 for(int i = 1;i<=nCount;i++){
					 	addnode();
					 }
					 printf("Successfully added %d nodes\n",nCount);
					 break;
				 }
			
			case 2 : {
				 	int num;
					 printf("Enter number : ");
					 scanf("%d",&num);
					 occurance(num);
				 
				}
				 break;

			case 3 :
				 printll();
				 break;

			default : 
				 printf("Invalid Choice\n");
		
		}
		getchar();
		printf("Do you want to continue (y/n) : ");
		scanf("%c",&choice);
		
	
	}while(choice=='y' || choice=='Y');

}
