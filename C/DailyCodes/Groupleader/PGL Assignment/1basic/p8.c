//Write a program to print a table of 11 in reverse order
#include<stdio.h>
void main(){    
    int x=10,y=1;
    while(x>y){
        printf("Enter starting value: ");
        scanf("%d",&x);
        printf("Enter limiting value: ");
        scanf("%d",&y);
    }
    int num=y;
    for(int i=y;i>=x;i--){    
        printf("%d\n",11*num);
        num--;
    }
}