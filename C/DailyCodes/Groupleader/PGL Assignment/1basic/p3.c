// Write a program to print the first ten, 3 digit number

#include<stdio.h>
void main(){
    int x=109,y=100;
    while(x>y){
        printf("Enter starting value: ");
        scanf("%d",&x);
        printf("Enter limiting value: ");
        scanf("%d",&y);
    }
    int num=100;
    if(x<0&&y<0)
        num=-100;
    for(int i=x;i<=y;i++){
        printf("%d\n",num);
        if(num<0)
            num--;
        else
            num++;
    }
}