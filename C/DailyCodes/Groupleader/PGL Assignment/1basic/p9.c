//Write a program to print the sum of the first 10 odd numbers

#include<stdio.h>
void main(){
     
    int x=10,y=1;
    while(x>y){
        printf("Enter starting value: ");
        scanf("%d",&x);
        printf("Enter limiting value: ");
        scanf("%d",&y);
    }
    long int sum=0;
    for(int i=x;i<=y;i++){
        if(i%2!=0)
            sum=sum+i;
    }
        printf("%ld\n",sum);

}