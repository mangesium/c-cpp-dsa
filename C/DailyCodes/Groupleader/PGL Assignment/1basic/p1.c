//Write a program to print the first 10 capital Alphabets
#include<stdio.h>
#include<stdbool.h>
void main(){
    int x,y;
    int flag=1;
    while(flag){
        printf("Enter starting value: ");
        scanf("%d",&x);
        printf("Enter limiting value: ");
        scanf("%d",&y);
        if(x<y){
            flag=0;
        }
    }
    char ch='A';
    for(int i=x;i<=y;i++){
        if(ch>'Z')
            ch='A';
        printf("%c\n",ch);
        ch++;
    }
}