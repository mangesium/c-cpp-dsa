// WAP that dynamically allocates a 2-D Array of integers , takes value from the user,print it.

#include <stdio.h>
#include <stdlib.h>
void main()
{
    int row = 0, col = 0;
    printf("Enter rows of array : ");
    scanf("%d", &row);
    printf("Enter columns of array : ");
    scanf("%d", &col);
    int **arr = (int **)malloc(sizeof(int *) * row);
    for (int i = 0; i < row; i++)
    {
        arr[i] = (int *)malloc(sizeof(int) * col);
        for (int j = 0; j < col; j++)
        {
            printf("Enter element in array : ");
            scanf("%d", &arr[i][j]);
        }
    }
    printf("Array of integer : \n");
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++)
        {
            printf("%d ", arr[i][j]);
        }
        printf("\n");
    }
    for (int i = 0; i < row; i++)
        free(arr[i]);
    free(arr);
}