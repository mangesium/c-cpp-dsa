// WAP that dynamically allocates a 3-D Array of integers , takes value from the user,print it.

#include <stdio.h>
#include <stdlib.h>
void main()
{
    int row = 0, col = 0, plane = 0;
    printf("Enter planes of array : ");
    scanf("%d", &plane);
    printf("Enter rows of array : ");
    scanf("%d", &row);
    printf("Enter columns of array : ");
    scanf("%d", &col);
    int ***arr = (int ***)malloc(sizeof(int **) * plane);
    for (int k = 0; k < plane; k++)
    {
        arr[k] = (int **)malloc(sizeof(int *) * row);
        for (int i = 0; i < row; i++)
        {
            arr[k][i] = (int *)malloc(sizeof(int) * col);
            for (int j = 0; j < col; j++)
            {
                printf("Enter element in array : ");
                scanf("%d", &arr[k][i][j]);
            }
        }
    }
    printf("Array of integer : \n");
    for (int k = 0; k < plane; k++)
    {
        printf("%dth plane :\n", k);
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                printf("%d ", arr[k][i][j]);
            }
            printf("\n");
        }
    }
    for (int i = 0; i < plane; i++)
    {
        for (int j = 0; j < row; j++)
        {
            free(arr[i][j]);
        }
        free(arr[i]);
    }
    free(arr);
}