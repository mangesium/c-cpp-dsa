// WAP that dynamically allocates a 1-D Array of marks , takes value from the user,print it.
#include <stdio.h>
#include <stdlib.h>
void main()
{
    int size = 0;
    printf("Enter Size of array : ");
    scanf("%d", &size);
    float *arr = (float *)malloc(sizeof(float) * size);
    for (int i = 0; i < size; i++)
    {
        printf("Enter element at %d index : ", i);
        scanf("%f", arr + i);
    }
    printf("Array of marks : ");
    for (int i = 0; i < size; i++)
    {
        printf("%.2f ", *(arr + i));
    }
    printf("\n");
    free(arr);
}