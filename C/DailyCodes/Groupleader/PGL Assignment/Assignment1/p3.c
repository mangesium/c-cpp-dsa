#include<stdio.h>
#include<limits.h>
void main(){
    int len=0;
    while(len<=0){
        printf("Enter size of array : ");
        scanf("%d",&len);
    }
    int arr[len];
    printf("Enter elements in array : ");
    for(int i=0;i<len;i++){
        scanf("%d",&arr[i]);
    }
    int flag=0;
    if(len<=0){
        flag=1;
    }
    int max=INT_MIN,min=INT_MAX;
    for(int i=0;i<len;i++){
        if(arr[i]>max){
            max=arr[i];
        }
        else if(min>arr[i]){
            min=arr[i];
        }
    }
    if(flag==0){
        printf("Maximum = %d and Minimum = %d\n",max,min);
    }
    else{
        printf("Empty array\n");
    }
}