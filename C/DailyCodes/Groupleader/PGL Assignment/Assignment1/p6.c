#include<stdio.h>

int occur(int*arr,int len,int num){

    if(len==0){
        printf("Empty array\n");
        return 0;
    }
    int count=0;
    for(int i=0;i<len;i++){

        if(num==arr[i]){
            count++;
        }
    }
    return count;
}

void main(){
    int len=0;
    while(len<=0){
        printf("Enter size of array : ");
        scanf("%d",&len);
    }
    int arr[len];
    printf("Enter elements in array : ");
    for(int i=0;i<len;i++){
        scanf("%d",&arr[i]);
    }
    int num;
    printf("Enter number to find occurrence : ");
    scanf("%d",&num);
    int count=occur(arr,len,num);
    printf("%d occur %dtimes in following array",num,count);
}