#include<stdio.h>
void pair(int*arr1,int*arr2,int len1,int len2,int sum){
    if(len1==0||len2==0){
        printf("Zero pairs\n");
        return;
    }
    int flag=0;
    printf("The number of pairs are : ");
    int i=0,j=0;
    for(i=0;i<len1;){
        if(j==len2-1){ 
            if(arr1[i]+arr2[j]==sum){
                printf("(%d,%d) ",arr1[i],arr2[j]);
                flag=1;
            }
            i++;
            j=0;
        }
        else if(j<len2){
             if(arr1[i]+arr2[j]==sum){
                printf("(%d,%d) ",arr1[i],arr2[j]);
                flag=1;
            }
            j++;
            continue;
        }
    } 
    if(flag==0){
        printf("Zero pairs\n");
    }
}
void main(){
    int len1=0,len2=0;
    while(len1<=0){
        printf("Enter size of 1st array : ");
        scanf("%d",&len1);
    }
    int arr1[len1];
    printf("Enter elements in 1st array : ");
    for(int i=0;i<len1;i++){
        scanf("%d",&arr1[i]);
    }
    while(len2<=0){
        printf("Enter size of 2nd array : ");
        scanf("%d",&len2);
    }
    int arr2[len2];
    printf("Enter elements in 2nd array : ");
    for(int i=0;i<len2;i++){
        scanf("%d",&arr2[i]);
    }
    int sum;
    printf("Enter number to search pairs : ");
    scanf("%d",&sum);
    pair(arr1,arr2,len1,len2,sum);   
}