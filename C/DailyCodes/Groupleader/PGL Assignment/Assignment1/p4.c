#include<stdio.h>
void search(int*arr,int len,int num){
    if(len<=0){
        printf("Empty array\n");
        return;
    }
    int flag=0;
    for(int i=0;i<len;i++){
        if(arr[i]==num){
            printf("%d is present at index %d",num,i);
            flag=1;
            break;
        }
    }
    if(flag==0){
        printf("Element not found\n");
    }
}

void main(){
    int len=0;
    while(len<=0){
    printf("Enter size of array : ");
    scanf("%d",&len);
    }
    int arr[len];
    printf("Enter elements in array : ");
    for(int i=0;i<len;i++){
        scanf("%d",&arr[i]);
    }
    int num;
    printf("Enter number to search : ");
    scanf("%d",&num);
    search(arr,len,num);
}