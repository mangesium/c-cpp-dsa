#include <stdio.h>
#include <limits.h>

int maxprod(int *arr, int len)
{
    if (len == 0)
    {
        printf("Empty array\n");
        return 0;
    }
    int max = INT_MIN;
    for (int i = 0; i < len; i++)
    {
        for (int j = i + 1; j < len; j++)
        {
            if (arr[i] * arr[j] > max)
            {
                max = arr[i] * arr[j];
            }
        }
    }
    return max;
}
void main()
{
    int len = 0;
    while (len <= 0)
    {
        printf("Enter size of array : ");
        scanf("%d", &len);
    }
    int arr[len];
    printf("Enter elements in array : ");
    for (int i = 0; i < len; i++)
    {
        scanf("%d", &arr[i]);
    }
    int max = maxprod(arr, len);
    printf("%d is the maximum product of two numbers in following array\n", max);
}