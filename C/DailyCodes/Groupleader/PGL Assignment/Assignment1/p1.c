#include<stdio.h>
void per(int* arr,int len){
    if(len==0){
        printf("Empty array\n");
        return;
    }
    int flag=0;
    for(int i=0;i<len/2;i++){
        if(arr[i]==arr[len-i-1]){
            continue;
        }
        else{
            flag=1;
            break;
        }
    }
    if(flag==0){
        printf("PERFECT\n");
    }else{
        printf("NOT PERFECT\n");
    }
}
void main(){
    int len=0;
    while(len<=0){
        printf("Enter size of array : ");
        scanf("%d",&len);
    }
    int arr[len];
    printf("Enter elements in array : ");
    for(int i=0;i<len;i++){
        scanf("%d",&arr[i]);
    }
    per(arr,len);
}