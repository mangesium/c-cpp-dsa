#include<stdio.h>
void swap(int *x,int *y){
    int temp=*x;
    *x=*y;
    *y=temp;
}
int part(int* arr,int start,int end){
    int pivot=arr[end];
    int index=start;
    for(int i=start;i<end;i++){
        if(arr[i]<=arr[end]){
            swap(&arr[i],&arr[index]);
            index++;
        }
    }
    swap(&arr[end],&arr[index]);
    return index;
}

void quick(int *arr,int start,int end){
    if(start<end){
        int pivot=part(arr,start,end);
        quick(arr,start,pivot-1);
        quick(arr,pivot+1,end);
    }
}
void main(){
    int len=0;
    while(len<=0){
        printf("Enter size of array : ");
        scanf("%d",&len);
    }
    int arr[len];
    printf("Enter elements in array : ");
    for(int i=0;i<len;i++){
        scanf("%d",&arr[i]);
    }
    int num=0;
    while(num>len || num<=0){
    printf("Enter degree of smallest element : ");
    scanf("%d",&num);
    }
    int flag=0; 
    if(num>len || num<=0){
        flag=1;
    }
    if(flag==0){
        int start=0,end=len-1;
        quick(arr,start,end);
        for(int i=0;i<len;i++){
            printf("%d ",arr[i]);
        }
        printf("\n");
        printf("%d is the Kth smallest number\n",arr[num-1]);
    }else{
        printf("Degree of element not valid\n");
    }

}