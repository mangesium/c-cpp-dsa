#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct demo
{
    char str[20];
    struct demo *next;
} d;
d *head = NULL;
d *createnode()
{
    d *newnode = (d *)malloc(sizeof(d));
    printf("Enter String : ");
    char ch;
    int i = 0;
    while ((ch = getchar()) != '\n')
    {
        (*newnode).str[i++] = ch;
    }
    newnode->next = NULL;
    return newnode;
}
void addnode()
{
    d *newnode = createnode();
    if (head == NULL)
    {
        head = newnode;
    }
    else
    {
        d *temp = head;
        while (temp->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = newnode;
    }
}
void cmpstring(int num)
{
    d *temp = head;
    while (temp)
    {
        if ((strlen(temp->str)) == num)
        {
            printf("%s\n",temp->str);
        }
        temp=temp->next;
    }
}
void main()
{
    int num = 0;
    printf("Enter Number of node : ");
    scanf("%d", &num);
    getchar();
    for (int i = 0; i < num; i++)
    {
        addnode();
    }
    printf("Enter String length to compare : ");
    scanf("%d", &num);
    cmpstring(num);
}