#include <stdio.h>
#include<stdlib.h>
#include<string.h>
typedef struct demo
{
    int data;
    struct demo* next;
}d;
d* head=NULL;
d *createnode()
{
    d *newnode = (d *)malloc(sizeof(d));
    printf("Enter data:");
    scanf("%d", &newnode->data);
    newnode->next = NULL;
    return newnode;
}
void addnode()
{
    d *newnode = createnode();
    if (head == NULL)
    {
        head = newnode;
    }
    else
    {
        d *temp = head;
        while (temp->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = newnode;
    }
}
void findelement(int num){
    d* temp=head;
    int count=0,flag=0;
    while(temp){
        count++;
        if(num==temp->data){
            flag=1;
            break;
        }
        temp=temp->next;
    }
    if(flag){
        printf("%d is found at %d position\n",num,count);

    }else{
        printf("%d is Not available in linked list\n",num);
    }
}
void findsnum( int n)
{
    int p1 = 0, p2 = 0 ,count = 0;
    d* temp=head;
    while (temp != NULL)
    {
        count++;
        if (n == temp->data)
        {
            p2 = p1;
            p1 = count;
        }
        temp = temp->next;
    }
    if (p2 != 0)
    {
        printf("%d's second last occourance is at %d in linked list\n", n, p2);
    }
    else if (p1 != 0)
    {
        printf("%d's occourance is once at %d position in linked list\n", n, p1);
    }
    else
        printf("%d is not present in linked list\n", n);
}
void findelementoccur(int num){
    d* temp=head;
    int count=0,occ=0;
    while(temp){
        count++;
        if(num==temp->data){
            occ++;
        }
        temp=temp->next;
    }
    if(occ){
        printf("%d's occurance is %d\n",num,occ);

    }else{
        printf("%d is Not available in linked list\n",num);
    }
}
int fun(int num){
    int sum=0;
    while(num!=0){
        sum=sum+num%10;
        num/=10;
    }
    return sum;

}
void makedigit(){
    d* temp=head;
    while(temp){
        int num=fun(temp->data);
        temp->data=num;
        temp=temp->next;
    }
}
int ispalindrome(int num)
{
    int sum=0;
    while(num!=0){
        sum=sum*10+num%10;
        num/=10;
    }
    return sum;

}
void searchpalindrome(){
    d* temp=head;
    int count=0;
    while(temp){
        count++;
        if(temp->data==ispalindrome(temp->data)){
            printf("%d ia a palindrome number at position %d\n",temp->data,count);
        }
        temp=temp->next;
    }
}

void printnode(){
	d* temp=head;
	while(temp!=NULL){
		if(temp->next!=NULL)
			printf("%d->",temp->data);
		else
			printf("%d",temp->data);
		temp=temp->next;
	}
	printf("\n");
}
void main()
{
    int num = 0;
    printf("Enter Number of node : ");
    scanf("%d", &num);
    for (int i = 0; i < num; i++)
    {
        addnode();
    }
    printnode();
    printf("Enter Element to search : ");
    scanf("%d",&num);
    findelement(num);
    //makedigit();
    //printnode();
    searchpalindrome();

}