#include<stdio.h>
void main(){
    int num;
    char chr;
    float rs;
    double crMoney;
    printf("Enter integer number : ");
    scanf("%d",&num);
    printf("Enter character : ");
    getchar();
    scanf("%c",&chr);
    printf("Enter float value : ");
    scanf("%f",&rs);
    printf("Enter double value : ");
    scanf("%lf",&crMoney);

    printf("num = %d is integer of size %ld bytes\n",num,sizeof(num));
    printf("chr = %c is character of size %ld bytes\n",chr,sizeof(chr));
    printf("rs = %f is float of size %ld bytes\n",rs,sizeof(rs));
    printf("crMoney = %lf is double of size %ld bytes\n",crMoney,sizeof(crMoney));
     
}