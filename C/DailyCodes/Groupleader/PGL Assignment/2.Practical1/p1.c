#include<stdio.h>

void main(){
    int year=0,num=0;
    while(year<=0){
        printf("Enter year :");
        scanf("%d",&year);
    }
    if(year%4==0){
        if(year%100==0){
            if(year%400==0){
                num=1;
            }
            else{
                num=0;
            }
        }
        else
            num=1;
    }else{
        num=0;
    }
    if(num)
        printf("%d is the Leap Year\n",year);
    else
        printf("%d is not a Leap Year\n",year);

}