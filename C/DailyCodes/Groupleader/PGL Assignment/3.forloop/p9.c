#include<stdio.h>
void main()
{
    int num;
    float temp,root;
    int num1=10,num2=1;
    while(num1>num2){
        printf("Enter smaller number : ");
        scanf("%d",&num1);
        printf("Enter greater number : ");
        scanf("%d",&num2);
        if(num1<0||num2<0){
            num1=10;
            num2=1;
            continue;
        }
    }
    for(int i=num1;i<=num2;i++){
        root = i/2;
        temp = 0;

        while(root != temp){
            temp = root;
            root = ( i/temp + temp) / 2;
        }

        printf("The square root of %d is %f\n",i,root);
    }
}