//WAP to take the Number input and print all the factors of that number
#include<stdio.h>
void main(){
    int num=0;
    printf("Enter number : ");
    scanf("%d",&num);
    printf("The factors of number are : ");
    for(int i=1;i<num/2;i++){
        if(num%i==0)
            printf("%d ",i);
    }
    printf("%d\n",num);
}