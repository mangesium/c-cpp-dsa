/*WAP to print all even numbers in reverse order and odd numbers in the standard
way. Both separately. Within a range.  */
#include<stdio.h>
void main(){
    int start=10,end=1;
    while(start>end){
        printf("Enter starting value :");
        scanf("%d",&start);
         printf("Enter ending value :");
        scanf("%d",&end);
    }
    int flag=0,num=0;
    printf("Even numbers : ");
    for(int i=end;i>=start;i--){
        if(i%2==0&&flag==0)
            printf("%d ",i);
        if(i==start&&flag==0){
            i=end;
            num=start;
            flag=1;
            printf("\n");
            printf("Odd numbers : ");
        }
        if(flag==1)
            num++;
        if(num%2!=0&&flag==1)
            printf("%d ",num);
    }
    printf("\n");
}
