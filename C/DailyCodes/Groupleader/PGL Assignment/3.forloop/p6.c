//WAP to calculate the factorial of a given number.

#include<stdio.h>
void main(){
    int num=-1;
    while(num<0){
        printf("Enter number : ");
        scanf("%d",&num);
    }
    int sum=num;
    long fact=1;
    while(num>0){
        fact*=num;
        num--;
    }
    printf("%ld is the factorial of %d\n",fact,sum);
}