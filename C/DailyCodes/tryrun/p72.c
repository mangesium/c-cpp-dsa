#include<stdio.h>
int fun(int *arr,int len){
    int o_cnt=0,e_cnt=0;
    for(int i=0;i<len;i++){
        if(arr[i]%2==0)
            e_cnt++;
        else
            o_cnt++;
    }
    if(o_cnt==e_cnt)
        return 1;
    return 0;
}
void main(){
    int arr[]={-1,2,3,4,5,6};
    int len=sizeof(arr)/sizeof(int);
    printf("%d\n",fun(arr,len));
}