#include <stdio.h>
void main()
{
    int Row = 4;
    int row = 2 * Row + 1, k = 0, num = 1;
    for (int i = 1; i <= row; i++)
    {
        if (i <= row / 2 + 1)
            k++;
        else
            k--;
        num = 0;
        for (int j = 1; j <= row; j++)
        {
            if (j <= row / 2+1 )
                num++;
            else
                num--;
            if (k == num)
                printf("%d ", num);
            else
                printf("  ");
        }
        printf("\n");
    }
}