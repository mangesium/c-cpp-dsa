#include<stdio.h>
#include<string.h>
int max(int a,int b){
    if(a>b)
        return a;
    return b;
}
int les(char *str1, char *str2){

    int i,j,len1, len2;
    len1= strlen(str1);
    len2 = strlen(str2);
    int arr[len1+1][len2 + 1];
    for(i = 0; i <= len1; i++)
        arr[i][0] = 0;

    for(i = 0; i <= len2; i++) 
        arr[0][i] = 0;

    for(i = 1; i <= len1; i++) { 
        for(j= 1; j <= len2; j++) {

            if(str1[i-1] == str2[j-1]){
                arr[i][j] = 1 + arr[i - 1][j-1]; 
            }
            else 
            arr[i][j] = max(arr[i - 1][j], arr[i][j-1]);
        }
    }
    return arr[len1][len2];
}   

int main() {

char str1[] = "hbcfgmnapq", str2[] = "cbhgrsfnmq"; 
int ans = les(str1, str2); 
printf("%d", ans);

return 0;
}