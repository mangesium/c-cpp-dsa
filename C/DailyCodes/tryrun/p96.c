#include <stdio.h>
int fact(int num)
{
    static int facto = 1;
    if (num == 1)
    {
        return facto;
    }
    facto *= num;
    fact(--num);
}
void main()
{
    printf("%d\n", fact(5));
}