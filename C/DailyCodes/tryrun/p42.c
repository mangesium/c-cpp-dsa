#include<stdio.h>
#include<stdbool.h>
#include<string.h>
bool arrayStringsAreEqual(char ** word1, int word1Size, char ** word2, int word2Size){
    
    if(word1Size!=word2Size)
        return false;
    int i=0,j=0,p=0,q=0;
    char ch1=' ';
    char ch2=' ';

    while(i<word1Size&&p<word2Size&&ch1==ch2){
        ch1=word1[i][j];
        j++;
        if(j==strlen(word1[i])){
            i++;
            j=0;
        }
        ch2=word2[p][q];
        q++;
        if(q==strlen(word2[p])){
            p++;
            q=0;
        }
    }
    if(ch1==ch2&&i==word1Size&&p==word2Size)
        return true;
    return false;
    
}


void main(){
    char* carr1[]={"a","cb"};
    char* carr2[]={"a","bc"};

    printf("%d\n",arrayStringsAreEqual(carr1,2,carr2,2));
}