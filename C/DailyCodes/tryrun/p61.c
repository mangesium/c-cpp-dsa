#include<stdio.h>
#include<stdlib.h>

int* fun(int *len){
    int size=0;
    printf("Enter Size of array : ");
    scanf("%d",&size);
    int* arr=malloc(sizeof(int)*size);
    printf("Enter elements in array : ");
    for(int i=0;i<size;i++){
        scanf("%d",&arr[i]);
    }
    *len=size;
    return arr;
}

void main(){
    int size;
    int* arr=fun(&size);

    for(int i=0;i<size;i++){
        printf("%d ",arr[i]);
    }
}