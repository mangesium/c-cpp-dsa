#include<stdio.h>
int count(int* arr,int len,int sum){
    int count=0;
    for(int i=0;i<len;i++){
        for(int j=i+1;j<len;j++){
            if(arr[i]+j==sum){
                count++;
            }
        }
    }
    return count;
}
void main(){
    int arr[]={0,1,2,3,4,5,6};
    int sum=6;
    int len = sizeof(arr)/sizeof(int);
    printf("%d\n",count(arr,len,sum));
}