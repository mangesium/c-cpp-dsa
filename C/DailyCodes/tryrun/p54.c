//Passing pointer to an array of pointer to an function 
#include<stdio.h>
void fun(int (*ptr[])[]){
    printf("In fun = %d\n",(*(*(*ptr)+0)+1));
}
void main(){
    int arr1[]={1,2,3,4};
    int arr2[]={4,5,6,7};
    int arr3[]={7,8,9,10};
    int (*ptr[3])[]={&arr1,&arr2,&arr3};

    fun(ptr);
}