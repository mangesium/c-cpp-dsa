#include<stdio.h>
#include<string.h>
int strStr(char * haystack, char * needle){
    if(needle[0]=='\0')
        return 0;
    int flag=0;
    for(int i=0;i<strlen(haystack);i++){
        if(needle[0]==haystack[i]){
            for(int j=1;j<strlen(needle);j++){
                if(needle[j]==haystack[i+j]){
                    flag++;
                    continue;
                }
                else{
                    flag=0;
                    break;
                }
            }
        }
        if(flag!=0)
            return i;        
    }
    return -1;
}
void main(){
    int num=strStr("hello","llp");
    printf("%d\n",num);
}