//IMPLEMENTATION OF STACK USING ARRAY


#include<stdio.h>

int top=-1;
int size;
int flag=1;

int push(int *stack){

	if(top == size-1){
		printf("Stack Overflow...\n");
		return -1;
	}else{
	
		printf("Enter data:\n");
		scanf("%d",&(stack[++top]));

		return 0;
	}


}

int pop(int *stack){

	if(top == -1){
		flag=0;
		printf("Stack Underflow...\n");
		return -1;
	}else{
		flag=1;
		int ret = stack[top];
		top--;

		return ret;
	}
}


int peek(int *stack){

	if(top == -1){
		flag =0;
		printf("Stack Underflow...\n");
		return -1;
	}else{
		flag=1;
		return stack[top];
	
	}
}

void main(){

	printf("Enter size of stack:\n");
	scanf("%d",&size);

	int stack[size];
	
	char ch = 'y';

	do{
		printf("----------STACK OPERATION----------\n");
		printf("1.Push\n2.Pop\n3.Peek\n4.Exit\n");

		int choice;
		printf("Enter choice:\n");
		scanf("%d",&choice);

		switch(choice){

			case 1:
				push(stack);
				break;

			case 2:{
					int ret =pop(stack);
					if(flag == 1)
						printf("%d is popped\n",ret);
				}

				break;
			
			case 3:{
					int ret =peek(stack);
					if(flag == 1)
						printf("%d\n",ret);
				}

				break;

			case 4:
				ch = 'n';
				break;

			default:
				printf("Invalid input...\n");
		}

	}while(ch == 'y');
}

