#include <stdio.h>
int arrsum(int *arr, int len)
{
    static int i = -1;
    i++;
    if (i == len-1)
    {
        return arr[len - 1];
    }
    return arr[i] + arrsum(arr, len);
}
void main()
{
    int arr[] = {1, 2, 3, 4, 5,6,7,8,9,10};
    int len = sizeof(arr) / sizeof(int);
    printf("array sum = %d\n", arrsum(arr, len));
}