#include <stdio.h>
void main()
{
    int arr1[] = {1, 2, 3, 4};
    int arr2[] = {5, 6, 7, 8};
    int arr3[] = {9, 10, 11, 12};
    int k = 0;

    int(*ptr[])[4] = {&arr1, &arr2, &arr3};
    printf("%ld\n", sizeof(*ptr[0]));

    for (int i = 0; i < 3; i++)
    {
        int len = sizeof(*ptr[i]) / sizeof(int);
        for (int j = 0; j < len; j++)
        {
            printf("%d ", *(*(*(ptr + i) + k) + j));
        }
        printf("\n");
    }
}
