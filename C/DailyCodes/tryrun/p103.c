#include <stdio.h>
#include <string.h>
int ispalindrome(char *str, int start,int end)
{
    if (str[start] != str[end])
    {
        return 0;
    }
    else if (start == strlen(str) / 2)
        return 1;
    else
        return ispalindrome(str, ++start,--end);
}
void main()
{
    char str[500];
    printf("Enter String : ");
    char ch;
    int i = 0;
    while ((ch = getchar()) != '\n')
    {
        str[i] = ch;
        i++;
    }
    str[i] = '\0';
    int start=0,end=strlen(str)-1;
    if (ispalindrome(str, start,end))
    {
        printf(" %s is a Palindrome String\n", str);
    }
    else
    {
        printf("%s is not a Palindrome String\n", str);
    }
}