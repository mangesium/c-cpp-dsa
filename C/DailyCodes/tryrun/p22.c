#include<stdio.h>
#include<string.h>
int addStrings(char * num1, char * num2){
    int len1=strlen(num1);
    int len2=strlen(num2);
    int sum1=0,sum2=0;
    for(int i=0;i<len1;i++){
        sum1=sum1*10+num1[i]-'0';
    }
    for(int i=0;i<len2;i++){
        sum2=sum2*10+num2[i]-'0';
    }
    return sum1+sum2;

}
void main(){
    printf("%d\n",addStrings("11","123"));
}