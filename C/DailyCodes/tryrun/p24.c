#include<stdio.h>
#include<stdbool.h>
#include<string.h>
bool canConstruct(char * ransomNote, char * magazine){
    int a[26]={0};
    int b[26]={0};
    if(strlen(ransomNote)==0)
        return true;

    for(int i=0;i<strlen(ransomNote);i++){
        a[ransomNote[i]-'a']++;
    }
    for(int i=0;i<strlen(magazine);i++){
        b[magazine[i]-'a']++;
    }

    int k=0;
    for(int i=0;i<26;i++){
        if(a[i]!=0){
            if(a[i]<=b[i])
                k+=a[i];
        }
    }

    if(k==strlen(ransomNote))
    return true;

    return false;
}
void main(){
    printf("%d\n",canConstruct("aa","a"));
}