#include<stdio.h>
#include<stdlib.h>
int* decrypt(int* code, int codeSize, int k, int* returnSize){ 
    int* arr= malloc(sizeof(int)*codeSize);
    *returnSize=codeSize;
    int sum=0,num=k,flag=0,j=0;
    for(int i=0;i<codeSize;i++){
        sum=0;
        num=k;
        j=i+1;
        if(i==codeSize-1&&flag==0){
            j=0;
            flag=1;
        }
        while(num){
            sum+=code[j];
            j++;
            if(j==codeSize)
                j=0;
            num--;
        }
        //printf(" sum= %d ",sum);
        arr[i]=sum;
    }
    return arr;
}
void main(){
    int arr[]={5,7,1,4};
    int x=0;
    int *ptr=decrypt(arr,4,3,&x);

    for(int i=0;i<x;i++){
        printf(" %d ",ptr[i]);  //[12,10,16,13]
    }
    

}