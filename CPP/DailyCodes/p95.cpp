#include <iostream>

template <typename T,typename U,typename V>
struct Node
{
    T name;
    U id;
    V balance;
};

int main()
{

    Node<std::string,int,double> node = {"Mangesh", 1, 123456789.99};

    std::cout << node.name<<" = " << node.id <<" = "<< node.balance << std::endl;
}