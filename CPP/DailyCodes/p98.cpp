#include <iostream>
int main()
{
    std::cout << "Start main" << std::endl;
    try
    {
        throw 'A';  //  In exception datatype does not get upcasted or lowcasted like function calls
    }
    catch (int x) // terminate called after throwing an instance of 'char'
    {
        std::cerr << x << '\n';
    }
    catch (...)
    {
        std::cout << "In Generic catch for premetive data type" << std::endl;
    }

    std::cout << "End main" << std::endl;

    return 0;
}