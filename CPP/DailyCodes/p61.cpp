#include <iostream>
class One;
class Two
{
    int x = 10;

public:
    void accessData(const One &obj);
};
class One
{
    int x = 20, y = 30;

public:
    friend void Two::accessData(const One &obj);
};

void Two::accessData(const One &obj)
{
    std::cout << "One x = " << obj.x << std::endl;
    std::cout << "One y = " << obj.y << std::endl;
}
int main()
{
    One obj1;
    Two obj2;
    obj2.accessData(obj1);
    return 0;
}