#include <iostream>
class One
{
    int x = 10, y = 20;
    friend class Two;
};
class Two
{

public:
    void accessData(const One &obj)
    {
        std::cout << "One x = " << obj.x << std::endl;
        std::cout << "One y = " << obj.y << std::endl;
    }
};
int main()
{
    One obj1;
    Two obj2;
    obj2.accessData(obj1);
    return 0;
}