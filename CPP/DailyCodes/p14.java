public class p14 {

    p14(int x) {
        System.out.println("In para");
    }

    p14() {
        //p14 d = new p14();
        this(10);
        System.out.println("In constructor");
    }

    public static void main(String[] args) {

        p14 d = new p14();
    }
}