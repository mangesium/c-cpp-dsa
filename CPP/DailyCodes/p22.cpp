#include <iostream>

class Demo
{
    int x = 10;

public:
    Demo(int x)
    {
        this->x = x;
    }
    int getData() const
    {
        return x;
    }
};
std::ostream& operator<<(std::ostream &cout, const Demo &obj)
{
    std::cout << obj.getData() ;
    return cout;
}
int main()
{

    Demo obj(10);
    std::cout << obj << std::endl;
    return 0;
}

//Operator Overloading using Normal function