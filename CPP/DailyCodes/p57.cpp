#include <iostream>
class Parent
{

    int x = 10;

protected:
    int y = 20; // protected Scope

public:
    int z = 30;

    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    void getData() // Public Scope
    {
        std::cout << "Parent x = " << x << "Parent y = " << y << "Parent z = " << z << std::endl;
    }
};
class Child : public Parent
{
    // using Parent::getData; // Private Scope
    //   void getData() = delete;    //use of deleted function

public:
    using Parent::y; // Public Scope
};
int main()
{
    Child obj;
    std::cout << "Child y = " << obj.y << "Child z = " << obj.z << std::endl;
    obj.getData();
    return 0;
}

// Changing Scope of access Specifier