#include <iostream>

class One;
class Two
{
    int x = 10, y = 20;

public:
    void accessData(const One &obj);
};
class One
{
    friend void Two::accessData(const One &obj);
};
void Two::accessData(const One &obj) // Member Friend Function
{
    std::cout << "X = " << x << std::endl;
    std::cout << "Y = " << y << std::endl;
}

int main()
{
    std::cout << std::min(10, 20) << std::endl;
    // One one;
    // Two two;
    // two.accessData(one);
    return 0;
}