#include <iostream>

class Demo
{
    int x = 10;

public:
    // int operator++(int)
    // {
    //     int temp = x;
    //     x++;
    //     return temp;
    // }
    // int operator++()
    // {
    //     return ++x;
    // }
    // friend int operator++(Demo &obj,int)
    // {
    //     int temp = obj.x;
    //     obj.x++;
    //     return temp;
    // }
    // friend int operator++(Demo &obj)
    // {
    //     return ++obj.x;
    // }
    friend std::ostream &operator<<(std::ostream &out, const Demo &obj)
    {
        out << obj.x;
    }
    int getX()
    {

        return x;
    }
    void setX(int x)
    {
        this->x = x;
    }
};
int operator++(Demo &obj,int)
{
    int temp = obj.getX();
    obj.setX(temp + 1);
    return temp;
}
int operator++(Demo &obj)
{
    obj.setX(obj.getX() + 1);
    return obj.getX();
}

int main()
{

    Demo obj;
    std::cout << obj++ << std::endl;
    std::cout << ++obj << std::endl;
    std::cout << obj << std::endl;
}