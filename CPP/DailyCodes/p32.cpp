#include <iostream>
class Demo
{
    int x = 10;
    int y = 20;

public:
    Demo(int x, int y)
    {
        this->x = x;
        this->y = y;
    }

    int operator<=(const Demo &obj)
    {
        return x <= obj.x && y <= obj.y;
    }
};

int main()
{

    Demo obj1(100, 200);
    Demo obj2(30, 400);

    std::cout << (obj1 <= obj2) << std::endl;
}