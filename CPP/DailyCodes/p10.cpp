#include <iostream>
class Demo
{
    // int j;   //Error
public:
    int y;
    std::string str;
    int x;
};
int main()
{
    Demo d{18, "virat"};
    std::cout << "(" << d.y << ", " << d.str << ", " << d.x << ")\n";
    return 0;
}
