#include <iostream>
using namespace std;

const int MIN_RUN = 3;

// Function to perform Insertion Sort on array from left to right
void insertionSort(int arr[], int left, int right)
{
    for (int i = left + 1; i <= right; i++)
    {
        int key = arr[i];
        int j = i - 1;
        while (j >= left && arr[j] > key)
        {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key;
    }
}

// Function to perform Merge operation on array from left to mid and mid+1 to right
void merge(int arr[], int left, int mid, int right)
{
    int len1 = mid - left + 1;
    int len2 = right - mid;
    int left_arr[len1], right_arr[len2];

    for (int i = 0; i < len1; i++)
        left_arr[i] = arr[left + i];
    for (int j = 0; j < len2; j++)
        right_arr[j] = arr[mid + 1 + j];

    int i = 0, j = 0, k = left;

    while (i < len1 && j < len2)
    {
        if (left_arr[i] <= right_arr[j])
        {
            arr[k] = left_arr[i];
            i++;
        }
        else
        {
            arr[k] = right_arr[j];
            j++;
        }
        k++;
    }

    while (i < len1)
    {
        arr[k] = left_arr[i];
        i++;
        k++;
    }

    while (j < len2)
    {
        arr[k] = right_arr[j];
        j++;
        k++;
    }
}

// Function to perform Tim Sort on the array
void timSort(int arr[], int n)
{
    // Insertion Sort for small runs
    for (int i = 0; i < n; i += MIN_RUN)
    {
        insertionSort(arr, i, std::min(i + MIN_RUN - 1, n - 1));
    }

    // Merge the sorted runs
    for (int size = MIN_RUN; size < n; size *= 2)
    {
        for (int left = 0; left < n; left += 2 * size)
        {
            std::cout << "In merge inner for" << std::endl;
            int mid = left + size - 1;
            int right = min(left + 2 * size - 1, n - 1);
            merge(arr, left, mid, right);
        }
    }
}

int main()
{

    int input_array[] = {64, 34, 25, 12, 22, 11, 90};
    int n = sizeof(input_array) / sizeof(input_array[0]);

    timSort(input_array, n);

    // Print the sorted array
    for (int i = 0; i < n; i++)
    {
        cout << input_array[i] << " ";
    }
    cout << endl;

    return 0;
}
