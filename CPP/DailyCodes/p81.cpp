#include <iostream>
class Parent
{

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    // Virtual Destructor =solved problem of child object desructor
    virtual ~Parent()
    {
        std::cout << "Parent Destructor" << std::endl;
    }
    // void getData()
    // {
    //     std::cout << "Parent getData" << std::endl;
    // }
};

class Child : public Parent
{
public:
    Child()
    {
        std::cout << "Child Constructor" << std::endl;
    }
    ~Child()
    {
        std::cout << "Child Destructor" << std::endl;
    }
};
int main()
{

    Parent *obj = new Child();
    delete obj;
    return 0;
}
