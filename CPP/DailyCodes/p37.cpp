#include <iostream>
class Demo
{
    int x = 10;

public:
    friend void *operator new(size_t size)
    {
        std::cout << "In new" << std::endl;
        void *ptr = malloc(size);
        return ptr;
    }

    void operator delete(void *ptr)
    {
        std::cout << ptr << std::endl;
        std::cout << "In delete" << std::endl;
        free(ptr);
    }
    ~Demo()
    {

        std::cout << this << std::endl;
        std::cout << "In Destructor" << std::endl;
    }
};
int main()
{
    Demo *obj = new Demo();
    delete obj;
    return 0;
}