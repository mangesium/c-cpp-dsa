#include <iostream>
class Demo
{
    int x = 10, y = 20;

public:
    Demo(Demo &obj)
    {
        std::cout << "Copy " << x << std::endl;
    }
    Demo()
    {
        std::cout << "No-Args" << x << std::endl;
    }

    void getData()
    {
        std::cout << "X = " << x << std::endl;
        std::cout << "Y = " << y << std::endl;
    }
    friend void printdata(const Demo &obj) // Friend function
    {
        std::cout << "X = " << obj.x << std::endl;
        std::cout << "Y = " << obj.y << std::endl;
    }
};

int main()
{
    Demo obj;
    printdata(obj);
    return 0;
}