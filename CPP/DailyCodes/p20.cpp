#include <iostream>
class One;
class Two
{
    int x = 10;

protected:
    int y = 20;

public:
    void accessData(const One &obj);
};
class One
{
    int x = 50;

protected:
    int y = 60;

public:
    friend void Two::accessData(const One &obj);
};
void Two::accessData(const One &obj)
{
    std::cout << "x = " << obj.x << std::endl;
    std::cout << "y = " << obj.y << std::endl;
}

int main()
{
    Two obj1;
    One obj2;
    obj1.accessData(obj2);
    return 0;
}

// Friend Member function