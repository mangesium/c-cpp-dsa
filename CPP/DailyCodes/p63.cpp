#include <iostream>
class Parent
{

public:
    virtual void getData(int x)
    {
        std::cout << "Parent getData" << std::endl;
    }
    virtual void printData()
    {
        std::cout << "Parent printData" << std::endl;
    }
};
class Child : public Parent
{

public:
    void getData(short int x) // For overriding in cpp Method signature and return type must be same
    {
        std::cout << "Child getData" << std::endl;
    }
    void printData()
    {
        std::cout << "Child printData" << std::endl;
    }
};

int main()
{
    Parent *obj = new Child();
    obj->getData(10);
    obj->printData();
    return 0;
}