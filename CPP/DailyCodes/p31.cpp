#include <iostream>
class Demo
{
    int x = 10;
    int y = 20;

public:
    Demo(int x, int y)
    {
        this->x = x;
        this->y = y;
    }

    // int operator<(const Demo &obj)
    // {
    //     std::cout << "In Member Function" << std::endl;
    //     return x < obj.x && y < obj.y;
    // }
    // friend int operator<(const Demo &obj1, const Demo &obj2)
    // {
    //     std::cout << "In Friend Function" << std::endl;
    //     return (obj1.x < obj2.x) && obj1.y < obj2.y;
    // }
    int getX() const
    {
        return x;
    }
    int getY() const
    {
        return y;
    }
};
int operator<(const Demo &obj1, const Demo &obj2)
{
    std::cout << "In Normal Function" << std::endl;
    return (obj1.getX() < obj2.getX()) && (obj1.getY() < obj2.getY());
}
int main()
{

    Demo obj1(100, 200);
    Demo obj2(300, 400);

    std::cout << (obj1 < obj2) << std::endl;
}