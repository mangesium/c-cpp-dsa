#include <iostream>

template <typename T>
struct Node
{
    T data;
    Node *next;
};
template <class T>
class SinglyCircularLinkedList
{
    Node<T> *head = NULL;

public:
    Node<T> *createNode();
    void addLast();
    void addFirst();
    void addAtPos(int pos);
    void printLinkedList();
    int countNode();
    void deleteFirst();
    void deleteLast();
    void deleteAtPos(int pos);
    void reverse();
    void implReverse();
};
template <typename T>
Node<T> *SinglyCircularLinkedList<T>::createNode()
{
    Node<T> *newnode = new Node<T>();
    std::cout << "Enter data = ";
    std::cin >> newnode->data;
    newnode->next = NULL;
}
template <typename T>
void SinglyCircularLinkedList<T>::printLinkedList()
{
    Node<T> *temp = head;
    while (temp->next != head)
    {
        std::cout << "|" << temp->data << "|->";
        temp = temp->next;
    }
    std::cout << "|" << temp->data << "|" << std::endl;
}
template <typename T>
int SinglyCircularLinkedList<T>::countNode()
{
    if (head == nullptr)
        return 0;
    Node<T> *temp = head;
    int cnt = 1;
    while (temp->next != head)
    {
        cnt++;
        temp = temp->next;
    }
    return cnt;
}
template <typename T>
void SinglyCircularLinkedList<T>::addLast()
{
    if (head == nullptr)
    {
        head = createNode();
        head->next = head;
    }
    else
    {
        Node<T> *newnode = createNode();
        Node<T> *temp = head;
        while (temp->next != head)
        {
            temp = temp->next;
        }
        newnode->next = head;
        temp->next = newnode;
    }
}
template <typename T>
void SinglyCircularLinkedList<T>::addFirst()
{
    if (head == nullptr)
    {
        head = createNode();
        head->next = head;
    }
    else
    {
        Node<T> *newnode = createNode();
        Node<T> *temp = head;
        while (temp->next != head)
        {
            temp = temp->next;
        }
        temp->next = newnode;
        newnode->next = head;
        head = newnode;
    }
}
template <typename T>
void SinglyCircularLinkedList<T>::addAtPos(int pos)
{
    if (pos < 1 || pos > countNode() + 1)
    {
        std::cout << "Invalid Position" << std::endl;
    }
    else if (pos == 1)
    {
        addFirst();
    }
    else if (pos == countNode() + 1)
    {
        addLast();
    }
    else
    {
        Node<T> *temp = head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        Node<T> *newnode = createNode();
        newnode->next = temp->next;
        temp->next = newnode;
    }
}
template <typename T>
void SinglyCircularLinkedList<T>::deleteFirst()
{
    if (head == nullptr)
    {
        std::cout << "Empty Linkedlist" << std::endl;
    }
    else
    {
        Node<T> *temp = head;
        while (temp->next != head)
        {
            temp = temp->next;
        }
        Node<T> *node = head;
        head = head->next;
        temp->next = head;
        delete node;
    }
}
template <typename T>
void SinglyCircularLinkedList<T>::deleteLast()
{
    if (head == nullptr)
    {
        std::cout << "Empty Linkedlist" << std::endl;
    }
    else
    {
        Node<T> *temp = head;
        while (temp->next->next != head)
        {
            temp = temp->next;
        }
        Node<T> *node = temp->next;
        temp->next = head;
        delete node;
    }
}
template <typename T>
void SinglyCircularLinkedList<T>::deleteAtPos(int pos)
{
    if (pos < 1 || pos > countNode())
    {
        std::cout << "Invalid position" << std::endl;
    }
    else if (pos == 1)
    {
        deleteFirst();
    }
    else if (pos == countNode())
    {
        deleteLast();
    }
    else
    {
        Node<T> *temp = head;
        while(pos-2){
            temp=temp->next;
            pos--;
        }
        Node<T>* node =temp->next;
        temp->next=node->next;
        delete node;
    }
}
template <typename T>
void SinglyCircularLinkedList<T>::reverse()
{
    Node<T> *temp1 = head;
    Node<T> *temp2 = head;
    int cnt = countNode(), x = 0, k = 0;
    while (cnt / 2 - x)
    {
        k = cnt - 1;
        while (k - x)
        {
            temp2 = temp2->next;
            k--;
        }
        T swap = temp1->data;
        temp1->data = temp2->data;
        temp2->data = swap;
        temp1 = temp1->next;
        temp2 = head;
        x++;
    }
}
template <typename T>
void SinglyCircularLinkedList<T>::implReverse(){
    Node<T>* temp1 = head;
    Node<T>* temp2 = NULL;
    Node<T>* temp3 = NULL;

    while(temp3!=head){
        temp3=temp1->next;
        temp1->next=temp2;
        temp2=temp1;
        temp1=temp3;
    }
    temp3->next=temp2;
    head=temp2;
}
main()
{
    int exit = 1;
    do
    {
        std::cout << "1.Integer" << std::endl;
        std::cout << "2.Float" << std::endl;
        std::cout << "3.Character" << std::endl;
        std::cout << "4.Double" << std::endl;
        std::cout << "5.String" << std::endl;
        std::cout << "6.exit" << std::endl;
        std::cout << "Choose data type = ";
        int choice;
        std::cin >> choice;
        switch (choice)
        {
        case (1):
        {
            SinglyCircularLinkedList<int> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (2):
        {
            SinglyCircularLinkedList<float> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (3):
        {
            SinglyCircularLinkedList<char> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (4):
        {
            SinglyCircularLinkedList<double> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (5):
        {
            SinglyCircularLinkedList<std::string> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (6):
        {
            exit = 0;
            break;
        }
        }

    } while (exit == 1);
    return 0;
}