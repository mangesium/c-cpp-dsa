#include <iostream>

class Demo
{

    int x = 10;
    int y = 10;

public:
    Demo()
    {
        std::cout << "No-Args" << std::endl;
    }
    Demo(int x)
    {
        std::cout << "Args" << std::endl;
    }
    Demo(Demo &obj)
    {
        std::cout << "Copy" << std::endl;
    }
};

int main()
{
    Demo obj1;
    Demo obj2;
    Demo obj3;
    Demo obj4;

    Demo arr[] = {obj1, obj2, obj3, obj4};
    std::cout << &obj1 << std::endl;
    std::cout << &arr[0] << std::endl;
}