#include <iostream>
class Parent
{

public:
    virtual Parent *getData(int x)
    {
        std::cout << "Parent getData" << std::endl;
        return this;
        // Parent obj;
        // return obj;
    }
};
class Child : public Parent
{

public:
    Child *getData(int x)
    {
        std::cout << "Child getData" << std::endl;
        return this;
        // Child obj;
        // return obj;
    }
};

int main()
{
    Parent *obj = new Child();
    obj->getData(10);
    return 0;
}