#include <iostream>

class Demo
{

    int x = 10;
    int y = 20;

public:
    Demo()
    {
        std::cout << "No-Args" << std::endl;
    }
    Demo(Demo &ref)
    {
        std::cout << "Copy-Constructor" << std::endl;
    }
    int getData() const
    {
        return this->x;
    }
};
int main()
{
    const Demo obj;
    std::cout << obj.getData() << std::endl;

    Demo obj1;
    std::cout << &obj1 << std::endl;
    Demo obj2=obj1;
    std::cout << &obj2 << std::endl;
    // obj2 = obj1;
    // std::cout << &obj1 << std::endl;

}
