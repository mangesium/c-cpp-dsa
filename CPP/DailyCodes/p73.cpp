#include <iostream>

class Parent
{
    int x = 10;

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    virtual void getData() = 0;
    void property()
    {
        std::cout << "100 Acre " << std::endl;
    }
};

void Parent::getData() // Parent can give body to it's pure virtual function as if it's multiple child have same or default function
{
    std::cout << "Parent getData" << std::endl;
}
class Child : public Parent
{

public:
    void getData()
    {
        std::cout << "child getData" << std::endl;
    }
};
int main()
{

    Parent *obj = new Child();
    obj->getData();
    obj->property();
    return 0;
}
