#include <iostream>
class One
{
    int x = 10, y = 20;

public:
    friend class Two;   //Friend class
};
class Two
{
public:
    void getData(const One &obj)
    {
        std::cout << "X = " << obj.x << std::endl;
        std::cout << "Y = " << obj.y << std::endl;
    }
};
int main()
{
    Two obj;
    One Objo;
    obj.getData(Objo);

    return 0;
}