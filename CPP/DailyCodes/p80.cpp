#include <iostream>
class Parent
{

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }

    ~Parent()
    {
        std::cout << "Parent Destructor" << std::endl;
    }
    // void getData()
    // {
    //     std::cout << "Parent getData" << std::endl;
    // }
};

class Child : public Parent
{
public:
    Child()
    {
        std::cout << "Child Constructor" << std::endl;
    }
    ~Child()
    {
        std::cout << "Child Destructor" << std::endl;
    }
};
int main()
{

    Parent *obj = new Child();
    delete obj; // Only refrence classe's destructor gets called here
    return 0;
}
