#include <iostream>

class Parent
{

public:
    virtual void getData();
};
class Child : public Parent
{

public:
    void getData()
    {
        std::cout << "child getData" << std::endl;
    }
};
int main()
{

    Parent *obj = new Child(); //(.text$_ZN6ParentC2Ev[__ZN6ParentC2Ev]+0xa): undefined reference to `vtable for Parent' collect2.exe: error: ld returned 1 exit status
    obj->getData();
    return 0;
}
