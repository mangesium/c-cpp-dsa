#include <iostream>
//class Two;
class One
{
    int x = 10;

protected:
    int y = 20;

public:
    void accessData() const
    {
        std::cout << "x = " << x << std::endl;
        std::cout << "y = " << y << std::endl;
    }
    friend class Two;
};
class Two
{
    int x = 10;

protected:
    int y = 20;

public:
    void getData(const One &obj)
    {
        obj.accessData();
        std::cout << "x = " << obj.x << std::endl;
        std::cout << "y = " << obj.y << std::endl;
        std::cout << "x = " << x << std::endl;
        std::cout << "y = " << y << std::endl;
    }
};
int main()
{
    One obj1;
    Two obj2;
    obj2.getData(obj1);
    return 0;
}
//Friend Class