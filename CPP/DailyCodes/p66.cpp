#include <iostream>
class Parent
{
public:
    virtual void getData(int x) final // If we make member function final then child cannot write or override given member function
    {
        std::cout << "Parent getData" << std::endl;
    }
};
class Child final : public Parent // If we make class final then it is not possible to inherit it
{

public:
    void getData(int x)
    {
        std::cout << "Child getData" << std::endl;
    }
};
class Child1 : public Child
{
};
int main()
{
    return 0;
}