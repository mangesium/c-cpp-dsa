#include <iostream>
class Parent
{

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    void getData()
    {
        std::cout << "Parent getData" << std::endl;
    }
};
class Child1 : public Parent
{

public:
    Child1()
    {
        std::cout << "Child1 Constructor" << std::endl;
    }
    void getData()
    {
        std::cout << "child1 getData" << std::endl;
    }
};
class Child2 : public Parent
{

public:
    Child2()
    {
        std::cout << "Child2 Constructor" << std::endl;
    }
    void getData()
    {
        std::cout << "child2 getData" << std::endl;
    }
};
// Repeated Class Construction problem in cpp == see output
class Child : public Child1, public Child2
{
public:
    Child()
    {
        std::cout << "Child Constructor" << std::endl;
    }
};
int main()
{

    Child obj;
    // obj.getData();
    return 0;
}
