// Rethrowing function

#include <iostream>
int elementSearch(int *arr, int size, int num)
{

    try
    {
        for (int i = 0; i < size; i++)
        {
            if (arr[i] == num)
                return i;
        }
        throw "Exception : Number not found";
    }
    catch (const char *str)
    {
        std::cout << str << "In Function" << std::endl;
        throw;
    }
}

int main()
{
    int size;
    std::cout << "Enter size of array : ";
    std::cin >> size;
    int arr[size];
    for (int i = 0; i < size; i++)
    {
        std::cout << "Enter element: ";
        std::cin >> arr[i];
    }
    int num;
    std::cout << "Enter element to search : ";
    std::cin >> num;

    try
    {
        std::cout << "Element Present at index : " << elementSearch(arr, size, num) << std::endl;
    }
    catch (const char *str)
    {
        std::cout << str << "In main" << std::endl;
    }
    return 0;
}