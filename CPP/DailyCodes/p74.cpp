#include <iostream>

class IDemo
{
public:
    virtual void getData() = 0;
    virtual void printData() = 0;
};
/*Adapet Design PAttern*/
class Adapter : public IDemo
{
public:
    void getData() {}
    void printData() {}
};
class DChild1 : public Adapter
{
public:
    void getData() {}
};
class DChild2 : public Adapter
{
public:
    void printData() {}
};
int main()
{
    Adapter a;
    DChild1 obj1;
    DChild2 obj2;
    return 0;
}