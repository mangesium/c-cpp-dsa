#include <iostream>
#include <vector>

class Demo
{

    int x = 10;
    int *ptr = new int[1000000];

public:
    Demo *next;
    void *operator new(size_t size, Demo **obj)
    {
        // std::cout << "In overloaded new" << std::endl;
        void *ptr = malloc(size);
        // void *ptr = ::operator new(size);
        std::cout << ptr << std::endl;
        if (ptr == NULL)
        {
            std::cout << "Object NULLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL" << std::endl;
            Demo *temp = *obj;
            *obj = (*obj)->next;
            delete temp;
        }
        return ptr;
    }
    void operator delete(void *ptr)
    {
        free(ptr);
    }
    ~Demo()
    {
        delete[] ptr;
        std::cout << "Object Deleted" << std::endl;
    }
};

int main()
{
    Demo *head = new (&head) Demo();
    Demo *temp = head;
    while (1)
    {
        temp->next = new (&head) Demo();
        // std::cout << "Object Created" << std::endl;
    }
    return 0;
}