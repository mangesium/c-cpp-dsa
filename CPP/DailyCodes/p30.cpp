#include <iostream>

class One;
class Two
{
    int x = 20;

public:
    void accessData(const One &obj);
};
class One
{

    int x = 10;

protected:
    int y = 20;

    friend void Two::accessData(const One &obj);
};
void Two::accessData(const One &obj)
{
    std::cout << "One X = " << obj.x << std::endl;
    std::cout << "Two X = " << x << std::endl;
    std::cout << "One Y = " << obj.y << std::endl;
}
int main()
{
    One obj1;
    Two obj2;
    obj2.accessData(obj1);
    return 0;
}