#include <iostream>

class Parent
{
    int x = 10;

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    Parent(Parent &obj)
    {
        std::cout << "Parent Copy Constructor" << std::endl;
    }
    friend std::ostream &operator<<(std::ostream &out, const Parent &obj)
    {
        out << "In Parent" << std::endl;
        out << obj.x;
        return out;
    }
};
class Child : public Parent
{
    int x = 10;

public:
    Child()
    {
        std::cout << "Child Constructor" << std::endl;
    }
    Child(Child &obj)
    {
        std::cout << "Child Copy Constructor" << std::endl;
    }
    friend std::ostream &operator<<(std::ostream &out, const Child &obj)
    {
        out << "In Child" << std::endl;
        out << obj.x;
        return out;
    }
};
int main()
{
    Child obj;
    std::cout << obj << std::endl;
    std::cout << (const Parent)obj << std::endl;   // Copy constructor call
    std::cout << (const Parent &)obj << std::endl; // No Copy Constructor call

    return 0;
}