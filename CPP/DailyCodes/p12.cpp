#include <iostream>

class Demo
{
public:
    int x = 10;
    Demo()
    {
        std::cout << "In No_args Constructor" << std::endl;
    }
    Demo(int x)
    {
        std::cout << "In Para Constructor" << std::endl;
    }
    Demo(Demo &xyz)
    {
        free(&xyz);
        std::cout << "In copy Constructor" << std::endl;
    }
};

int main()
{
    Demo *d=new Demo();
    Demo d1(*d);
    d1.x = 20;
    std::cout << d1.x << std::endl;

    return 0;
}