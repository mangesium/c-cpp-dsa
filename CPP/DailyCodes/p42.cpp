#include <iostream>

class Parent
{
    int x = 10;

protected:
    int y = 20;

public:
    int z = 30;
    Parent()
    {
        std::cout << "Normal Parent" << this << std::endl;
    }
    Parent(int x, int y, int z)
    {
        std::cout << "Parameter Parent" << this << std::endl;
        this->x = x;
        this->y = y;
        this->z = z;
    }
    void getData()
    {
        std::cout << x << y << z << std::endl;
    }
};
class Child : Parent
{
    // int x = 100;
    // int y = 200;

public:
    Child()
    {
        std::cout << this << std::endl;
        Parent(11, 22, 33);
    }
    void getInfo()
    {
        getData();
        std::cout << y << z << std::endl;
    }
};

int main()
{
    Child c;
    c.getInfo();
    return 0;
}