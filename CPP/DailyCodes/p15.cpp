#include <iostream>

class Demo
{

    int x = 10;

public:
    Demo()
    {
        this->x = 80;
        std::cout << x << std::endl;
        std::cout << "No-Args" << std::endl;
        Demo(0);
    }
    Demo(int x)
    {
        std::cout << this->x << std::endl;
        std::cout << "Args" << std::endl;
    }
};

int main()
{
    Demo obj;
    return 0;
}