// Late Binding
#include <iostream>

void add(int x, int y)
{
    printf("%d", x + y);
}
void sub(int x, int y)
{
    printf("%d", x - y);
}
void mult(int x, int y)
{
    printf("%d", x * y);
}
int main()
{
    printf("1.add\n");
    printf("2.sub\n");
    printf("3.multi\n");
    printf("Enter Choice :");
    int ch;
    std::cin >> ch;
    void (*funptr)(int, int) = nullptr;
    switch (ch)
    {
    case 1:
        funptr = add;
        break;
    case 2:
        funptr = sub;
        break;
    case 3:
        funptr = mult;
        break;
    }
    funptr(20, 10); // Here compiler dont know which function is going to call for any input while compile time hence late binding
    return 0;
}