#include <iostream>

class One
{
    int x = 10;
    friend class Two;
};
class Two
{
    int x = 100;

public:
    void accessData(const One &obj)
    {
        std::cout << "One X = " << obj.x << std::endl;
        std::cout << "Two X = " << x << std::endl;
    }
};

int main()
{
    One obj1;
    Two obj2;
    obj2.accessData(obj1);
    return 0;
}