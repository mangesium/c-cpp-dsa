#include <iostream>

class Parent
{

public:
    virtual void getData()
    {
        std::cout << "Parent getData" << std::endl;
    }
};
class Child : public Parent
{

public:
    void getData() // Here virtual keyword comes implecitly if we wrote it explicitly there will be no change in beheviour
    {
        std::cout << "Child getData" << std::endl;
    }
};
int main()
{
    Parent *obj = new Child();
    obj->getData();
    return 0;
}