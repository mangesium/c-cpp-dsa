# Insertion Sort used for small runs and merging runs
def insertion_sort(arr, left, right):
    for i in range(left + 1, right + 1):
        key = arr[i]
        j = i - 1
        while j >= left and arr[j] > key:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key

# Merge function for merging two sorted runs
def merge(arr, left, mid, right):
    len1, len2 = mid - left + 1, right - mid
    left_arr, right_arr = arr[left:mid + 1], arr[mid + 1:right + 1]

    i, j, k = 0, 0, left

    while i < len1 and j < len2:
        if left_arr[i] <= right_arr[j]:
            arr[k] = left_arr[i]
            i += 1
        else:
            arr[k] = right_arr[j]
            j += 1
        k += 1

    while i < len1:
        arr[k] = left_arr[i]
        i += 1
        k += 1

    while j < len2:
        arr[k] = right_arr[j]
        j += 1
        k += 1

# Tim Sort implementation
def tim_sort(arr):
    min_run = 32
    n = len(arr)

    # Insertion Sort for small runs
    for i in range(0, n, min_run):
        insertion_sort(arr, i, min((i + min_run - 1), n - 1))

    # Merge the sorted runs
    size = min_run
    while size < n:
        for left in range(0, n, 2 * size):
            mid = min((left + size - 1), n - 1)
            right = min((left + 2 * size - 1), n - 1)
            merge(arr, left, mid, right)
        size *= 2

    return arr

# Example usage:
input_list = [64, 34, 25, 12, 22, 11, 90]
sorted_list = tim_sort(input_list)
print(sorted_list)
