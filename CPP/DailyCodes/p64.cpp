#include <iostream>
class Parent
{

public:
    virtual void getData(int x)
    {
        std::cout << "Parent getData" << std::endl;
    }
    virtual void printData()
    {
        std::cout << "Parent printData" << std::endl;
    }
};
class Child : public Parent
{

public:
    void getData(short int x)
    {
        std::cout << "Child getData" << std::endl;
    }
    void printData()
    {
        std::cout << "Child printData" << std::endl;
    }
};
int main()
{
    Parent *obj = new Child();
    obj->getData(10);
    obj->printData();

    Child obj1;
    Parent *obj2 = &obj1;
    obj2->getData(10);
    obj2->printData();

    Child obj3;
    Parent &obj4 = obj3;
    obj4.getData(100);
    obj4.printData();
    return 0;
}