#include <iostream>

class Demo
{

    int x = 10;
    std::string str;

public:
    Demo(int x)
    {
        this->x = x;
    }
    void *operator new(size_t size,std::string str,int x,int y)
    {
        std::cout << "In overloaded new" << std::endl;
        // void *ptr = malloc(size);
        void *ptr = ::operator new(size);
        return ptr;
    }
};

int main()
{
    Demo *obj = new("Mangesh",10,20) Demo(100);
    return 0;
}