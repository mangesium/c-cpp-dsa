#include <iostream>
class One
{
    int x = 10;
    int y = 20;

public:
    void getData()
    {
        std::cout << x << std::endl;
    }
};
class Two
{
    friend void getData(One &obj)
    {
        std::cout << obj.x << std::endl;
    }
};

int main()
{
    
    return 0;
}