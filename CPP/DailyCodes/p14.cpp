#include <iostream>

class Demo
{

    int x = 10;
    int y = 20;
    // Demo *obj;

public:
    void fun()
    {
        std::cout << "In-Fun" << std::endl;
    }
    Demo()
    {
        Demo *obj = new Demo(10);
        std::cout << obj->x << std::endl;
        std::cout << obj->y << std::endl;
        std::cout << "No-Args" << std::endl;
        delete obj;
    }
    Demo(int x)
    {
        std::cout << "Args-Constructor" << std::endl;
    }
    Demo(Demo &obj)
    {
        std::cout << "Copy" << std::endl;
    }
    ~Demo()
    {
        std::cout << "In-Destructor" << std::endl;
    }
};

int main()
{
    Demo obj1;
    Demo obj2(obj1);
    return 0;
}