#include<iostream>

class Demo{
    int x=10;
    public:
    void fun(){
        std::cout<<"In instance fun"<<std::endl;
    }
    static void fun(int x){
        std::cout<<"In static fun"<<std::endl;
    }
};
int main(){
    Demo d;
    //Demo::fun();
    d.fun();
    d.fun(1);
    return 0;
}