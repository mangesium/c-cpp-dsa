#include <iostream>

class One
{
    int x = 10;

public:
    One(int x)
    {
        this->x = x;
    }
    friend void accessData(const One& obj)
    {
        std::cout << obj.x << std::endl;
    }
};


int main()
{
    One obj1(100);
    accessData(obj1);
    return 0;
}