#include<iostream>
class Template{
    public:

    template<typename T>
    T add(T x,T y){
        return x+y;
    }
};

int main(){
    Template obj;
    
    std::cout<<obj.add(10,5)<<std::endl;
    std::cout<<obj.add(10.6,5.53)<<std::endl;
}