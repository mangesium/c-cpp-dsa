#include <iostream>
#include <exception>

class BelowAvg : public std::runtime_error
{
public:
    // BelowAvg()       //No-Args Constructor throws error here as std::runtime_error does not have No-Args Constructor
    // {
    // }
    BelowAvg(std::string str) : runtime_error(str)
    {
    }
    // BelowAvg(const BelowAvg &obj)
    // {
    //     std::cout << "Copy" << std::endl;
    // }
};
class AboveAvg : public std::runtime_error
{
public:
    AboveAvg(std::string str) : runtime_error(str)
    {
    }
    // AboveAvg(const AboveAvg &obj)
    // {
    //     std::cout << "Copy" << std::endl;
    // }
};

class Employee
{
    int id = 0;
    double salary;
    std::string name;

public:
    friend std::istream &operator>>(std::istream &in, Employee &obj)
    {
        std::cout << "Enter id : ";
        in >> obj.id;
        std::cout << "Enter name : ";
        in >> obj.name;
        std::cout << "Enter salary : ";
        in >> obj.salary;
        return in;
    }
    friend std::ostream &operator<<(std::ostream &out, const Employee &obj)
    {

        if (obj.salary <= 500000)
            throw BelowAvg("Garib ye tu Bhenchod Gadi khali jaoun mar");
        else if (obj.salary >= 1500000)
            throw AboveAvg("Tu Pan Garib ye Bhenchod pan bara ye");
        else
            std::cout << "Employee id  : " << obj.id << " Employee name  : " << obj.name << " Employee sal : " << obj.salary << std::endl;
        return out;
    }
};
int main()
{
    int size;
    std::cout << "Enter Size of Array : ";
    std::cin >> size;
    Employee arr[size];
    for (int i = 0; i < size; i++)
    {
        std::cout << "Enter Employee Data : " << std::endl;
        std::cin >> arr[i];
    }
    for (int i = 0; i < size; i++)
    {
        try
        {
            std::cout << arr[i] << std::endl;
        }
        catch (BelowAvg &obj)
        {
        }
    }
    return 0;
}