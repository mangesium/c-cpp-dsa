#include <iostream>
class One
{
    int x = 10;

public:
    friend void accessData(const One &obj)
    {
        std::cout << "One x = " << obj.x << std::endl;
    }
};
int main()
{
    One obj;
    accessData(obj);
    return 0;
}