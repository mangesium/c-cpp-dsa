#include <iostream>

class Employee
{
    double sal;
    std::string name;

public:
    Employee(std::string name, double sal)
    {
        this->sal = sal;
        this->name = name;
    }
    template <typename T>
    T& max(T& x, T& y)
    {
       return (x > y) ? x : y;
    }
    friend bool operator>(Employee &obj1, Employee &obj2)
    {
        return obj1.sal > obj2.sal;
    }
    friend std::ostream& operator<<(std::ostream &out, Employee &obj)
    {
        out << obj.name << " = " << obj.sal;

        return out;
    }
};

int main()
{
    Employee obj1("Mangesh", 10000000);
    Employee obj2("Ganesh", 10000000);
    int x=10,y=20;
    std::cout << obj1.max(obj1, obj2) << std::endl;
    // std::cout << obj1.max(10, 20) << std::endl;  //Error
    std::cout << obj1.max(x, y) << std::endl;

    return 0;
}