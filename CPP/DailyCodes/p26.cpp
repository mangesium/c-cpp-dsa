#include <iostream>
class Demo
{
    int y = 2;
    int x = 1;

public:
    Demo()
    {
        std::cout << "Normal constructor" << std::endl;
    }
    Demo(int x)
    {
        std::cout << "Parameter constructor" << std::endl;
        this->x = x;
        std::cout << "x = " << this->x << std::endl;
        std::cout << "y = " << this->y << std::endl;
    }
    int getDatax() const
    {
        return x;
    }
    int getDatay() const
    {
        return y;
    }
    Demo(int x, int y)
    {
        this->x = x;
        this->y = y;
    }
    friend int operator+(const Demo &obj1, const Demo &obj2);
    friend int operator+(int, const Demo &obj2);
};
int operator+(const Demo &obj1, const Demo &obj2)
{
    std::cout << "Demo,Demo" << std::endl;

    return obj1.x + obj2.x;
}
int operator+(int x, const Demo &obj2)
{
    std::cout << "Int , Demo" << std::endl;

    return x + obj2.x;
}
int main()
{
    Demo obj1(10);
    Demo obj2(20);
    // std::cout << obj1 + obj2 << std::endl;
    std::cout << 100.979592 + obj1 << std::endl;
    // std::cout << 100 + obj2 << std::endl;

    // Demo obj3 = (Demo)55;
    // obj3 = (Demo)69;
    // std::cout << "x = " << obj3.getDatax() << std::endl;
    // std::cout << "y = " << obj3.getDatay() << std::endl;
    return 0;
}
