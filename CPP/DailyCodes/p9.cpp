#include <iostream>

class Demo
{
    int x;

public:
    int y;
    Demo()
    {
        std::cout << "In No_args Constructor" << std::endl;
    }
    Demo(int x)
    {
        this->x = x;
        std::cout << "In Args Constructor" << std::endl;
    }
};
int main()
{

    Demo d(10); //Calls Args Constructor
    Demo d1{};  //Calls No_args Constructor
    Demo d2{0}; //Calls Args Constructor
    return 0;
}