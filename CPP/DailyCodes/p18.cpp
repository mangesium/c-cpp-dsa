#include <iostream>

class One
{
    int x = 10;

protected:
    int y = 20;

public:
    void accessData() const
    {
        std::cout << "x = " << x << std::endl;
        std::cout << "y = " << y << std::endl;
    }
    friend void getData(const One &obj);
};

void getData(const One &obj)
{
    obj.accessData();
    std::cout << "x = " << obj.x << std::endl;
    std::cout << "y = " << obj.y << std::endl;
}

int main()
{
    One obj;
    getData(obj);
    return 0;
}

// Friend Function