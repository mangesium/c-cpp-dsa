#include <iostream>

template <typename T>
struct Node
{
    T data;
    Node *prev;
    Node *next;
};
template <typename T>
class DoublyCircularLinkedList
{
    Node<T> *head = NULL;

public:
    Node<T> *createNode();
    void addLast();
    void addFirst();
    void addAtPos(int pos);
    void printLinkedList();
    int countNode();
    void deleteFirst();
    void deleteLast();
    void deleteAtPos(int pos);
    void reverse();
    void implReverse();
};
template <typename T>
Node<T> *DoublyCircularLinkedList<T>::createNode()
{
    Node<T> *newnode = new Node<T>();
    std::cout << "Enter data : ";
    std::cin >> newnode->data;
    newnode->prev = nullptr;
    newnode->next = nullptr;
    return newnode;
}
template <typename T>
int DoublyCircularLinkedList<T>::countNode()
{
    if (head == nullptr)
        return 0;
    Node<T> *temp = head;
    int cnt = 1;
    while (temp->next != head)
    {
        cnt++;
        temp = temp->next;
    }
    return cnt;
}
template <typename T>
void DoublyCircularLinkedList<T>::printLinkedList()
{
    Node<T> *temp = head;
    while (temp->next != head)
    {
        std::cout << "|" << temp->data << "|->";
        temp = temp->next;
    }
    std::cout << "|" << temp->data << "|" << std::endl;
}
template <typename T>
void DoublyCircularLinkedList<T>::addFirst()
{
    if (head == nullptr)
    {
        head = createNode();
        head->next = head;
        head->prev = head;
    }
    else
    {
        Node<T> *node = createNode();
        node->next = head;
        node->prev = head->prev;
        head->prev->next = node;
        head->prev = node;
        head = node;
    }
}
template <typename T>
void DoublyCircularLinkedList<T>::addLast()
{
    if (head == nullptr)
    {
        head = createNode();
        head->next = head;
        head->prev = head;
    }
    else
    {
        Node<T> *node = createNode();
        node->prev = head->prev;
        node->next = head;
        head->prev->next = node;
        head->prev = node;
    }
}
template <typename T>
void DoublyCircularLinkedList<T>::addAtPos(int pos)
{
    if (pos < 1 || pos > countNode() + 1)
    {
        std::cout << "Invalid Position" << std::endl;
    }
    else if (pos == 1)
    {
        addFirst();
    }
    else if (pos == countNode() + 1)
    {
        addLast();
    }
    else
    {
        Node<T> *temp = head;
        Node<T> *node = createNode();
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        node->prev = temp;
        node->next = temp->next;
        temp->next = node;
        node->next->prev = node;
    }
}
template <typename T>
void DoublyCircularLinkedList<T>::deleteFirst()
{
    if (head == nullptr)
    {
        std::cout << "LinkedList is empty" << std::endl;
    }
    else if (countNode() == 1)
    {
        delete head;
        head = nullptr;
    }
    else
    {
        head->next->prev = head->prev;
        head = head->next;
        delete head->prev->next;
        head->prev->next = head;
    }
}
template <typename T>
void DoublyCircularLinkedList<T>::deleteLast()
{
    if (head == nullptr)
    {
        std::cout << "LinkedList is empty" << std::endl;
    }
    else if (head->prev == head)
    {
        delete head;
        head = nullptr;
    }
    else
    {
        head->prev = head->prev->prev;
        delete head->prev->next;
        head->prev->next = head;
    }
}
template <typename T>
void DoublyCircularLinkedList<T>::deleteAtPos(int pos)
{
    if (pos < 1 || pos > countNode())
    {
        std::cout << "Invalid Position" << std::endl;
    }
    else if (pos == 1)
    {
        deleteFirst();
    }
    else if (pos == countNode())
    {
        deleteLast();
    }
    else
    {
        Node<T> *temp = head;
        while (pos - 2)
        {
            temp = temp->next;
            pos--;
        }
        temp->next = temp->next->next;
        delete temp->next->prev;
        temp->next->prev = temp;
    }
}
template <typename T>
void DoublyCircularLinkedList<T>::reverse()
{
    if (head == nullptr)
        return;
    Node<T> *temp1 = head;
    Node<T> *temp2 = head;
    int k = 0, x = 0, cnt = countNode();
    while (cnt / 2 - x)
    {
        k = cnt;
        while (k - 1 - x)
        {
            temp2 = temp2->next;
            k--;
        }
        T swap = temp1->data;
        temp1->data = temp2->data;
        temp2->data = swap;
        temp1 = temp1->next;
        temp2 = head;
        x++;
    }
}
template <typename T>
void DoublyCircularLinkedList<T>::implReverse()
{
    if (head == nullptr)
        return;
    Node<T> *temp1 = head;
    Node<T> *temp2 = head;
    while (1)
    {
        temp2 = temp1->prev;
        temp1->prev = temp1->next;
        temp1->next = temp2;
        temp1 = temp1->prev;
        if (temp1 == head)
            break;
    }
    head = head->next;
}

int main()
{
    int exit = 1;
    do
    {
        std::cout << "1.Integer" << std::endl;
        std::cout << "2.Float" << std::endl;
        std::cout << "3.Character" << std::endl;
        std::cout << "4.Double" << std::endl;
        std::cout << "5.String" << std::endl;
        std::cout << "6.exit" << std::endl;
        std::cout << "Choose data type = ";
        int choice;
        std::cin >> choice;
        switch (choice)
        {
        case (1):
        {
            DoublyCircularLinkedList<int> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (2):
        {
            DoublyCircularLinkedList<float> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (3):
        {
            DoublyCircularLinkedList<char> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (4):
        {
            DoublyCircularLinkedList<double> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (5):
        {
            DoublyCircularLinkedList<std::string> obj;
            int k = 1;
            do
            {
                std::cout << "1.Add first" << std::endl;
                std::cout << "2.Add Last" << std::endl;
                std::cout << "3.Add at Position" << std::endl;
                std::cout << "4.print LinkedList" << std::endl;
                std::cout << "5.LinkedList count" << std::endl;
                std::cout << "6.Delete first" << std::endl;
                std::cout << "7.Delete  Last" << std::endl;
                std::cout << "8.Delete at Position" << std::endl;
                std::cout << "9.Reverse" << std::endl;
                std::cout << "10.Implace Reverse" << std::endl;
                std::cout << "11.Exit" << std::endl;
                std::cout << "Enter Your Choice : ";
                int ch;
                std::cin >> ch;
                switch (ch)
                {
                case (1):
                {
                    obj.addFirst();
                    break;
                }
                case (2):
                {
                    obj.addLast();
                    break;
                }
                case (3):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.addAtPos(pos);
                    break;
                }
                case (4):
                {
                    obj.printLinkedList();
                    break;
                }
                case (5):
                {
                    std::cout << "No of nodes in linked list = " << obj.countNode() << std::endl;
                    break;
                }
                case (6):
                {
                    obj.deleteFirst();
                    break;
                }
                case (7):
                {
                    obj.deleteLast();
                    break;
                }
                case (8):
                {
                    std::cout << "Enter position : ";
                    int pos;
                    std::cin >> pos;
                    obj.deleteAtPos(pos);
                    break;
                }
                case (9):
                {
                    obj.reverse();
                    break;
                }
                case (10):
                {
                    obj.implReverse();
                    break;
                }
                case (11):
                {
                    k = 0;
                    break;
                }
                }
                std::cout << std::endl;
            } while (k == 1);
            break;
        }
        case (6):
        {
            exit = 0;
            break;
        }
        }

    } while (exit == 1);
    return 0;
}