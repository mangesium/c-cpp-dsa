#include <iostream>

template <typename T>
class Template
{

    T data;

public:
    Template(T data)
    {
        this->data = data;
        std::cout << "In constructor" << std::endl;
    }
    T getData()
    {
        return this->data;
    }
};

int main()
{
    Template<int> obj1(10);
    Template<std::string> *obj2 = new Template<std::string>("The Mangesh Manjare");
    Template<double> obj3(10.5);
    std::cout << sizeof(obj1) << " = " << obj1.getData() << std::endl;
    std::cout << sizeof(obj2) << " = " << obj2->getData() << std::endl;
    std::cout << sizeof(obj3) << " = " << obj3.getData() << std::endl;
    return 0;
}