#include <iostream>
class Employee
{
    std::string eName = "Mangesh";
    int empId = 10;

public:
    Employee()
    {
        std::cout << "Emp Constructor" << std::endl;
    }
    void getInfo()
    {
        std::cout << eName << " " << empId << std::endl;
    }
    void setEname(std::string eName)
    {
        this->eName = eName;
    }
    std::string getEname()
    {
        return eName;
    }
    void setEmpId(int empId)
    {
        this->empId = empId;
    }
    int getEmpId()
    {
        return empId;
    }
    ~Employee()
    {
        std::cout << "Emp Destructor" << std::endl;
    }
};
class Company
{
    std::string cName = "Manjare Associates";
    int strEmp = 10000;
    Employee obj;

public:
    Company()
    {
        std::cout << "Company Constructor" << std::endl;
    }
    Company(std::string cName, int strEmp, std::string eName, int empId)
    {
        this->cName = cName;
        this->strEmp = strEmp;
        this->obj.setEname(eName);
        this->obj.setEmpId(empId);
    }
    void getInfo()
    {
        std::cout << cName << " " << strEmp << std::endl;
        std::cout << obj.getEname() << " " << obj.getEmpId() << std::endl;
    }
    ~Company()
    {
        std::cout << "company Destructor" << std::endl;
    }
};
int main()
{
    Company obj("Veritas",10000,"Mangesh",369);
    obj.getInfo();
    return 0;
}