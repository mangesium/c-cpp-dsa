#include <iostream>

class Parent
{
    int x = 10, y = 20, z = 30;

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    Parent(int x, int y)
    {
        std::cout << "Parent Constructor para" << std::endl;
        this->x = x;
        this->y = y;
    }
    ~Parent()
    {
        std::cout << "Parent Destructor" << std::endl;
    }
    void getData()
    {
        std::cout << "X = " << x << " Y = " << y << " z = " << z << std::endl;
    }
    // friend void *operator new(size_t size)
    // {
    //     std::cout << "Parent new " << std::endl;
    //     return malloc(size);
    // }
    void operator delete(void *ptr)
    {
        std::cout << "Parent Delete" << std::endl;
        free(ptr);
    }
};
class Child : public Parent
{
    int a = 69, b = 79;

public:
    Child() : Parent(49, 369)
    {
        std::cout << "Child Constructor" << std::endl;
    }
    ~Child()
    {
        std::cout << "Child Destructor" << std::endl;
    }
    void printData()
    {
        std::cout << "a = " << a << " b = " << b << std::endl;
    }
    friend void *operator new(size_t size)
    {
        std::cout << "Child new " << std::endl;
        return malloc(size);
    }
    void operator delete(void *ptr)
    {
        std::cout << "Child Delete" << std::endl;
        free(ptr);
    }
};

int main()
{
    Child obj2;
    obj2.getData();
    obj2.printData();
    return 0;
}