#include <iostream>

class Demo
{

public:
    static int x;
    int y = 20;
};
int Demo::x = 10; // Static varable defination
int main()
{
    Demo d;
    std::cout << "y = " << d.y << std::endl;
    d.y = 50;
    std::cout << "y = " << d.y << std::endl;
    std::cout << "x = " << d.x << std::endl;
    d.x = 50;
    std::cout << "x = " << d.x << std::endl;
    return 0;
}