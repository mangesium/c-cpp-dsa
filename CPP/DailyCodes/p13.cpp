#include <iostream>
class Demo
{

    int x = 10;
    int y = 20;

public:
    Demo()
    {
        std::cout << "No-Args" << std::endl;
    }
    Demo(int x, int y)
    {
        this->x = x;
        this->y = y;
        std::cout << "Para" << std::endl;
    }
    Demo(Demo &obj){
        std::cout << "Copy" << std::endl;
    }
    Demo& fun(Demo obj)
    {
        std::cout << "Before change"<< std::endl;
        std::cout << obj.x << std::endl;
        std::cout << obj.y << std::endl;
        obj.x = 1000;
        obj.y = 2000;
        std::cout << "After change"<< std::endl;
        std::cout << obj.x << std::endl;
        std::cout << obj.y << std::endl;
        return obj;
    }
    void access()
    {
        std::cout << x << std::endl;
        std::cout << y << std::endl;
    }
};

int main()
{

    Demo obj1;
    obj1.access();
    Demo obj2(100, 200);
    obj2.access();
    Demo obj4 = obj2.fun(obj1);
    Demo obj3(obj2);
    obj3.access();
        return 0;
}