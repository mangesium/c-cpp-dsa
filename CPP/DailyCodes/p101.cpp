#include <iostream>
class IndexException{
    std::string exce = "Default";

public:
    IndexException(std::string exce)
    {
        this->exce = exce;
    }
    IndexException(const IndexException &obj)
    {
        std::cout << "Copy Constructor" << std::endl;
    }
    std::string getException()
    {
        return exce;
    }
};
class Demo
{
    int arr[5] = {1, 2, 3, 4, 5};

public:
    int getIndex()
    {
        return (sizeof(arr) / sizeof(int));
    }
    int operator[](int x)
    {
        if (x < 0 || x >= getIndex())
            throw IndexException("Ye Vedhya ");

        return arr[x];
    }
};
int main()
{
    Demo obj;
    try
    {
        std::cout << obj[-4] << std::endl;
    }
    catch (IndexException io)            //Here following line will create a copy object and prints default value
    {
        std::cout << "Exception : " << io.getException() << std::endl;
    }
    catch (IndexException &io)
    {
        std::cout << "Exception : " << io.getException() << std::endl;
    }
    return 0;
}