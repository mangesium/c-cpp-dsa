#include <iostream>
class Demo
{
    int x = 10;
    int y = 20;

public:
    Demo(int x, int y)
    {
        this->x = x;
        this->y = y;
    }
    int operator+(const Demo &obj)
    {

        return x + obj.y;
    }
};

int main()
{
    Demo obj1(100, 20);
    Demo obj2(150, 20);
    std::cout << obj1 + obj2 << std::endl;
    return 0;
}
// Operator Overloading using Member function