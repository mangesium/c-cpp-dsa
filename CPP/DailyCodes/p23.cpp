#include <iostream>
class Demo
{
    int x = 10;
    int y = 20;

public:
    Demo(int x, int y)
    {
        this->x = x;
        this->y = y;
    }
    int getDatax() const
    {
        return x;
    }
    int getDatay() const
    {
        return y;
    }
};
int operator+(const Demo &obj1, const Demo &obj2)
{

    return obj1.getDatax() + obj2.getDatay();
}
int main()
{
    Demo obj1(10, 20);
    Demo obj2(10, 20);
    std::cout << obj1 + obj2 << std::endl;
    return 0;
}
// Operator Overloading using Normal function