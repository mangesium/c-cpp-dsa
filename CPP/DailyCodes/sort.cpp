#include <iostream>

class BubbleSort
{
public:
    static void sort(int *arr, int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size - 1 - i; j++)
            {
                if (arr[j] > arr[j + 1])
                {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
};
class SelectionSort
{
public:
    static void sort(int *arr, int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = i + 1; j < size; j++)
            {
                if (arr[i] > arr[j])
                {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }
};
class InsertionSort
{
public:
    static void sort(int *arr, int size)
    {
        for (int i = 1; i < size; i++)
        {
            int j = 0, temp = arr[i];
            for (j = i - 1; j >= 0 && arr[j] > temp; j--)
            {
                arr[j + 1] = arr[j];
            }
            arr[j + 1] = temp;
        }
    }
};
class MergeSort
{
public:
    static void part(int *arr, int start, int mid, int end)
    {
        int i = start, j = mid + 1, k = 0;
        int A[end - start + 1];
        while (i <= mid && j <= end)
        {
            if (arr[i] < arr[j])
            {
                A[k] = arr[i];
                i++;
            }
            else
            {
                A[k] = arr[j];
                j++;
            }
            k++;
        }
        while (j <= end)
        {
            A[k] = arr[j];
            k++;
            j++;
        }
        while (i <= mid)
        {
            A[k] = arr[i];
            k++;
            i++;
        }
        for (int p = start; p <= end; p++)
        {
            arr[p] = A[p - start];
        }
    }
    static void sort(int *arr, int start, int end)
    {
        if (start < end)
        {
            int mid = start + (end - start) / 2;
            sort(arr, start, mid);
            sort(arr, mid + 1, end);
            part(arr, start, mid, end);
        }
    }
};
class Quick1
{
public:
    static void swap(int *x, int *y)
    {
        int temp = *x;
        *x = *y;
        *y = temp;
    }
    static int part(int *arr, int start, int end)
    {
        int idx = start;
        int pivot = arr[end];
        for (int i = start; i <= end; i++)
        {
            if (arr[i] < pivot)
            {
                swap(&arr[i], &arr[idx]);
                idx++;
            }
        }
        swap(&arr[idx], &arr[end]);
        return idx;
    }
    static void sort(int *arr, int start, int end)
    {
        if (start < end)
        {
            int pivot = part(arr, start, end);
            sort(arr, start, pivot - 1);
            sort(arr, pivot + 1, end);
        }
    }
};
class Quick2
{
public:
    static void swap(int *x, int *y)
    {
        int temp = *x;
        *x = *y;
        *y = temp;
    }
    static int part(int *arr, int start, int end)
    {
        int idx = start, pivot = arr[start];
        int i = start - 1, j = end + 1;
        while (1)
        {
            do
            {
                i++;
            } while (arr[i] < pivot);

            do
            {
                j--;
            } while (arr[j] > pivot);
            if (i >= j)
                return j;
            swap(&arr[i], &arr[j]);
        }
        return j;
    }
    static void sort(int *arr, int start, int end)
    {
        if (start < end)
        {
            int pivot = part(arr, start, end);
            sort(arr, start, pivot);
            sort(arr, pivot + 1, end);
        }
    }
};
class Quick3
{
public:
    static void swap(int *x, int *y)
    {
        int temp = *x;
        *x = *y;
        *y = temp;
    }
    static int part(int *arr, int start, int end)
    {
        int idx = start, pivot = arr[end];
        int A[end - start + 1], k = 0;
        for (int i = start; i < end; i++)
        {
            if (arr[i] < pivot)
            {
                A[k] = arr[i];
                k++;
            }
        }
        int ret = k + start;
        A[k] = pivot;
        k++;
        for (int i = start; i < end; i++)
        {
            if (arr[i] >= pivot)
            {
                A[k] = arr[i];
                k++;
            }
        }
        for (int i = start; i <= end; i++)
        {
            arr[i] = A[i - start];
        }
        return ret;
    }
    static void sort(int *arr, int start, int end)
    {
        if (start < end)
        {
            int pivot = part(arr, start, end);
            sort(arr, start, pivot - 1);
            sort(arr, pivot + 1, end);
        }
    }
};
class Counting
{
public:
    static void sort(int *arr, int size)
    {
        int max = arr[0];
        for (int i = 1; i < size; i++)
        {
            if (max < arr[i])
            {
                max = arr[i];
            }
        }
        // int count[max + 1]={0};  //It works when size of array is fixed eg. count[10]
        int count[max + 1];
        for (int i = 0; i <= max; i++)
        {
            count[i] = 0;
        }
        for (int i = 0; i < size; i++)
        {
            count[arr[i]]++;
        }

        // std::cout << "count of num :";
        // for (int i = 0; i <= max; i++)
        // {
        //     std::cout << count[i] << " ";
        // }
        // std::cout << std::endl;
        for (int i = 1; i <= max; i++)
        {
            count[i] += count[i - 1];
        }
        // std::cout << "Prefix Sum :";
        // for (int i = 0; i <= max; i++)
        // {
        //     std::cout << count[i] << " ";
        // }
        // std::cout << std::endl;
        int output[size];
        for (int i = size - 1; i >= 0; i--)
        {
            output[count[arr[i]] - 1] = arr[i];
            // std::cout << "count[arr[i]] - 1 = " << count[arr[i]] - 1 << std::endl;
            count[arr[i]]--;
        }
        for (int i = 0; i < size; i++)
        {
            arr[i] = output[i];
        }
    }
};
class Radix
{
public:
    static void counting(int *arr, int size, int pos)
    {
        int count[10] = {0};
        for (int i = 0; i < size; i++)
        {
            count[(arr[i] / pos) % 10]++;
        }
        for (int i = 1; i < 10; i++)
        {
            count[i] += count[i - 1];
        }
        int output[size];
        for (int i = size - 1; i >= 0; i--)
        {
            output[count[(arr[i] / pos) % 10] - 1] = arr[i];
            count[(arr[i] / pos) % 10]--;
        }
        for (int i = 0; i < size; i++)
            arr[i] = output[i];
    }
    static void sort(int *arr, int size)
    {
        int max = arr[0];
        for (int i = 1; i < size; i++)
        {
            if (arr[i] > max)
                max = arr[i];
        }

        for (int pos = 1; max / pos > 0; pos *= 10)
        {
            counting(arr, size, pos);
        }
    }
};
class TimSort
{

public:
    const static int MIN_RUN = 3;
    static void insertionSort(int arr[], int left, int right)
    {
        for (int i = left + 1; i <= right; i++)
        {
            int key = arr[i];
            int j = i - 1;
            while (j >= left && arr[j] > key)
            {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = key;
        }
    }
    static void merge(int arr[], int left, int mid, int right)
    {
        int len1 = mid - left + 1;
        int len2 = right - mid;
        int left_arr[len1], right_arr[len2];

        for (int i = 0; i < len1; i++)
            left_arr[i] = arr[left + i];
        for (int j = 0; j < len2; j++)
            right_arr[j] = arr[mid + 1 + j];

        int i = 0, j = 0, k = left;

        while (i < len1 && j < len2)
        {
            if (left_arr[i] <= right_arr[j])
            {
                arr[k] = left_arr[i];
                i++;
            }
            else
            {
                arr[k] = right_arr[j];
                j++;
            }
            k++;
        }

        while (i < len1)
        {
            arr[k] = left_arr[i];
            i++;
            k++;
        }

        while (j < len2)
        {
            arr[k] = right_arr[j];
            j++;
            k++;
        }
    }

    static void sort(int *arr, int n)
    {

        for (int i = 0; i < n; i += MIN_RUN)
        {
            TimSort::insertionSort(arr, i, std::min(i + MIN_RUN - 1, n - 1));
        }
        for (int size = MIN_RUN; size < n; size *= 2)
    {
        for (int left = 0; left < n; left += 2 * size)
        {
            std::cout << "In merge inner for" << std::endl;
            int mid = left + size - 1;
            int right = std::min(left + 2 * size - 1, n - 1);
            merge(arr, left, mid, right);
        }
    }
    }
};
int main()
{
    int size = 0;
    std::cout << "Enter size of array : ";
    std::cin >> size;
    int *arr = new int[size];
    for (int i = 0; i < size; i++)
    {
        std::cout << "Enter elsments in array : ";
        std::cin >> arr[i];
    }
    std::cout << "Before Sorting : ";
    for (int i = 0; i < size; i++)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
    // BubbleSort::sort(arr, size);
    // SelectionSort::sort(arr, size);
    // InsertionSort::sort(arr, size);
    // MergeSort::sort(arr, 0, size - 1);
    // Quick1::sort(arr, 0, size - 1);
    // Quick2::sort(arr, 0, size - 1);
    //  Quick3::sort(arr, 0, size - 1);
    // Counting::sort(arr, size);
    // Radix::sort(arr, size);
    TimSort::sort(arr,size);
    std::cout << "After Sorting : ";
    for (int i = 0; i < size; i++)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}