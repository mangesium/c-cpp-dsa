#include <iostream>

class Demo
{
    int x = 1000;
    static int y;

public:
    void fun()
    {
        int a = 10;
        static int b = 20;
        std::cout << x << y << a << b << std::endl;
    }
    static void gun()
    {
        int a = 10;
        static int b = 20;
        Demo *d = new Demo();
        std::cout << (*d).x << y << a << b << std::endl;
    }
};
int Demo::y = 20;
int main()
{
    Demo::gun();
    Demo *d = new Demo();
    d->fun();
    return 0;
}