#include <iostream>

template <typename T, typename U>
auto min(T x, U y)
{
    // if (x < y)
    //     return x;
    // else
    //     return y;       //error: inconsistent deduction for auto return type: 'char' and then 'int'

    return (x < y) ? x : y;
}

int main()
{
    std::cout << min('A', 10) << std::endl;
    return 0;
}