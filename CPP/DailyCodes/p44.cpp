#include <iostream>

class Parent
{
    int x = 10, y = 20;

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    ~Parent()
    {
        std::cout << "Parent Destructor" << std::endl;
    }
    void getData()
    {
        std::cout << "X = " << x << " Y = " << y << std::endl;
    }
    // friend void *operator new(size_t size)
    // {
    //     std::cout << "Parent new " << std::endl;
    //     return malloc(size);
    // }
    void operator delete(void *ptr)
    {
        std::cout << "Parent Delete" << std::endl;
        free(ptr);
    }
};
class Child : public Parent
{
    int x = 1090, y = 200;

public:
    Child()
    {
        std::cout << "Child Constructor" << std::endl;
    }
    ~Child()
    {
        std::cout << "Child Destructor" << std::endl;
    }
    void getData()
    {
        std::cout << "X = " << x << " Y = " << y << std::endl;
    }
    friend void *operator new(size_t size)
    {
        std::cout << "Child new " << std::endl;
        return malloc(size);
    }
    void operator delete(void *ptr)
    {
        std::cout << "Child Delete" << std::endl;
        free(ptr);
    }
};
// void *operator new(size_t size)
// {
//     std::cout << "Child new " << std::endl;
//     return malloc(size);
// }

int main()
{
    Child obj;
    Parent *obj1 = new Child();
    obj1->getData();
    delete obj1;
    // Child *obj2 = new Child();
    // obj2->getData();
    // delete obj2;
    return 0;
}