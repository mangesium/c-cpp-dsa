#include <iostream>

class Demo
{
    int x = 10;
    int arr[5] = {1, 2, 3, 4, 5};

public:
    void getArray()
    {
        for (int i = 0; i < (sizeof(arr) / sizeof(int)); i++)
        {
            std::cout << arr[i] << " ";
        }
        std::cout << std::endl;
    }
    void getX()
    {
        std::cout << "X = " << x << std::endl;
    }
    int &operator[](int index)
    {
        return arr[index];
        // return x;
    }
    int operator()(int x, int y)
    {

        return x + y;
    }
};

int main()
{
    Demo obj;
    obj[1] = 369;
    obj.getX();
    obj.getArray();
    int res = obj(1000, 333333);
    std::cout << "Res = " << res << std::endl;

    return 0;
}