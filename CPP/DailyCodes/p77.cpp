#include <iostream>
class Parent
{

public:
    void getData()
    {
    }
};
class Child1 : public Parent
{

public:
    void getData()
    {
        std::cout << "child1 getData" << std::endl;
    }
};
class Child2 : public Parent
{

public:
    void getData()
    {
        std::cout << "child2 getData" << std::endl;
    }
};
// Diamond Problem In CPP
class Child : public Child1, public Child2
{
};
int main()
{

    Child obj;
    // obj.getData();    //Error
    return 0;
}
