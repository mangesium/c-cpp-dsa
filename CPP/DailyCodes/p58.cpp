#include <iostream>

class Demo
{
    int x = 10;

public:
    Demo()
    {
        std::cout << "No arg constructor" << std::endl;
    }
    Demo(int x)
    {
        this->x = x;
        std::cout << "arg constructor" << std::endl;
    }
    Demo(Demo &obj)
    {
        std::cout << "Copy constructor" << std::endl;
    }
    void data(Demo& obj)
    {
        std::cout << "this x = " << x << std::endl;
        std::cout << "Obj x = " << obj.x << std::endl;
    }
};

int main()
{
    Demo obj1(100);
    Demo obj2(200);
    obj1.data(obj2);
    return 0;
}