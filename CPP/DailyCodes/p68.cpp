#include <iostream>
class Parent
{

public:
    virtual void getData(int x)const
    {
        std::cout << "Parent getData" << std::endl;
    }
};
class Child : public Parent
{

public:
    void getData(int x)const override
    {
        std::cout << "Child getData" << std::endl;
    }
};
int main()
{
    Parent *obj = new Child();
    obj->getData(0);
    return 0;
}