#include <iostream>

template <typename T>

T min(T x, T y)
{
    return (x < y) ? x : y;
}

int main()
{
    std::cout << min('A', 'B') << std::endl;
    std::cout << min(5, 10) << std::endl;
    std::cout << min(5.5f, 10.5f) << std::endl;
    std::cout << min(5.5, 10.5) << std::endl;
    return 0;
}