// Early Binding
#include <iostream>

void add(int x, int y)
{
    printf("%d", x + y);
}
void sub(int x, int y)
{
    printf("%d", x - y);
}
void mult(int x, int y)
{
    printf("%d", x * y);
}
int main()
{
    printf("1.add\n");
    printf("2.sub\n");
    printf("3.multi\n");
    printf("Enter Choice :");
    int ch;
    std::cin >> ch;
    switch (ch)
    {
    case 1:
        add(10, 20); // Here compiler knows which function is going to call for any input while compile time hence early binding
        break;
    case 2:
        sub(100, 20);
        break;
    case 3:
        mult(10, 20);
        break;
    }
    return 0;
}