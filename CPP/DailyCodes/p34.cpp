#include <iostream>
class Demo
{
    int x = 100;
    // friend std::ostream &operator<<(std::ostream &out, const Demo &obj)
    // {
    //     out << "Demo X = " << obj.x;
    //     return out;
    // }
    public:
    int getX() const
    {
        return x;
    }
};
std::ostream &operator<<(std::ostream &out, const Demo &obj)
{
    out << "Demo X = " << obj.getX();
    return out;
}
int main()
{

    Demo obj1;
    std::cout << obj1 << std::endl;
    return 0;
}