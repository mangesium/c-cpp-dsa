#include <iostream>

class Parent
{
    int x = 10, y = 20, z = 30;

public:
    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    ~Parent()
    {
        std::cout << "Parent Destructor" << std::endl;
    }
    void getData()
    {
        std::cout << "X = " << x << " Y = " << y << std::endl;
    }
    // friend void *operator new(size_t size)
    // {
    //     std::cout << "Parent new " << std::endl;
    //     return malloc(size);
    // }
    void operator delete(void *ptr)
    {
        std::cout << "Parent Delete" << std::endl;
        free(ptr);
    }
};
class Child : public Parent
{
    int x = 1090, y = 200;

public:
    Child()
    {
        std::cout << "Child Constructor" << std::endl;
    }
    ~Child()
    {
        std::cout << "Child Destructor" << std::endl;
    }
    void getData()
    {
        std::cout << "X = " << x << " Y = " << y << std::endl;
    }
    friend void *operator new(size_t size)
    {
        std::cout << "Child new " << std::endl;
        return malloc(size);
    }
    void operator delete(void *ptr)
    {
        std::cout << "Child Delete" << std::endl;
        free(ptr);
    }
};

int main()
{
    // Parent obj1;
    // Child obj2;
    // Child* p = new Child();
    // delete p;
    std::cout << "Parent Size = " << sizeof(Parent) << std::endl;
    std::cout << "Child Size = " << sizeof(Child) << std::endl;
    // IN Child Private variables of parent are there as parent constructor call but they are not accessible to child class
    return 0;
}