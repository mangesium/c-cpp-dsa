#include <iostream>
template <class T>
class Stack
{
    int flag = 0;
    int top = -1;
    int size = 0;

public:
    Stack(int size)
    {
        this->size = size;
    }
    bool isFull();
    bool isEmpty();
    void push(T *arr, T num);
    int pop(T *arr);
    int front(T *arr);
    void printStack(T *arr);
};
template <typename T>
bool Stack<T>::isFull()
{
    if (top == size - 1)
        return true;
    return false;
}
template <typename T>
bool Stack<T>::isEmpty()
{
    if (top == -1)
    {
        return true;
    }
    return false;
}
template <typename T>
void Stack<T>::push(T *arr, T num)
{
    arr[++top] = num;
}
template <typename T>
int Stack<T>::pop(T *arr)
{
    T ret = arr[top];
    --top;
    return ret;
}
template <typename T>
int Stack<T>::front(T *arr)
{
    return arr[top];
}
template <typename T>
void Stack<T>::printStack(T *arr)
{
    for (int i = top; i >= 0; i--)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}
int main()
{
    int size;
    std::cout << "Enter size of stack : ";
    std::cin >> size;
    int stack[size];
    Stack<int> obj(size);
    int k = 1;
    do
    {
        std::cout << "1.Push" << std::endl;
        std::cout << "2.Pop" << std::endl;
        std::cout << "3.Front" << std::endl;
        std::cout << "4.Print Stack" << std::endl;
        std::cout << "5.isFull" << std::endl;
        std::cout << "6.isEmpty" << std::endl;
        std::cout << "7.Exit" << std::endl;
        int ch;
        std::cout << "Enter choice : " << std::endl;
        std::cin >> ch;
        switch (ch)
        {
        case 1:
        {
            if (obj.isFull())
            {
                std::cout << "Stack Overflow!!!" << std::endl;
            }
            else
            {
                int num;
                std::cout << "Enter data : " << std::endl;
                std::cin >> num;
                obj.push(stack, num);
            }
        }
        break;
        case 2:
        {
            if (obj.isEmpty())
            {
                std::cout << "Stack Underflow!!!" << std::endl;
            }
            else
            {
                std::cout << "Popped Element : " << obj.pop(stack) << std::endl;
            }
        }
        break;
        case 3:
        {
            if (obj.isEmpty())
            {
                std::cout << "Stack Underflow!!!" << std::endl;
            }
            else
            {
                std::cout << "Front Element : " << obj.front(stack) << std::endl;
            }
        }
        break;
        case 4:
        {
            if (obj.isEmpty())
            {
                std::cout << "Stack Underflow!!!" << std::endl;
            }
            else
            {
                obj.printStack(stack);
            }
        }
        break;
        case 5:
        {
            if (obj.isFull())
            {
                std::cout << "True" << std::endl;
            }
            else
            {
                std::cout << "False" << std::endl;
            }
        }
        break;
        case 6:
        {
            if (obj.isEmpty())
            {
                std::cout << "True" << std::endl;
            }
            else
            {
                std::cout << "False" << std::endl;
            }
        }
        break;
        case 7:
        {
            k = 0;
        }
        break;
        default:
            break;
        }

    } while (k == 1);

    return 0;
}