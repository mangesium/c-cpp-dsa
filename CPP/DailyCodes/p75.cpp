#include <iostream>

class Demo
{
    Demo(){

    }
public:
    static Demo *obj;
    static Demo *getObject()
    {
        return obj;
    }
};
Demo *Demo::obj = new Demo();

/*Singleton Design pattern in CPP*/
int main()
{
    Demo *obj1 = Demo::getObject();
    Demo *obj2 = Demo::getObject();
    Demo *obj3 = Demo::getObject();

    std::cout << obj1 << std::endl;
    std::cout << obj2 << std::endl;
    std::cout << obj3 << std::endl;
    return 0;
}