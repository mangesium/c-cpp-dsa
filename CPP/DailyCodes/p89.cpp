#include <iostream>

template <typename T>
class Template
{

    T data;

public:
    Template()
    {
        std::cout << "Template Constructor" << std::endl;
    }
};

template <>
class Template<int> // Specilized template following class will be provided by compiler
{

    int data;

public:
    Template()
    {
        std::cout << "Int Constructor" << std::endl;
    }
};

int main()
{
    Template<double> obj1;
    Template<int> obj2;
    Template<float> obj3;
    return 0;
}