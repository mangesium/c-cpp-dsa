#include <iostream>

class Demo
{
    int x = 10;

    friend std::istream &operator>>(std::istream &in, Demo &obj)
    {
        in >> obj.x;
        return in;
    }

public:
    void printData()
    {
        std::cout << "Demo X = " << x << std::endl;
    }
};

int main()
{

    Demo obj1;
    std::cout << "Enter Data : ";
    std::cin >> obj1;
    obj1.printData();
    return 0;
}