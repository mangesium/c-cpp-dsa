#include <iostream>
template <typename T>
void fun(T x)
{
    int size;
    std::cout << "Enter Size : ";
    std::cin >> size;
    T arr[size];
    for (int i = 0; i < size; i++)
    {
        std::cout << "Enter element : ";
        std::cin >> arr[i];
    }

    for (int i = 0; i < size; i++)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}
int main()
{
    fun(10.7);
}