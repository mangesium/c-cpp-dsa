#include <iostream>
class Parent
{

    int x = 10;

protected:
    int y = 20;

public:
    int z = 30;

    Parent()
    {
        std::cout << "Parent Constructor" << std::endl;
    }
    Parent(Parent* obj)
    {
        std::cout << "Parent copy Constructor" << std::endl;
    }
    void getData()
    {
        std::cout << "Parent x = " << x << "Parent y = " << y << "Child z = " << z << std::endl;
    }
};
class Child : public Parent
{
    int x = 10;

protected:
    int y = 20;

public:
    int z = 30;

public:
    Child()
    {
        std::cout << "Child Constructor" << std::endl;
    }
    void getData()
    {
        std::cout << "Child x = " << x << "Child y = " << y << "Child z = " << z << std::endl;
    }
};
int main()
{
    Child *obj = new Child();
    obj->Parent::getData();
    (Parent(obj)).getData();
    (static_cast<Parent*>(obj))->getData();
    return 0;
}

//Type Casting of child pointer into parent pointer 