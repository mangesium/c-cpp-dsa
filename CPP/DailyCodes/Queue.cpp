#include <iostream>

template <class T>
class Queue
{
    int front = -1, rear = -1, size = 0, flag = 0;

public:
    Queue(int size)
    {
        this->size = size;
    }
    T enqueue(T *queue, T val);
    T dequeue(T *queue);
    T frontt(T *queue);
    void printQueue(T *queue);
    bool isFull();
    bool isEmpty();
};
template <typename T>
bool Queue<T>::isEmpty()
{
    if (front == -1)
        return true;
    return false;
}
template <typename T>
bool Queue<T>::isFull()
{
    if (rear == size - 1)
        return true;
    return false;
}
template <typename T>
void Queue<T>::printQueue(T *arr)
{
    for (int i = front; i <= rear; i++)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}
template <typename T>
T Queue<T>::frontt(T *arr)
{
    int ret = -1;
    if (isEmpty())
    {
        std::cout << "Queue is Empty" << std::endl;
        flag = 0;
    }
    else
    {
        ret = arr[front];
        flag = 1;
    }
    return ret;
}
template <typename T>
T Queue<T>::enqueue(T *arr, T val)
{
    if (isFull())
    {
        flag = 0;
        std::cout << "Queue is full" << std::endl;
    }
    else
    {
        flag = 1;
        if (isEmpty())
        {
            front++;
        }
        arr[++rear] = val;
    }
    return val;
}
template <typename T>
T Queue<T>::dequeue(T *queue)
{
    T ret;
    if (isEmpty())
    {
        std::cout << "Queue is Empty" << std::endl;
        flag = 0;
    }
    else
    {
        flag = 1;
        ret = queue[front];
        if (front == rear)
        {
            front = rear = -1;
        }
        else
        {
            front++;
        }
    }
    return ret;
}
int main()
{
    int size;
    std::cout << "Enter size of queue : ";
    std::cin >> size;
    int queue[size];
    Queue<int> obj(size);
    int k = 1;
    do
    {
        std::cout << "1.Enqueue" << std::endl;
        std::cout << "2.Dequeue" << std::endl;
        std::cout << "3.Front" << std::endl;
        std::cout << "4.Print Queue" << std::endl;
        std::cout << "5.Exit" << std::endl;
        std::cout << "Enter choice : ";
        int ch;
        std::cin >> ch;
        switch (ch)
        {
        case 1:
        {
            if (obj.isFull())
            {
                std::cout << "Queue Is Full" << std::endl;
            }
            else
            {
                std::cout << "Enter data : ";
                int num;
                std::cin >> num;
                std::cout << obj.enqueue(queue, num) << " is Enqueued" << std::endl;
            }
        }
        break;
        case 2:
        {
            if (obj.isEmpty())
            {
                std::cout << "Queue Is Empty" << std::endl;
            }
            else
            {
                std::cout << obj.dequeue(queue) << " is Dequeued" << std::endl;
            }
        }
        break;
        case 3:
        {
            if (obj.isEmpty())
            {
                std::cout << "Queue Is Empty" << std::endl;
            }
            else
            {
                std::cout << obj.frontt(queue) << " is at front" << std::endl;
            }
        }
        break;
        case 4:
        {
            if (obj.isEmpty())
            {
                std::cout << "Queue Is Empty" << std::endl;
            }
            else
            {
                obj.printQueue(queue);
            }
        }
        break;
        case 5:
        {
            k = 0;
        }
        break;
        default:
            break;
        }
    } while (k == 1);

    return 0;
}