#include <iostream>

int z = 30;
int main()
{
    int x = 10, y = 20;
    auto obj = [=]()mutable -> int
    {   
        x++;
        return x * y * z;
    };
    std::cout << obj() << std::endl;
    return 0;
}