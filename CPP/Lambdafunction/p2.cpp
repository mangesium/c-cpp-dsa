#include <iostream>
#include <functional>
int main()
{
    std::function<int(int, int)> obj = [](int x, int y)
    {
        return x * y;
    };
    std::cout << obj(10, 20) << std::endl;
}