#include <iostream>
int main()
{
    auto obj = [](int x, int y)
    {
        std::cout << "In lambda function" << std::endl;
        return x * y;
    };
    std::cout << sizeof(obj) << std::endl;
    std::cout << obj(10,33) << std::endl;

    return 0;
}