#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <stack>
#include <queue>
#include <set>
using namespace std;
void explainVector()
{
    vector<int> v;
    v.push_back(1);
    v.emplace_back(2);
    v.emplace_back(3);
    v.emplace_back(4);
    v.emplace_back(5);
    v.emplace_back(6);
    v.emplace_back(7);
    v.emplace_back(8);
    v.emplace_back(9);
    cout << v[0] << endl;
    cout << v[1] << endl;
    // cout << v[2] << endl;

    vector<pair<int, int>> vec;
    vec.push_back({1, 2});
    vec.emplace_back(5, 6);
    cout << vec[0].first << endl;
    cout << vec[1].second << endl;

    vector<int> v0(5); // This will create vector of size 5 with 0 as it's element
    cout << v0.size() << endl;
    vector<int> v1(9, 100); // This will create vector of size 9 with 100 as it's element
    cout << v1.size() << endl;
    vector<int> v2(v1); // This will copy vector
    cout << v2.size() << endl;

    vector<int>::iterator it = v.begin();
    cout << sizeof(it) << endl; // It is a pointer
    cout << *it << endl;
    it = it + 2;
    cout << *it << endl;

    vector<int>::iterator it1 = v1.end(); // Here iterator will store address next to the last element
    cout << *it1 << endl;

    // vector<int>::reverse_iterator it2 = v.rend();
    // vector<int>::reverse_iterator it3= v.rbegin();   

    for (vector<int>::iterator it = v.begin(); it != v.end(); it++)
    {
        cout << *(it) << " ";
    }
    cout << endl;
    for (auto it = v.begin(); it != v.end(); it++)
    {
        cout << *(it) << " ";
    }
    cout << endl;

    for (auto it : v)
    {
        cout << it << " ";
    }
    cout << endl;

    // Ip = 1 2 3 4 5 6 ,OP = 1 3 4 5 6
    v.erase(v.begin() + 1);

    // IP = 1 2 3 4 5 6 , OP = 1 2 5 6
    v.erase(v.begin() + 2, v.begin() + 4); //[start ,end )

    // Insert Function
    vector<int> v3(2, 100); //{100,100}
    cout << v3[0] << endl;
    cout << v3[1] << endl;
    v3.insert(v3.begin(), 300);                      //{300,100,100}
    v3.insert(v3.begin() + 1, 2, 10);                //{300,10,10,100,100}
    vector<int> copy(2, 50);                         //{50,50}
    v3.insert(v3.begin(), copy.begin(), copy.end()); //{50,50,300,10,10,100,100}
    for (auto part : v3)
    {
        cout << part << " ";
    }
    cout << endl;
    cout << v3.size() << endl;

    // pop_back Function
    v3.pop_back(); //{50,50,300,10,10,100}
    for (auto part : v3)
    {
        cout << part << " ";
    }
    cout << endl;
    v3.swap(v);
    for (auto part : v3)
    {
        cout << part << " ";
    }
    cout << endl;
    cout << v.empty() << endl;
    v.clear();
    cout << v.empty() << endl;
}

void explainList()
{
    list<int> ls;
    ls.push_back(2);     //{2}
    ls.emplace_back(3);  //{2,3}
    ls.push_front(5);    //{5,2,3}
    ls.emplace_front(1); //{1,5,2,3}
    for (int part : ls)
    {
        cout << part << " ";
    }
    cout << endl;

    // rest functions are same as of vector
    // begin , end, rbegin,rend,clear,insert,size,swap
}

void explainDequeue()
{
    deque<int> dq;
    dq.push_back(1);      //{1}
    dq.emplace_back(2);   //{1,2}
    dq.push_front(0);     //{0,1,2}
    dq.emplace_front(-1); //{-1,0,1,2}
    dq.pop_back();        //{-1,0,1}
    dq.pop_front();       //{0,1}

    for (int part : dq)
    {
        cout << part << " ";
    }
    cout << endl;

    // rest functions are same as of vector
    // begin , end, rbegin,rend,clear,insert,size,swap
}

void explainStack()
{
    stack<int> st;
    st.push(1);    //{1}
    st.push(2);    //{1,2}
    st.push(3);    //{1,2,3}
    st.push(4);    //{1,2,3,4}
    st.emplace(5); //{1,2,3,4,5}

    cout << st.top() << endl; // prints 5 "st[2] is invalid"
    st.pop();                 //{1,2,3,4}
    cout << st.top() << endl;
    cout << st.size() << endl;
    cout << st.empty() << endl;
    stack<int> st1;
    st1.swap(st);
    cout << "st1 = " << st1.size() << endl;
    cout << "st = " << st.size() << endl;
}
void explainQueue()
{

    queue<int> q;
    q.push(1);
    q.push(2);
    q.emplace(4);
    // for (auto part : q)
    // {
    //     cout << part << " "; // Error  no matching function for call to 'begin(std::queue<int>&)'
    //                          // for (auto part : q)
    // }
    // cout << endl;

    q.back() += 5;
    cout << q.back() << endl;
    cout << q.front() << endl;
    q.pop();
    cout << q.front() << endl;

    // size , swap empty same as stack
}

void explainPQ()
{
    priority_queue<int> pq;
    pq.push(5);               //{5}
    pq.push(2);               //{5,2}
    pq.push(8);               //{8,5,2}
    pq.emplace(10);           //{10,8,5,2}
    cout << pq.top() << endl; // 10
    pq.pop();
    cout << pq.top() << endl; // 8
    // size swap empty function same as others

    // Minimum Heap
    priority_queue<int, vector<int>, greater<int>> mpq;
    mpq.push(5);       //{5}
    mpq.push(2);       //{2,5}
    mpq.push(8);       //{2,5,8}
    mpq.emplace(10);   //{2,5,8,10}
    cout << mpq.top(); // 2
}

void explainSet()
{
    set<int> st;
    st.insert(1);  //{1}
    st.emplace(2); //{1,2}
    st.insert(2);  //{1,2}
    st.insert(9);  //{1,2,9}
    st.insert(3);  //{1,2,3,9}

    // Functionality of insert is vector
    // can be used also,that only increases efficiency

    // begin(),end(),rbegin,rend(),size(),
    // empty() and swap() are same as those of above

    auto it = st.find(0);
    cout << *it << endl;
}
int main()
{
    // explainVector();
    // explainList();
    // explainDequeue();
    // explainStack();
    // explainQueue();
    // explainPQ();
    explainSet();
    return 0;
}