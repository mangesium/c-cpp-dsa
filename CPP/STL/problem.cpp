#include <iostream>
#include <vector>
#include <list>

std::_List_iterator<int> &operator+(std::_List_iterator<int> &itr, int x)
{
    while (x)
    {
        ++itr;
        x--;
    }
    return itr;
}
int fun(std::list<int> v, int x)
{
    std::list<int>::iterator itr = v.begin();
    ;
    return *(itr + x);
}
int main()
{
    std::list<int> v = {10, 20, 30, 10, 20, 40};
    std::list<int>::iterator itr = v.begin();
    std::cout << *v.begin() << std::endl;
    std::cout << *itr << std::endl;
    std::cout << *(itr + 2) << std::endl;
    std::cout << fun(v, 5) << std::endl;
    // std::cout << *(v.begin() + 2) << std::endl;

    return 0;
}