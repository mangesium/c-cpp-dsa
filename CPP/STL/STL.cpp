#include <iostream>
#include <vector>
#include <list>
#include <array>

void tryVector()
{
    std::vector<int> v;
    v.emplace_back(10);
    v.push_back(20);
    v.push_back(30);
    v.push_back(40);
    v.insert(v.end(), 50); // inserts elements at provides position pointer

    std::cout << "is Vector empty : " << v.empty() << std::endl;       // checks whether the container is empty
    std::cout << "Vector capacity : " << v.capacity() << std::endl;    // returns the number of elements that can be held in currently allocated storage
    std::cout << "size of Vector : " << v.size() << std::endl;         //	returns the number of elements
    std::cout << "max_size of Vector : " << v.max_size() << std::endl; // returns the maximum possible number of elements
    v.reserve(59);                                                     // reserves storage
    std::cout << "After reserve capacity of Vector : " << v.capacity() << std::endl;
    v.shrink_to_fit(); // reduces memory usage by freeing unused memory
    std::cout << "After shrink_to_fit capacity of Vector: " << v.capacity() << std::endl;

    std::cout << "Vector front : " << v.front() << std::endl;      // returns first element
    std::cout << "Vector back : " << v.back() << std::endl;        // returns last element
    std::cout << "Vector data : " << *(v.data() + 2) << std::endl; // direct access to the underlying array

    std::cout << "Iterating vector using operator[] = ";
    for (int i = 0; i < v.size(); i++)
    {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "Iterating vector using at(int) = ";
    for (int i = 0; i < v.size(); i++)
    {
        std::cout << v.at(i) << " ";
    }
    std::cout << std::endl;
    std::vector<int>::iterator nitr = v.begin();
    std::cout << "Iterating vector using iterator from begin() to end() = ";
    while (nitr != v.end())
    {
        std::cout << *nitr << " ";
        nitr++;
    }
    std::cout << std::endl;

    std::vector<int>::const_iterator citr = v.cbegin();
    std::cout << "Iterating vector using iterator from cbegin() to cend() = ";
    while (citr != v.cend())
    {
        std::cout << *citr << " ";
        citr++;
    }
    std::cout << std::endl;
    std::vector<int>::reverse_iterator ritr = v.rbegin();
    std::cout << "Iterating vector using iterator from rbegin() to rend() = ";
    while (ritr != v.rend())
    {
        std::cout << *ritr << " ";
        ritr++;
    }
    std::cout << std::endl;
    std::vector<int>::const_reverse_iterator critr = v.crbegin();
    std::cout << "Iterating vector using iterator from crbegin() to crend() = ";
    while (critr != v.crend())
    {
        std::cout << *critr << " ";
        critr++;
    }
    std::cout << std::endl;
    std::vector<int> v1 = {1, 2, 3, 4, 5};
    v1.swap(v);
    std::cout << "after Swapping two vectors : " << std::endl;
    std::cout << "v1 = ";
    for (int i = 0; i < v1.size(); i++)
    {
        std::cout << v1[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "v2 = ";
    for (int i = 0; i < v.size(); i++)
    {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
    v1.resize(2);
    std::cout << "After resizing Vector : ";
    for (int i = 0; i < v1.size(); i++)
    {
        std::cout << v1[i] << " ";
    }
    std::cout << std::endl;
    v1.erase(v1.begin());
    v.erase(v.begin());
    std::cout << "after Erasing one element of vectors : " << std::endl;
    std::cout << "v1 = ";
    for (int i = 0; i < v1.size(); i++)
    {
        std::cout << v1[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "v2 = ";
    for (int i = 0; i < v.size(); i++)
    {
        std::cout << v[i] << " ";
    }
    std::cout << std::endl;
}

void tryArray()
{
    std::array<int, 5> arr = {1, 2, 3, 4, 5};
    std::array<int, 5> arr2 = {10, 20, 30};
    // arr.swap(arr2); // Requires ideal size else throws error
    std::cout << "is Array empty = " << arr.empty() << std::endl;
    std::cout << "Array size = " << arr.size() << std::endl;
    std::cout << "Array Max_size = " << arr.max_size() << std::endl;
    std::cout << "Array front = " << arr.front() << std::endl;
    std::cout << "Array back = " << arr.back() << std::endl;
    std::cout << "Array data = " << arr.data() << std::endl; // major change in vector
    std::cout << "Iterating Array using operator[] = ";
    for (int i = 0; i < arr.size(); i++)
    {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
    std::cout << "Iterating Array using at(int) = ";
    for (int i = 0; i < arr.size(); i++)
    {
        std::cout << arr.at(i) << " ";
    }
    std::cout << std::endl;
    std::array<int, 5>::iterator nitr = arr.begin();
    std::cout << "Iterating Array using iterator from begin() to end() = ";
    while (nitr != arr.end())
    {
        std::cout << *nitr << " ";
        nitr++;
    }
    std::cout << std::endl;

    std::array<int, 5>::const_iterator citr = arr.cbegin();
    std::cout << "Iterating Array using iterator from cbegin() to cend() = ";
    while (citr != arr.cend())
    {
        std::cout << *citr << " ";
        citr++;
    }
    std::cout << std::endl;
    std::array<int, 5>::reverse_iterator ritr = arr.rbegin();
    std::cout << "Iterating Array using iterator from rbegin() to rend() = ";
    while (ritr != arr.rend())
    {
        std::cout << *ritr << " ";
        ritr++;
    }
    std::cout << std::endl;
    std::array<int, 5>::const_reverse_iterator critr = arr.crbegin();
    std::cout << "Iterating Array using iterator from crbegin() to crend() = ";
    while (critr != arr.crend())
    {
        std::cout << *critr << " ";
        critr++;
    }
    std::cout << std::endl;
}

void tryList()
{
    std::list<int> lst;
    lst.emplace_back(5);
    lst.emplace_back(6);
    lst.emplace_back(7);
    lst.emplace_front(1);
    lst.emplace_front(2);
    lst.emplace_front(3);
    lst.emplace_front(4);
    lst.push_front(-1);
    lst.push_front(0);
    lst.push_back(8);
    lst.push_back(9);
    std::cout << "is List empty = " << lst.empty() << std::endl;
    std::cout << "List size = " << lst.size() << std::endl;
    std::cout << "List Max_size = " << lst.max_size() << std::endl;
    lst.insert(lst.begin(), 100);
    lst.pop_back();
    lst.pop_front();

    std::list<int>::iterator nitr = lst.begin();
    std::cout << "Iterating List using iterator from begin() to end() = ";
    while (nitr != lst.end())
    {
        std::cout << *nitr << " ";
        nitr++;
    }
    std::cout << std::endl;

    std::list<int>::const_iterator citr = lst.cbegin();
    std::cout << "Iterating List using iterator from cbegin() to cend() = ";
    while (citr != lst.cend())
    {
        std::cout << *citr << " ";
        citr++;
    }
    std::cout << std::endl;
    std::list<int>::reverse_iterator ritr = lst.rbegin();
    std::cout << "Iterating List using iterator from rbegin() to rend() = ";
    while (ritr != lst.rend())
    {
        std::cout << *ritr << " ";
        ritr++;
    }
    std::cout << std::endl;
    std::list<int>::const_reverse_iterator critr = lst.crbegin();
    std::cout << "Iterating List using iterator from crbegin() to crend() = ";
    while (critr != lst.crend())
    {
        std::cout << *critr << " ";
        critr++;
    }
    std::cout << std::endl;
    lst.reverse();
    nitr = lst.begin();
    std::cout << "List after using reverse() = ";
    while (nitr != lst.end())
    {
        std::cout << *nitr << " ";
        nitr++;
    }
    std::cout << std::endl;
    std::list<int> lst1 = {4, 34, 354, 23, 465, 67};
    std::list<int> lst2 = {42, 3364, 3, 23, 45665, 6257, 641, 553};
    lst1.merge(lst2);
    nitr = lst1.begin();
    std::cout << "List after merging with another list = ";
    while (nitr != lst1.end())
    {
        std::cout << *nitr << " ";
        nitr++;
    }
    std::list<int> lst3 = {1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 2, 4, 1};
    lst3.unique();
    nitr = lst3.begin();
    std::cout << "List after using unique = ";
    while (nitr != lst3.end())
    {
        std::cout << *nitr << " ";
        nitr++;
    }
    // auto count2 = lst1.remove_if([](int n){ return n > 10; });
}
int main()
{
    std::cout << "*******************************************************Vector*******************************************************" << std::endl;
    tryVector();
    std::cout << std::endl;
    std::cout << "*******************************************************Array*******************************************************" << std::endl;
    tryArray();
    std::cout << std::endl;
    std::cout << "*******************************************************List*******************************************************" << std::endl;
    tryList();
    std::cout << std::endl;

    return 0;
}