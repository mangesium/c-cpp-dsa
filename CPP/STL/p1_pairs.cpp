#include <iostream>
// #include <utility>
void explainPairs()
{
    std::pair<int, int> p = {1, 3};
    std::cout << "P" << std::endl;
    std::cout << p.first << std::endl;
    std::cout << p.second << std::endl;

    std::pair<int, std::pair<int, int>> q = {1, {2, 3}};
    std::cout << "Q" << std::endl;
    std::cout << q.first << std::endl;
    std::cout << q.second.first << std::endl;
    std::cout << q.second.second << std::endl;

    std::pair<int, int> arr[] = {{1, 2}, {3, 4}, {5, 6}};
    std::cout << "ARRAY" << std::endl;
    std::cout << arr[0].first << std::endl;
    // std::cout << arr[99].first << std::endl;
}
int main()
{
    explainPairs();
    return 0;
}