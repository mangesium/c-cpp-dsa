#include <iostream>
#include <vector>
#include <list>

const std::_List_iterator<int> &operator+(const std::_List_iterator<int> &itr, int x)
{
    // while (x)
    // {
    //     itr++;
    //     x--;
    // }
    std::cout << *(itr + x) << std::endl;
    return itr + x;
}

int main()
{
    // std::vector<int> v = {10, 20, 30, 10, 20, 40};
    // std::vector<int>::iterator itr = v.begin();
    // std::cout << *(v.begin()+2) << std::endl;

    std::list<int> v = {10, 20, 30, 10, 20, 40};
    std::list<int>::iterator itr = v.begin();
    std::cout << *(v.begin() + 2) << std::endl;

    return 0;
}