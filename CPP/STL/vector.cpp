#include <iostream>
#include <vector>
#include <list>
#include <array>

void tryVector()
{
    std::vector<int> *v = new std::vector<int>();
    v->push_back(10);
    v->push_back(20);
    v->push_back(30);
    v->push_back(40);
    v->push_back(50);
    v->push_back(60);
    v->push_back(70);

    std::vector<int>::iterator itr = v->begin();
    while (itr != v->end())
    {
        std::cout << *itr << std::endl;
        itr++;
    }
}

int main()
{
    tryVector();
    return 0;
}