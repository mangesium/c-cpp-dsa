#include <stdio.h>
void main()
{
    int n;
    printf("Enter No. of rows and column : ");
    scanf("%d", &n);
    int k = n;
    for (int i = n; i >= 1; i--)
    {
        for (int j = 1; j <=k; j++)
        {
            if (i == n || j == 1 || j == k)
            {
                printf("* ");
            }
            else
            {
                printf("  ");
            }
        }
        k--;
        printf("\n");
    }
}