#include <stdio.h>
void main()
{
    int n;
    printf("Enter No. of rows and column : ");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        for (int j = 1; j <= 2 * n - 1; j++)
        {
            if (j > i && j <= 2 * n - 1 - i)
                printf("* ");
            else
                printf("  ");
        }
        printf("\n");
    }
}