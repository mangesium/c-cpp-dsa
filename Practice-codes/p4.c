#include <stdio.h>
void main()
{
    int n;
    printf("Enter No. of rows and column : ");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        char ch;
        if (i % 2 == 0)
            ch = '*';
        else
            ch = '#';
        for (int j = 0; j <= i; j++)
        {
            printf("%c ", ch);
            if (ch == '*')
                ch = '#';
            else
                ch = '*';
        }
        printf("\n");
    }
}