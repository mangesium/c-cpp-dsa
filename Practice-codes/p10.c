#include <stdio.h>
void main()
{
    int n;
    printf("Enter No. of rows and column : ");
    scanf("%d", &n);
    int k = -1;
    for (int i = 1; i <= n; i++)
    {
        (i <= (n / 2 + 1)) ? k++ : k--;
        for (int j = 1; j <= n; j++)
        {
            if (j >= ((n / 2 + 1) - k) && j <= ((n / 2 + 1) + k))
                printf("* ");
            else
                printf("  ");
        }
        printf("\n");
    }
}
