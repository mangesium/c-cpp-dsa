#include <stdio.h>
void main()
{
    int n;
    printf("Enter No. of rows and column : ");
    scanf("%d", &n);
    int k = 0;
    for (int i = 1; i <= 2 * n - 1; i++)
    {
        (i <= n) ? k++ : k--;
        for (int j = 1; j <= k; j++)
        {
            printf(" *");
        }
        printf("\n");
    }
}