#include <stdio.h>
void main()
{
    int n;
    printf("Enter No. of rows and column : ");
    scanf("%d", &n);
    int k = n;
    for (int i = 0; i < n; i++)
    {
        for (int j = 1; j <= n + i; j++)
        {
            if (j == n + i || j == n - i || i == n - 1)
            {
                printf("* ");
            }
            else
                printf("  ");
        }
        printf("\n");
    }
}