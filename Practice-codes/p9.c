#include <stdio.h>
void main()
{
    int n;
    printf("Enter No. of rows and column : ");
    scanf("%d", &n);
    int k = 0, m = 1;
    for (int i = 0; i < n; i++)
    {
        m = 1;
        int flag = 0;
        if (i == n - 1)
            flag = 1;
        for (int j = 1; j <= n + k; j++)
        {

            if (flag == 1)
            {
                printf("%d ", m);
                m++;
                flag = 0;
            }
            else if (j == n - k)
            {
                printf("1 ");
            }
            else if (j == n + k)
            {
                printf("%d ", i + 1);
            }
            else
            {
                printf("  ");
                if (i == n - 1)
                    flag = 1;
            }
        }
        k++;
        printf("\n");
    }
}